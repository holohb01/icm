CREATE DATABASE "icm" WITH
	OWNER = "svcredshiftdw";

CREATE TABLE "public"."account_owner"
(
	"business_id" BIGINT NOT NULL ENCODE delta32k DISTKEY,
	"employee_id" VARCHAR(40) NOT NULL ENCODE lzo,
	"start_date" DATE NOT NULL ENCODE delta32k,
	"end_date" DATE ENCODE delta32k,
	"insert_date" TIMESTAMP ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo
);

ALTER TABLE "public"."account_owner"
ADD CONSTRAINT "account_owner_pkey1"
PRIMARY KEY ("business_id", "employee_id", "start_date");



CREATE TABLE "public"."billing_transaction"
(
	"billing_transaction_id" BIGINT NOT NULL ENCODE lzo,
	"business_id" BIGINT ENCODE lzo,
	"product" VARCHAR(50) ENCODE lzo,
	"bill_date" DATE ENCODE lzo,
	"selling_transaction_id" BIGINT ENCODE lzo,
	"invoice_number" VARCHAR(255) ENCODE lzo,
	"primary_rep" VARCHAR(255) ENCODE lzo,
	"secondary_rep" VARCHAR(255) ENCODE lzo,
	"tertiary_rep" VARCHAR(255) ENCODE lzo,
	"source_system" VARCHAR(10) ENCODE lzo,
	"full_billing_amount" NUMERIC(38, 2) ENCODE lzo,
	"prorated_billing_amount" NUMERIC(38, 2) ENCODE lzo,
	"one_time_fees" NUMERIC(38, 2) ENCODE lzo,
	"spread_fees" NUMERIC(38, 2) ENCODE lzo,
	"initial_load_date" DATE ENCODE lzo,
	"update_date" DATE ENCODE lzo,
	"first_bill" SMALLINT ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "public"."billing_transaction"
ADD CONSTRAINT "billing_transaction_pkey"
PRIMARY KEY ("billing_transaction_id");



CREATE TABLE "stage"."billing_transaction_sap_update_selling_id_ordernum"
(
	"selling_transaction_id" BIGINT ENCODE lzo,
	"billing_transaction_id" BIGINT ENCODE lzo DISTKEY
);


CREATE TABLE "stage"."billing_transaction_stage"
(
	"business_id" BIGINT NOT NULL ENCODE lzo,
	"product" VARCHAR(50) NOT NULL ENCODE lzo,
	"bill_date" DATE NOT NULL ENCODE lzo,
	"selling_transaction_id" BIGINT ENCODE lzo,
	"invoice_number" VARCHAR(255) ENCODE lzo,
	"primary_rep" VARCHAR(255) ENCODE lzo,
	"secondary_rep" VARCHAR(255) ENCODE lzo,
	"tertiary_rep" VARCHAR(255) ENCODE lzo,
	"source_system" VARCHAR(10) NOT NULL ENCODE lzo,
	"full_billing_amount" NUMERIC(38, 2) ENCODE lzo,
	"prorated_billing_amount" NUMERIC(38, 2) ENCODE lzo,
	"one_time_fees" NUMERIC(38, 2) ENCODE lzo,
	"spread_fees" NUMERIC(38, 2) ENCODE lzo,
	"initial_load_date" DATE ENCODE lzo,
	"update_date" DATE ENCODE lzo,
	"first_bill" SMALLINT ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."billing_transaction_stage"
ADD CONSTRAINT "billing_transaction_stage_pkey"
PRIMARY KEY ("business_id", "product", "bill_date", "source_system");



CREATE TABLE "stage"."calendar_temp"
(
	"calendar_date" TIMESTAMP DISTKEY
);


CREATE TABLE "stage"."contract_proposal_guid_map"
(
	"proposalid" VARCHAR(40) ENCODE lzo,
	"proposal_guid" VARCHAR(40) ENCODE lzo,
	"region_key" INTEGER NOT NULL ENCODE lzo,
	"contract_key" INTEGER NOT NULL ENCODE lzo DISTKEY,
	"created_on" TIMESTAMP ENCODE lzo,
	"uuid" VARCHAR(150) NOT NULL ENCODE lzo
);

ALTER TABLE "stage"."contract_proposal_guid_map"
ADD CONSTRAINT "contract_proposal_guid_map_pkey"
PRIMARY KEY ("contract_key", "uuid", "region_key");



CREATE TABLE "stage"."contract_proposal_guid_map_dedup"
(
	"proposalid" VARCHAR(40) ENCODE lzo,
	"proposal_guid" VARCHAR(40) ENCODE lzo,
	"region_key" INTEGER ENCODE lzo,
	"contract_key" INTEGER ENCODE lzo,
	"created_on" TIMESTAMP ENCODE lzo,
	"uuid" VARCHAR(150) ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "stage"."contract_webreach_bill"
(
	"contract_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"seq_nbr" DOUBLE PRECISION NOT NULL ENCODE none,
	"budget_amount" DOUBLE PRECISION ENCODE none,
	"requested_start_date" TIMESTAMP ENCODE lzo,
	"actual_start_date" TIMESTAMP ENCODE lzo,
	"next_start_date" TIMESTAMP ENCODE lzo,
	"duration" DOUBLE PRECISION ENCODE none,
	"udac_code" VARCHAR(10) ENCODE lzo,
	"heading_desc" VARCHAR(150) ENCODE lzo,
	"status" VARCHAR(10) ENCODE lzo,
	"current_webreach_key" DOUBLE PRECISION ENCODE none,
	"udac_desc" VARCHAR(150) ENCODE lzo,
	"webreach_key" DOUBLE PRECISION ENCODE none,
	"region_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"total_rcf_dollars" DOUBLE PRECISION ENCODE none,
	"webreach_cancel_key" DOUBLE PRECISION ENCODE none,
	"cancel_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."contract_webreach_bill"
ADD CONSTRAINT "contract_webreach_bill_pkey"
PRIMARY KEY ("contract_key", "seq_nbr", "region_key");



CREATE TABLE "stage"."contract_webreach_bill_full"
(
	"contract_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"seq_nbr" DOUBLE PRECISION NOT NULL ENCODE none,
	"budget_amount" DOUBLE PRECISION ENCODE none,
	"requested_start_date" TIMESTAMP ENCODE lzo,
	"actual_start_date" TIMESTAMP ENCODE lzo,
	"next_start_date" TIMESTAMP ENCODE lzo,
	"duration" DOUBLE PRECISION ENCODE none,
	"udac_code" VARCHAR(10) ENCODE lzo,
	"heading_desc" VARCHAR(150) ENCODE lzo,
	"status" VARCHAR(10) ENCODE lzo,
	"current_webreach_key" DOUBLE PRECISION ENCODE none,
	"udac_desc" VARCHAR(150) ENCODE lzo,
	"webreach_key" DOUBLE PRECISION ENCODE none,
	"region_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"total_rcf_dollars" DOUBLE PRECISION ENCODE none,
	"webreach_cancel_key" DOUBLE PRECISION ENCODE none,
	"cancel_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."contract_webreach_bill_full"
ADD CONSTRAINT "contract_webreach_bill_full_pkey"
PRIMARY KEY ("contract_key", "seq_nbr", "region_key");



CREATE TABLE "stage"."contract_webreach_spent"
(
	"webreach_key" INTEGER NOT NULL ENCODE lzo,
	"spent_date_time" TIMESTAMP NOT NULL ENCODE lzo,
	"spent_amount" NUMERIC(18, 2) NOT NULL ENCODE lzo,
	"mbid" INTEGER ENCODE lzo,
	"customer_key" INTEGER NOT NULL ENCODE lzo,
	"region_key" INTEGER NOT NULL ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"id_key" VARCHAR(50) NOT NULL ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."contract_webreach_spent"
ADD CONSTRAINT "contract_webreach_spent_pkey1"
PRIMARY KEY ("webreach_key", "spent_date_time", "region_key");



CREATE TABLE "public"."customer"
(
	"business_id" BIGINT NOT NULL ENCODE lzo DISTKEY,
	"parent_business_id" BIGINT ENCODE lzo,
	"parent_account_name" VARCHAR(255) ENCODE lzo,
	"company_name" VARCHAR(255) ENCODE lzo,
	"original_acq_date" DATE ENCODE delta32k,
	"last_known_bill_date" DATE ENCODE lzo,
	"sfdc_type" VARCHAR(18) ENCODE lzo,
	"former_customer_flag" SMALLINT ENCODE lzo,
	"former_customer_start_flag_date" DATE,
	"primary_market" VARCHAR(255) ENCODE lzo,
	"primary_classification" VARCHAR(255) ENCODE lzo,
	"igr" NUMERIC(15, 2),
	"latest_acq_date" DATE ENCODE delta32k,
	"new_existing" VARCHAR(8) ENCODE lzo,
	"customer_new_start_date" DATE ENCODE delta32k,
	"customer_new_end_date" DATE ENCODE delta32k,
	"insert_date" TIMESTAMP ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo,
	"p2d_flag" INTEGER,
	"recent_sales_initiative" VARCHAR(255) ENCODE lzo
);

ALTER TABLE "public"."customer"
ADD CONSTRAINT "customer_pkey"
PRIMARY KEY ("business_id");



CREATE TABLE "public"."customer_new"
(
	"parent_business_id" BIGINT NOT NULL ENCODE lzo DISTKEY,
	"new_customer_start_date" DATE ENCODE delta32k,
	"new_customer_end_date" DATE ENCODE delta32k,
	"max_bill_date" DATE ENCODE lzo,
	"customer_end_date" DATE ENCODE lzo,
	"insert_date" TIMESTAMP ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo
);

ALTER TABLE "public"."customer_new"
ADD CONSTRAINT "customer_new_pkey"
PRIMARY KEY ("parent_business_id");



CREATE TABLE "public"."customer_new_audit"
(
	"parent_business_id" BIGINT ENCODE lzo DISTKEY,
	"new_customer_start_date" DATE ENCODE delta32k,
	"new_customer_end_date" DATE ENCODE delta32k,
	"max_bill_date" DATE ENCODE bytedict,
	"customer_end_date" DATE ENCODE lzo,
	"insert_date" TIMESTAMP ENCODE lzo,
	"deleted_flag" BOOLEAN
);


CREATE TABLE "public"."customer_new_update"
(
	"parent_business_id" BIGINT DISTKEY,
	"min_go_live_date" DATE
);


CREATE TABLE "public"."customer_product_incremental"
(
	"parent_business_id" BIGINT NOT NULL ENCODE lzo DISTKEY,
	"anaplan_level_3" VARCHAR(100) NOT NULL ENCODE lzo,
	"min_go_live_date" DATE ENCODE delta32k,
	"max_end_date" DATE ENCODE delta32k,
	"min_bill_date" DATE ENCODE delta32k,
	"max_bill_date" DATE ENCODE lzo,
	"new_product_end_date" DATE ENCODE delta32k,
	"incremental_end_date" DATE ENCODE lzo,
	"insert_date" TIMESTAMP ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo
);

ALTER TABLE "public"."customer_product_incremental"
ADD CONSTRAINT "customer_product_incremental_pkey1"
PRIMARY KEY ("parent_business_id", "anaplan_level_3");



CREATE TABLE "public"."customer_product_incremental_audit"
(
	"parent_business_id" BIGINT ENCODE lzo DISTKEY,
	"anaplan_level_3" VARCHAR(100) ENCODE lzo,
	"min_go_live_date" DATE ENCODE delta32k,
	"max_end_date" DATE ENCODE delta32k,
	"min_bill_date" DATE ENCODE delta32k,
	"max_bill_date" DATE ENCODE delta32k,
	"new_product_end_date" DATE ENCODE delta32k,
	"incremental_end_date" DATE ENCODE delta32k,
	"insert_date" TIMESTAMP ENCODE lzo,
	"deleted_flag" BOOLEAN ENCODE runlength
);


CREATE TABLE "stage"."customer_product_incremental_audit_seed"
(
	"parent_business_id" BIGINT ENCODE lzo,
	"anaplan_level_3" VARCHAR(100) ENCODE lzo,
	"min_submit_go_live_date" DATE ENCODE lzo,
	"max_end_date" DATE ENCODE lzo,
	"min_bill_date" DATE ENCODE lzo,
	"max_bill_date" DATE ENCODE lzo,
	"new_product_end_date" TIMESTAMP ENCODE lzo,
	"incremental_end_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "public"."customer_product_new_duration"
(
	"duration_type" VARCHAR(8),
	"anaplan_level_3" VARCHAR(100),
	"number_of_months" INTEGER DISTKEY
);


CREATE TABLE "stage"."diad_web_contract_bill"
(
	"contract_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"uuid" VARCHAR(150) NOT NULL ENCODE lzo,
	"sequence_number" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"bill_date" TIMESTAMP NOT NULL ENCODE lzo,
	"billed" NUMERIC(1, 0) NOT NULL ENCODE lzo,
	"book_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"pub_sequence" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"sale_pub_sequence" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"new_category" VARCHAR(20) ENCODE lzo,
	"amount" DOUBLE PRECISION ENCODE none,
	"salesrep_key" NUMERIC(38, 0) ENCODE lzo,
	"subcanvass_code" VARCHAR(20) ENCODE lzo,
	"sale_date" TIMESTAMP NOT NULL ENCODE lzo,
	"region_key" NUMERIC(38, 0) NOT NULL ENCODE lzo,
	"id_key" VARCHAR(50) ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."diad_web_contract_bill"
ADD CONSTRAINT "diad_web_contract_bill_pkey"
PRIMARY KEY ("contract_key", "uuid", "sequence_number", "region_key");



CREATE TABLE "stage"."diad_web_contract_lines"
(
	"customer_key" INTEGER NOT NULL ENCODE lzo,
	"region_key" INTEGER NOT NULL ENCODE lzo,
	"udac_code" VARCHAR(10) ENCODE lzo,
	"sale_date" DATE ENCODE lzo,
	"entry_date" DATE ENCODE lzo,
	"contract_key" INTEGER NOT NULL ENCODE lzo DISTKEY,
	"uuid" VARCHAR(150) NOT NULL ENCODE lzo,
	"line_item" INTEGER NOT NULL ENCODE lzo,
	"requested_start_date" DATE ENCODE lzo,
	"months_to_bill" INTEGER ENCODE lzo,
	"drop_date" DATE ENCODE lzo,
	"salesrep_code" VARCHAR(10) NOT NULL ENCODE lzo,
	"rate_override_amount" NUMERIC(9, 2) ENCODE lzo,
	"amount" NUMERIC(9, 2) ENCODE lzo,
	"last_update_date" DATE ENCODE lzo,
	"proposalid" VARCHAR(40) ENCODE lzo,
	"net_coverage" VARCHAR(8) ENCODE lzo,
	"pkg_guid" VARCHAR(40) ENCODE lzo,
	"pkg_seq_guid" VARCHAR(40) ENCODE lzo,
	"bundle_line_item_uuid" VARCHAR(40) ENCODE lzo
);

ALTER TABLE "stage"."diad_web_contract_lines"
ADD CONSTRAINT "diad_web_contract_lines_pkey"
PRIMARY KEY ("contract_key", "uuid", "region_key");



CREATE TABLE "public"."employee"
(
	"employee_num" INTEGER NOT NULL ENCODE lzo,
	"manager_id" INTEGER ENCODE lzo,
	"comp_position_code" VARCHAR(50) ENCODE lzo,
	"ultipro_job_code" VARCHAR(50) ENCODE lzo,
	"effective_start_date" DATE ENCODE lzo,
	"record_created_start_date" DATE ENCODE lzo,
	"record_created_end_date" DATE ENCODE lzo,
	"last_name" VARCHAR(50) ENCODE lzo,
	"first_name" VARCHAR(50) ENCODE lzo,
	"email_address" VARCHAR(100) ENCODE lzo,
	"original_hire_date" DATE ENCODE lzo,
	"termination_date" DATE ENCODE lzo,
	"last_hire_date" DATE ENCODE lzo,
	"date_in_job" DATE ENCODE lzo,
	"employment_agreement_date" DATE ENCODE lzo,
	"comp_agreement_return_date" DATE ENCODE lzo,
	"division_code" VARCHAR(50) ENCODE lzo,
	"division_name" VARCHAR(100) ENCODE lzo,
	"company_code" VARCHAR(50) ENCODE lzo,
	"department_code" VARCHAR(50) ENCODE lzo,
	"department_name" VARCHAR(100) ENCODE lzo,
	"employee_type" VARCHAR(50) ENCODE lzo,
	"initial_load_date" TIMESTAMP ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo,
	"sfdc_profile" VARCHAR(100) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "public"."employee"
ADD CONSTRAINT "employee_num_pkey"
PRIMARY KEY ("employee_num");



CREATE TABLE "stage"."employee_info"
(
	"employee" INTEGER NOT NULL ENCODE lzo,
	"last_name" VARCHAR(50) ENCODE lzo,
	"first_name" VARCHAR(50) ENCODE lzo,
	"userid" VARCHAR(10) ENCODE lzo,
	"business_email" VARCHAR(100) ENCODE lzo,
	"employee_id" VARCHAR(10) ENCODE lzo,
	"salesrep_code" VARCHAR(10) ENCODE lzo,
	"company_code" INTEGER ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."employee_info"
ADD CONSTRAINT "employee_pk"
PRIMARY KEY ("employee");



CREATE TABLE "public"."job_title_hierarchy"
(
	"jobcode" VARCHAR(255) NOT NULL ENCODE lzo,
	"jobdescription" VARCHAR(255) ENCODE lzo,
	"level" VARCHAR(256) ENCODE lzo,
	"abbreviation" VARCHAR(256) ENCODE lzo,
	"hierarchy_level" VARCHAR(256) ENCODE lzo,
	"initial_load_date" TIMESTAMP ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "public"."job_title_hierarchy"
ADD CONSTRAINT "jobcode_pkey"
PRIMARY KEY ("jobcode");



CREATE TABLE "stage"."mpe_asset_transaction"
(
	"customer_guid" VARCHAR(40) NOT NULL ENCODE lzo,
	"asset_id" BIGINT NOT NULL ENCODE lzo,
	"tran_date" TIMESTAMP NOT NULL ENCODE lzo,
	"line_number" INTEGER ENCODE lzo,
	"action" VARCHAR(10) ENCODE lzo,
	"end_date" TIMESTAMP ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"order_number_alpha" VARCHAR(10) ENCODE lzo,
	"cancel_order_num" VARCHAR(10) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."mpe_asset_transaction"
ADD CONSTRAINT "mpe_asset_transaction_pkey"
PRIMARY KEY ("asset_id", "tran_date", "customer_guid");



CREATE TABLE "stage"."mpe_order_sku_asset_map"
(
	"mpe_order_number" VARCHAR(100) NOT NULL ENCODE lzo,
	"product_sku" VARCHAR(100) ENCODE lzo,
	"parent_asset_id" BIGINT NOT NULL ENCODE lzo,
	"asset_sku" VARCHAR(100) ENCODE lzo,
	"asset_description" VARCHAR(100) ENCODE lzo,
	"asset_id" BIGINT NOT NULL ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."mpe_order_sku_asset_map"
ADD CONSTRAINT "mpe_order_sku_asset_map_pkey"
PRIMARY KEY ("mpe_order_number", "parent_asset_id", "asset_id");



CREATE TABLE "stage"."mpe_submitted_order"
(
	"proposal_guid" VARCHAR(40) NOT NULL ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"submitted_on" TIMESTAMP ENCODE lzo,
	"integration_id" VARCHAR(10) NOT NULL ENCODE lzo,
	"order_number_alpha" VARCHAR(10) NOT NULL ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."mpe_submitted_order"
ADD CONSTRAINT "mpe_submitted_order_pkey"
PRIMARY KEY ("proposal_guid");



CREATE TABLE "public"."product_hierarchy"
(
	"product_code" VARCHAR(30) NOT NULL,
	"product_description" VARCHAR(100),
	"product_type" VARCHAR(100),
	"product_category" VARCHAR(30),
	"product_sub_category" VARCHAR(50),
	"item_type" VARCHAR(50),
	"product_family" VARCHAR(50),
	"product_sub_category_02" VARCHAR(50),
	"anaplan_level_4" VARCHAR(100),
	"anaplan_level_3" VARCHAR(100),
	"anaplan_level_2" VARCHAR(100),
	"effective_start_date" DATE NOT NULL,
	"effective_end_date" DATE,
	"insert_date" TIMESTAMP DISTKEY,
	"update_date" TIMESTAMP,
	"one_time_fee_sku" SMALLINT ENCODE lzo,
	"spreed_fee_sku" SMALLINT ENCODE lzo,
	"prorated_sku" SMALLINT ENCODE lzo
);

ALTER TABLE "public"."product_hierarchy"
ADD CONSTRAINT "product_hierarchy_pkey1"
PRIMARY KEY ("product_code", "effective_start_date");



CREATE TABLE "stage"."sap_billing"
(
	"salesorg" VARCHAR(8) NOT NULL ENCODE runlength,
	"bill_num" VARCHAR(20) NOT NULL ENCODE lzo,
	"bill_item" VARCHAR(12) NOT NULL ENCODE bytedict,
	"calmonth" VARCHAR(12) NOT NULL ENCODE runlength,
	"recordmode" VARCHAR(2) NOT NULL ENCODE runlength,
	"comp_code" VARCHAR(8) NOT NULL ENCODE runlength,
	"bill_rule" VARCHAR(2) NOT NULL ENCODE lzo,
	"bill_type" VARCHAR(8) NOT NULL ENCODE lzo,
	"bill_date" VARCHAR(16) NOT NULL ENCODE lzo,
	"storno" VARCHAR(2) NOT NULL ENCODE runlength,
	"bill_cat" VARCHAR(2) NOT NULL ENCODE lzo,
	"cust_group" VARCHAR(4) NOT NULL ENCODE runlength,
	"payer" VARCHAR(20) NOT NULL ENCODE lzo,
	"billtoprty" VARCHAR(20) NOT NULL ENCODE lzo,
	"ship_to" VARCHAR(20) NOT NULL ENCODE lzo,
	"doc_number" VARCHAR(20) NOT NULL ENCODE lzo,
	"s_ord_item" VARCHAR(12) NOT NULL ENCODE bytedict,
	"refer_doc" VARCHAR(20) NOT NULL ENCODE lzo,
	"refer_itm" VARCHAR(12) NOT NULL ENCODE bytedict,
	"createdon" VARCHAR(16) NOT NULL ENCODE runlength,
	"serv_date" VARCHAR(16) NOT NULL ENCODE lzo,
	"trans_date" VARCHAR(16) NOT NULL ENCODE lzo,
	"itm_type" VARCHAR(2) NOT NULL ENCODE runlength,
	"prod_hier" VARCHAR(36) NOT NULL ENCODE runlength,
	"price_date" VARCHAR(16) NOT NULL ENCODE lzo,
	"item_categ" VARCHAR(8) NOT NULL ENCODE bytedict,
	"salesemply" VARCHAR(16) NOT NULL ENCODE runlength,
	"stat_date" VARCHAR(16) NOT NULL ENCODE runlength,
	"st_up_dte" VARCHAR(16) NOT NULL ENCODE lzo,
	"rate_type" VARCHAR(8) NOT NULL ENCODE runlength,
	"doc_categ" VARCHAR(4) NOT NULL ENCODE lzo,
	"distr_chan" VARCHAR(4) NOT NULL ENCODE lzo,
	"plant" VARCHAR(8) NOT NULL ENCODE runlength,
	"co_area" VARCHAR(8) NOT NULL ENCODE runlength,
	"costcenter" VARCHAR(20) NOT NULL ENCODE runlength,
	"matl_group" VARCHAR(18) NOT NULL ENCODE runlength,
	"material" VARCHAR(36) NOT NULL ENCODE lzo,
	"division" VARCHAR(4) NOT NULL ENCODE runlength,
	"div_head" VARCHAR(4) NOT NULL ENCODE runlength,
	"sales_off" VARCHAR(8) NOT NULL ENCODE lzo,
	"sales_dist" VARCHAR(12) NOT NULL ENCODE runlength,
	"sales_grp" VARCHAR(6) NOT NULL ENCODE lzo,
	"processkey" VARCHAR(6) NOT NULL ENCODE lzo,
	"logsys" VARCHAR(20) NOT NULL ENCODE lzo,
	"ch_on" VARCHAR(16) NOT NULL ENCODE lzo,
	"fiscvarnt" VARCHAR(4) NOT NULL ENCODE runlength,
	"exrate_acc" NUMERIC(17, 3) NOT NULL ENCODE runlength,
	"exchg_rate" DOUBLE PRECISION NOT NULL ENCODE runlength,
	"tax_amount" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"cshdsc_bas" NUMERIC(17, 2) NOT NULL ENCODE runlength,
	"cost" NUMERIC(17, 2) NOT NULL ENCODE runlength,
	"no_inv_it" NUMERIC(17, 3) NOT NULL ENCODE lzo,
	"subtotal_1" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"subtotal_2" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"subtotal_3" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"subtotal_4" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"subtotal_5" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"subtotal_6" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"netval_inv" NUMERIC(17, 2) NOT NULL ENCODE lzo,
	"exchg_stat" DOUBLE PRECISION NOT NULL ENCODE runlength,
	"exratexacc" DOUBLE PRECISION NOT NULL ENCODE runlength,
	"doc_currcy" VARCHAR(10) NOT NULL ENCODE runlength,
	"sold_to" VARCHAR(20) NOT NULL ENCODE lzo,
	"profit_ctr" VARCHAR(20) NOT NULL ENCODE lzo,
	"pymt_meth" VARCHAR(2) NOT NULL ENCODE lzo,
	"country" VARCHAR(6) NOT NULL ENCODE runlength,
	"bic_zsdcreadt" VARCHAR(16) NOT NULL ENCODE lzo,
	"pmnttrms" VARCHAR(8) NOT NULL ENCODE lzo,
	"bic_zcontstdt" VARCHAR(16) NOT NULL ENCODE lzo,
	"bic_zcontendt" VARCHAR(16) NOT NULL ENCODE lzo,
	"bic_zcontprd" VARCHAR(4) NOT NULL ENCODE lzo,
	"bilblk_itm" VARCHAR(4) NOT NULL ENCODE lzo,
	"bic_zcrdblock" VARCHAR(2) NOT NULL ENCODE runlength,
	"rejectn_st" VARCHAR(2) NOT NULL ENCODE runlength,
	"doc_type" VARCHAR(8) NOT NULL ENCODE lzo,
	"overall_st" VARCHAR(2) NOT NULL ENCODE lzo,
	"ord_reason" VARCHAR(6) NOT NULL ENCODE lzo,
	"knart" VARCHAR(8) NOT NULL ENCODE runlength,
	"bic_zcustmatl" VARCHAR(70) NOT NULL ENCODE runlength,
	"bic_zcpnind" VARCHAR(6) NOT NULL ENCODE runlength,
	"bic_zcount" NUMERIC(17, 3) NOT NULL ENCODE runlength,
	"fiscyear" VARCHAR(8) NOT NULL ENCODE runlength,
	"fiscper" VARCHAR(14) NOT NULL ENCODE runlength,
	"dunn_block" VARCHAR(2) NOT NULL ENCODE runlength,
	"bic_zbbsetdt" VARCHAR(16) NOT NULL ENCODE lzo,
	"bic_zdbsetdt" VARCHAR(16) NOT NULL ENCODE runlength,
	"netduedate" VARCHAR(16) NOT NULL ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sap_billing"
ADD CONSTRAINT "sap_billing_pkey"
PRIMARY KEY ("salesorg", "bill_num", "bill_item", "calmonth");



CREATE TABLE "public"."selling_transaction"
(
	"selling_transaction_id" BIGINT NOT NULL ENCODE delta DISTKEY,
	"business_id" BIGINT ENCODE lzo,
	"product" VARCHAR(50) ENCODE lzo,
	"asset_id" BIGINT ENCODE lzo,
	"asset_code" VARCHAR(40) ENCODE lzo,
	"sold_date" DATE ENCODE delta32k,
	"order_number" VARCHAR(1000) ENCODE lzo,
	"source_system" VARCHAR(4) ENCODE lzo,
	"original_acq_date" DATE ENCODE delta32k,
	"latest_acq_date" DATE ENCODE delta32k,
	"submitted" DATE ENCODE delta32k,
	"consultation_complete_date" DATE,
	"go_live_date" DATE ENCODE bytedict,
	"cancel_date" DATE,
	"end_date" DATE ENCODE bytedict,
	"new_customer" VARCHAR(8) ENCODE lzo,
	"incremental_sale" INTEGER ENCODE lzo,
	"primary_employee_id" VARCHAR(40) ENCODE lzo,
	"secondary_employee_id" VARCHAR(40) ENCODE lzo,
	"tertiary_employee_id" VARCHAR(40) ENCODE lzo,
	"unit_sold" INTEGER ENCODE lzo,
	"product_monthly_revenue" NUMERIC(18, 2) ENCODE bytedict,
	"discount_amount" NUMERIC(18, 2) ENCODE lzo,
	"discount_period" INTEGER ENCODE lzo,
	"one_time_fees" NUMERIC(18, 2) ENCODE lzo,
	"one_time_fees_discount" NUMERIC(18, 2) ENCODE lzo,
	"spread_fees" NUMERIC(18, 2) ENCODE lzo,
	"spread_period" INTEGER ENCODE lzo,
	"status" VARCHAR(255) ENCODE lzo,
	"sale_type" VARCHAR(40) ENCODE lzo,
	"initial_load_date" TIMESTAMP ENCODE lzo,
	"update_date" TIMESTAMP ENCODE lzo,
	"p2d" VARCHAR(255) ENCODE lzo,
	"p2d_expected" NUMERIC(18, 2),
	"p2d_serviced_dollars" NUMERIC(18, 2),
	"p2d_serviced_number" NUMERIC(18, 2),
	"p2d_total" NUMERIC(18, 2),
	"recent_sales_initiative" VARCHAR(255) ENCODE lzo,
	"upsell" INTEGER,
	"min_uuid" VARCHAR(40) ENCODE lzo,
	"spend_change_amount" NUMERIC(18, 2) ENCODE lzo,
	"drop_date" DATE ENCODE lzo,
	"bundle_iyp_amount" NUMERIC(18, 2) ENCODE lzo,
	"iyp_net_coverage" VARCHAR(8) ENCODE lzo,
	"bundle_id" VARCHAR(40) ENCODE lzo
);

ALTER TABLE "public"."selling_transaction"
ADD CONSTRAINT "selling_transaction_pkey"
PRIMARY KEY ("selling_transaction_id");



CREATE TABLE "stage"."sfa_proposal_master"
(
	"proposal_guid" VARCHAR(40) NOT NULL ENCODE lzo,
	"server_processed_date" DATE ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfa_proposal_master"
ADD CONSTRAINT "sfa_proposal_master_pkey"
PRIMARY KEY ("proposal_guid");



CREATE TABLE "stage"."sfdc_account_history_owner"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"isdeleted" VARCHAR(10) ENCODE lzo,
	"accountid" VARCHAR(18) ENCODE lzo,
	"createdbyid" VARCHAR(18) ENCODE lzo,
	"createddate" TIMESTAMP ENCODE lzo,
	"field" VARCHAR(255) ENCODE lzo,
	"oldvalue" VARCHAR(255) ENCODE lzo,
	"newvalue" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_account_history_owner"
ADD CONSTRAINT "sfdc_account_history_owner_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_asset"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"isdeleted" BOOLEAN NOT NULL ENCODE none,
	"name" VARCHAR(80) ENCODE lzo,
	"recordtypeid" VARCHAR(18) ENCODE lzo,
	"createddate" TIMESTAMP NOT NULL ENCODE lzo,
	"createdbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"lastmodifieddate" TIMESTAMP NOT NULL ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"systemmodstamp" TIMESTAMP NOT NULL ENCODE lzo,
	"lastvieweddate" TIMESTAMP ENCODE lzo,
	"lastreferenceddate" TIMESTAMP ENCODE lzo,
	"product_detail__c" VARCHAR(18) NOT NULL ENCODE lzo,
	"address__c" VARCHAR(255) ENCODE lzo,
	"aesst_artid__c" VARCHAR(1300) ENCODE lzo,
	"art_id__c" VARCHAR(10) ENCODE lzo,
	"bundle_id__c" VARCHAR(40) ENCODE lzo,
	"description__c" VARCHAR(255) ENCODE lzo,
	"discount_code__c" VARCHAR(2) ENCODE lzo,
	"external_asset_id__c" VARCHAR(100) ENCODE lzo,
	"external_product_id__c" VARCHAR(100) ENCODE lzo,
	"heading_classification__c" VARCHAR(100) ENCODE lzo,
	"heading_code__c" VARCHAR(20) ENCODE lzo,
	"listingnumber__c" VARCHAR(1300) ENCODE lzo,
	"listing__c" VARCHAR(4) ENCODE lzo,
	"monthly__c" NUMERIC(18, 2) ENCODE lzo,
	"name__c" VARCHAR(50) ENCODE lzo,
	"number__c" VARCHAR(40) ENCODE lzo,
	"reservation__c" VARCHAR(10) ENCODE lzo,
	"seniority__c" TIMESTAMP ENCODE lzo,
	"total_calls__c" NUMERIC(7, 0) ENCODE lzo,
	"line_item__c" NUMERIC(18, 0) ENCODE lzo,
	"account_id_calc__c" VARCHAR(1300) ENCODE lzo,
	"aria_asset_type__c" VARCHAR(50) ENCODE lzo,
	"assename_productname_calc__c" VARCHAR(1300) ENCODE lzo,
	"asset_desc_calc__c" VARCHAR(1300) ENCODE lzo,
	"asset_id__c" VARCHAR(50) ENCODE lzo,
	"asset_type__c" VARCHAR(1300) ENCODE lzo,
	"byod__c" BOOLEAN NOT NULL ENCODE none,
	"fulfillment_status__c" VARCHAR(100) ENCODE lzo,
	"isdigital__c" BOOLEAN NOT NULL ENCODE none,
	"one_time_fee__c" NUMERIC(18, 2) ENCODE lzo,
	"order_asset_id__c" VARCHAR(255) ENCODE lzo,
	"order_parent_asset_id__c" VARCHAR(255) ENCODE lzo,
	"status_last_refreshed_date__c" TIMESTAMP ENCODE lzo,
	"accountid__c" VARCHAR(1300) ENCODE lzo,
	"supplemental_product__c" VARCHAR(18) ENCODE lzo,
	"build_complete_date__c" TIMESTAMP ENCODE lzo,
	"set_live_date_volatile__c" TIMESTAMP ENCODE lzo,
	"type__c" VARCHAR(255) ENCODE lzo,
	"hibu_appointments_account_id__c" VARCHAR(100) ENCODE lzo,
	"hibu_appointments_company_id__c" VARCHAR(100) ENCODE lzo,
	"end_date__c" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_asset"
ADD CONSTRAINT "sfdc_asset_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_classification"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"ownerid" VARCHAR(18) ENCODE lzo,
	"isdeleted" INTEGER ENCODE lzo,
	"name" VARCHAR(255) ENCODE lzo,
	"recordtypeid" VARCHAR(18) ENCODE lzo,
	"createddate" VARCHAR(255) ENCODE lzo,
	"createdbyid" VARCHAR(18) ENCODE lzo,
	"lastmodifieddate" VARCHAR(255) ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) ENCODE lzo,
	"systemmodstamp" VARCHAR(255) ENCODE lzo,
	"lastvieweddate" VARCHAR(255) ENCODE lzo,
	"lastreferenceddate" VARCHAR(255) ENCODE lzo,
	"heading_code__c" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_classification"
ADD CONSTRAINT "sfdc_classification_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_market"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"ownerid" VARCHAR(18) ENCODE lzo,
	"isdeleted" INTEGER ENCODE lzo,
	"name" VARCHAR(255) ENCODE lzo,
	"createddate" VARCHAR(255) ENCODE lzo,
	"createdbyid" VARCHAR(18) ENCODE lzo,
	"lastmodifieddate" VARCHAR(255) ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) ENCODE lzo,
	"systemmodstamp" VARCHAR(255) ENCODE lzo,
	"description__c" VARCHAR(255) ENCODE lzo,
	"inactive__c" VARCHAR(255) ENCODE lzo,
	"region__c" VARCHAR(255) ENCODE lzo,
	"credit_lockout__c" VARCHAR(255) ENCODE lzo,
	"external_primary_mkt_id__c" VARCHAR(255) ENCODE lzo,
	"market_close_date__c" VARCHAR(255) ENCODE lzo,
	"sales_start_date__c" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_market"
ADD CONSTRAINT "sfdc_market_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_master_customer"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"name" VARCHAR(255) ENCODE lzo,
	"type" VARCHAR(18) ENCODE lzo,
	"recordtypeid" VARCHAR(18) ENCODE lzo,
	"parentid" VARCHAR(18) ENCODE lzo,
	"business_id_hidden__c" BIGINT ENCODE lzo,
	"business_group_id__c" BIGINT ENCODE lzo,
	"diad_account_id__c" VARCHAR(20) ENCODE lzo,
	"customer_key__c" BIGINT ENCODE lzo,
	"region_key__c" BIGINT ENCODE lzo,
	"ghost_account_id__c" BIGINT ENCODE lzo,
	"previous_customer__c" SMALLINT ENCODE lzo,
	"primary_market_lookup__c" VARCHAR(100) ENCODE lzo,
	"primary_classification__c" VARCHAR(100) ENCODE lzo,
	"ig_rank__c" NUMERIC(15, 2) ENCODE lzo,
	"ownerid" VARCHAR(18) ENCODE lzo,
	"createddate" VARCHAR(29) ENCODE lzo,
	"lastmodifieddate" VARCHAR(29) ENCODE lzo,
	"systemmodstamp" VARCHAR(29) ENCODE lzo,
	"p2d_flag" INTEGER ENCODE lzo,
	"recent_sales_initiative" VARCHAR(255) ENCODE lzo,
	"p2d_flag1" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_master_customer"
ADD CONSTRAINT "sfdc_master_customer_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_opportunity"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"isdeleted" BOOLEAN NOT NULL ENCODE none,
	"accountid" VARCHAR(18) ENCODE lzo,
	"recordtypeid" VARCHAR(18) ENCODE lzo,
	"isprivate" BOOLEAN NOT NULL ENCODE none,
	"name" VARCHAR(32000) NOT NULL ENCODE lzo,
	"description" VARCHAR(32000) ENCODE lzo,
	"stagename" VARCHAR(40) NOT NULL ENCODE lzo,
	"amount" NUMERIC(18, 2) ENCODE lzo,
	"probability" NUMERIC(3, 0) ENCODE lzo,
	"expectedrevenue" NUMERIC(18, 2) ENCODE lzo,
	"totalopportunityquantity" NUMERIC(18, 2) ENCODE lzo,
	"closedate" TIMESTAMP NOT NULL ENCODE lzo,
	"type" VARCHAR(40) ENCODE lzo,
	"nextstep" VARCHAR(255) ENCODE lzo,
	"leadsource" VARCHAR(40) ENCODE lzo,
	"isclosed" BOOLEAN NOT NULL ENCODE none,
	"iswon" BOOLEAN NOT NULL ENCODE none,
	"forecastcategory" VARCHAR(40) NOT NULL ENCODE lzo,
	"forecastcategoryname" VARCHAR(40) ENCODE lzo,
	"campaignid" VARCHAR(18) ENCODE lzo,
	"hasopportunitylineitem" BOOLEAN NOT NULL ENCODE none,
	"pricebook2id" VARCHAR(18) ENCODE lzo,
	"ownerid" VARCHAR(18) NOT NULL ENCODE lzo,
	"createddate" TIMESTAMP NOT NULL ENCODE lzo,
	"createdbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"lastmodifieddate" TIMESTAMP NOT NULL ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"systemmodstamp" TIMESTAMP NOT NULL ENCODE lzo,
	"lastactivitydate" TIMESTAMP ENCODE lzo,
	"fiscalquarter" INTEGER ENCODE lzo,
	"fiscalyear" INTEGER ENCODE lzo,
	"fiscal" VARCHAR(6) ENCODE lzo,
	"lastvieweddate" TIMESTAMP ENCODE lzo,
	"lastreferenceddate" TIMESTAMP ENCODE lzo,
	"syncedquoteid" VARCHAR(18) ENCODE lzo,
	"contractid" VARCHAR(18) ENCODE lzo,
	"db_competitor__c" VARCHAR(255) ENCODE lzo,
	"employee_number__c" VARCHAR(1300) ENCODE lzo,
	"closed_lost_reason__c" VARCHAR(255) ENCODE lzo,
	"sales_initiative__c" VARCHAR(255) ENCODE lzo,
	"aria_order_value__c" NUMERIC(18, 2) ENCODE lzo,
	"case__c" VARCHAR(18) ENCODE lzo,
	"order_number__c" VARCHAR(40) ENCODE lzo,
	"assign_sales_rep_name_code__c" VARCHAR(100) ENCODE lzo,
	"rep_rating__c" VARCHAR(255) ENCODE lzo,
	"rejected_reason__c" VARCHAR(255) ENCODE lzo,
	"dialsource__datetime_of_last_disposition_ds__c" TIMESTAMP ENCODE lzo,
	"engagement_score__c" NUMERIC(1, 0) ENCODE lzo,
	"mbid__c" VARCHAR(20) ENCODE lzo,
	"sub_channel__c" VARCHAR(1300) ENCODE lzo,
	"subtype__c" VARCHAR(255) ENCODE lzo,
	"account_location__c" VARCHAR(1300) ENCODE lzo,
	"account_name__c" VARCHAR(1300) ENCODE lzo,
	"account_phone__c" VARCHAR(1300) ENCODE lzo,
	"lead_source_most_recent__c" VARCHAR(255) ENCODE lzo,
	"primary_contact__c" VARCHAR(18) ENCODE lzo,
	"channel__c" VARCHAR(255) ENCODE lzo,
	"created_by_lead_conversion__c" BOOLEAN NOT NULL ENCODE none,
	"set_up_fee__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__acv__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__arr__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__allow_commit__c" BOOLEAN NOT NULL ENCODE none,
	"asf3__allow_save_as_draft__c" BOOLEAN NOT NULL ENCODE none,
	"asf3__ariadatetimecreated__c" TIMESTAMP ENCODE lzo,
	"asf3__aria_currency__c" VARCHAR(50) ENCODE lzo,
	"asf3__aria_discount_total__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__aria_mmr_unit_total__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__aria_mmr_unit_total_v2__c" NUMERIC(18, 0) ENCODE lzo,
	"asf3__aria_mrr_total__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__aria_opportunity_total__c" NUMERIC(28, 2) ENCODE lzo,
	"asf3__aria_opportunity_total_v2__c" NUMERIC(28, 2) ENCODE lzo,
	"asf3__aria_opportunity__c" BOOLEAN NOT NULL ENCODE none,
	"asf3__aria_order_total__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__aria_summary__c" VARCHAR(32768) ENCODE lzo,
	"asf3__datetimecommittoaria__c" TIMESTAMP ENCODE lzo,
	"asf3__ismasterplanchange__c" BOOLEAN NOT NULL ENCODE none,
	"asf3__previous_aria_mrr_total__c" NUMERIC(18, 2) ENCODE lzo,
	"p2d_expected__c" NUMERIC(18, 2) ENCODE lzo,
	"p2d_total__c" NUMERIC(18, 2) ENCODE lzo,
	"voice_verify__c" VARCHAR(255) ENCODE lzo,
	"dialforce__ds_last_call_list_name__c" VARCHAR(255) ENCODE lzo,
	"dialforce__vv_eligible__c" VARCHAR(255) ENCODE lzo,
	"asf3__tcv__c" NUMERIC(18, 2) ENCODE lzo,
	"asf3__trackingnumber__c" VARCHAR(12) ENCODE lzo,
	"asf3__iskeep_common_suppplan__c" BOOLEAN NOT NULL ENCODE none,
	"asf3__z_ariaplanchangetype__c" VARCHAR(1300) ENCODE lzo,
	"auto_close_date__c" TIMESTAMP ENCODE lzo,
	"flags__c" VARCHAR(100) ENCODE lzo,
	"dialsource__last_disposition_ds__c" VARCHAR(140) ENCODE lzo,
	"cities__c" VARCHAR(255) ENCODE lzo,
	"or_radius_address__c" VARCHAR(255) ENCODE lzo,
	"or_state_or_nationwide__c" VARCHAR(255) ENCODE lzo,
	"p2d__c" VARCHAR(255) ENCODE lzo,
	"account_profile__c" VARCHAR(100) ENCODE lzo,
	"impactradius_id__c" VARCHAR(255) ENCODE lzo,
	"products__c" VARCHAR(4099) ENCODE lzo,
	"services__c" VARCHAR(255) ENCODE lzo,
	"ir_reversal_reason__c" VARCHAR(255) ENCODE lzo,
	"created_time__c" TIMESTAMP ENCODE lzo,
	"exception__c" VARCHAR(255) ENCODE lzo,
	"source__c" VARCHAR(255) ENCODE lzo,
	"work_with_account__c" VARCHAR(50) ENCODE lzo,
	"display_paid_date__c" TIMESTAMP ENCODE lzo,
	"reviews_paid_date__c" TIMESTAMP ENCODE lzo,
	"search_paid_date__c" TIMESTAMP ENCODE lzo,
	"seo_paid_date__c" TIMESTAMP ENCODE lzo,
	"social_paid_date__c" TIMESTAMP ENCODE lzo,
	"wave_paid_date__c" TIMESTAMP ENCODE lzo,
	"website_paid_date__c" TIMESTAMP ENCODE lzo,
	"display_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"reviews_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"search_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"seo_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"social_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"wave_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"website_paid_value__c" NUMERIC(18, 2) ENCODE lzo,
	"weeks_remaining__c" NUMERIC(28, 0) ENCODE lzo,
	"closed_won_amount__c" NUMERIC(28, 2) ENCODE lzo,
	"dialforce__vv_recording__c" VARCHAR(255) ENCODE lzo,
	"impactradius_click_id__c" VARCHAR(255) ENCODE lzo,
	"dialsource__next_call_ds__c" TIMESTAMP ENCODE lzo,
	"dialsource__total_call_count_ds__c" NUMERIC(3, 0) ENCODE lzo,
	"opportunity_count__c" NUMERIC(28, 2) ENCODE lzo,
	"p2d_serviced_dollars__c" NUMERIC(28, 2) ENCODE lzo,
	"p2d_serviced_number__c" NUMERIC(28, 2) ENCODE lzo,
	"gtm_discount_applied__c" BOOLEAN NOT NULL ENCODE none,
	"gtm_discount_code__c" VARCHAR(255) ENCODE lzo,
	"gtm_discount_reason__c" VARCHAR(255) ENCODE lzo,
	"opportunity_profile__c" VARCHAR(100) ENCODE lzo,
	"dialsource__last_call_campaign_name__c" VARCHAR(255) ENCODE lzo,
	"locktype__c" BOOLEAN NOT NULL ENCODE none,
	"business_id__c" VARCHAR(1300) ENCODE lzo,
	"display_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"listings_management_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"reviews_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"search_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"seo_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"social_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"website_expected_value__c" NUMERIC(18, 2) ENCODE lzo,
	"dialsource__ds_owner_id__c" VARCHAR(1300) ENCODE lzo,
	"count_opportunities__c" NUMERIC(28, 0) ENCODE lzo,
	"ops_order__c" BOOLEAN NOT NULL ENCODE none
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_opportunity"
ADD CONSTRAINT "opportunity_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_product"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"ownerid" VARCHAR(18) ENCODE lzo,
	"isdeleted" INTEGER ENCODE lzo,
	"name" VARCHAR(80) ENCODE lzo,
	"recordtypeid" VARCHAR(18) ENCODE lzo,
	"createddate" DATE ENCODE lzo,
	"createdbyid" VARCHAR(18) ENCODE lzo,
	"lastmodifieddate" DATE ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) ENCODE lzo,
	"systemmodstamp" DATE ENCODE lzo,
	"lastvieweddate" DATE ENCODE lzo,
	"lastreferenceddate" DATE ENCODE lzo,
	"account__c" VARCHAR(18) ENCODE lzo,
	"approved__c" DATE ENCODE lzo,
	"book_code__c" VARCHAR(20) ENCODE lzo,
	"book_year__c" VARCHAR(4) ENCODE lzo,
	"business_id_hidden__c" VARCHAR(255) ENCODE lzo,
	"contract__c" VARCHAR(20) ENCODE lzo,
	"directory__c" VARCHAR(255) ENCODE lzo,
	"dropped_date__c" DATE ENCODE lzo,
	"email__c" VARCHAR(80) ENCODE lzo,
	"end_date__c" DATE ENCODE lzo,
	"entry_date__c" DATE ENCODE lzo,
	"external_product_id__c" VARCHAR(100) ENCODE lzo,
	"go_live_date__c" DATE ENCODE lzo,
	"issue_year__c" VARCHAR(4) ENCODE lzo,
	"link_to_contract__c" VARCHAR(255) ENCODE lzo,
	"monthly_spend__c" NUMERIC(18, 2) ENCODE lzo,
	"one_time_fees__c" NUMERIC(18, 2) ENCODE lzo,
	"payment_terms__c" VARCHAR(255) ENCODE lzo,
	"prior_contract__c" VARCHAR(20) ENCODE lzo,
	"product_family__c" VARCHAR(100) ENCODE lzo,
	"rate_card__c" VARCHAR(255) ENCODE lzo,
	"received_date__c" DATE ENCODE lzo,
	"renewal_contract__c" VARCHAR(100) ENCODE lzo,
	"renewal_status__c" VARCHAR(255) ENCODE lzo,
	"renewal_type__c" VARCHAR(255) ENCODE lzo,
	"sale_date__c" DATE ENCODE lzo,
	"send_proof__c" INTEGER ENCODE lzo,
	"status__c" VARCHAR(255) ENCODE lzo,
	"total_sale__c" NUMERIC(18, 2) ENCODE lzo,
	"type__c" VARCHAR(255) ENCODE lzo,
	"url__c" VARCHAR(255) ENCODE lzo,
	"year__c" VARCHAR(4) ENCODE lzo,
	"assign_sales_rep_name_code__c" VARCHAR(100) ENCODE lzo,
	"sold_salesrep_name_code__c" VARCHAR(100) ENCODE lzo,
	"account_historic__c" VARCHAR(18) ENCODE lzo,
	"region_name__c" VARCHAR(20) ENCODE lzo,
	"consultation_complete_date__c" DATE ENCODE lzo,
	"current_dunning_level_date__c" DATE ENCODE lzo,
	"dunning_level__c" INTEGER ENCODE lzo,
	"next_dunning_level_date__c" DATE ENCODE lzo,
	"sales_cycle_year__c" VARCHAR(4) ENCODE lzo,
	"parent_product__c" VARCHAR(18) ENCODE lzo,
	"plan_id__c" VARCHAR(30) ENCODE lzo,
	"product_asset_id__c" VARCHAR(50) ENCODE lzo,
	"product_instance_id__c" VARCHAR(50) ENCODE lzo,
	"product_number__c" VARCHAR(50) ENCODE lzo,
	"requested_start_date__c" DATE ENCODE lzo,
	"serial_num__c" INTEGER ENCODE lzo,
	"product_state__c" VARCHAR(1300) ENCODE lzo,
	"account_migrated__c" VARCHAR(18) ENCODE lzo,
	"close_date__c" DATE ENCODE lzo,
	"credit_lockout_date__c" DATE ENCODE lzo,
	"dunning_amount__c" NUMERIC(18, 2) ENCODE lzo,
	"dunning_state__c" VARCHAR(20) ENCODE lzo,
	"eligible_for_cancellation__c" INTEGER ENCODE lzo,
	"isdigitalrecordtype__c" INTEGER ENCODE lzo,
	"local_sales_start_date__c" DATE ENCODE lzo,
	"primary_market__c" VARCHAR(40) ENCODE lzo,
	"record_type_name_calc__c" VARCHAR(1300) ENCODE lzo,
	"website_instance_id__c" VARCHAR(100) ENCODE lzo,
	"website_template_id__c" VARCHAR(100) ENCODE lzo,
	"set_live_date_volatile__c" DATE ENCODE lzo,
	"dunning_state_string__c" VARCHAR(1300) ENCODE lzo,
	"assigned_analyst_email__c" VARCHAR(80) ENCODE lzo,
	"assigned_analyst_name__c" VARCHAR(18) ENCODE lzo,
	"assigned_analyst_name_text__c" VARCHAR(100) ENCODE lzo,
	"assigned_analyst_phone__c" VARCHAR(40) ENCODE lzo,
	"contract_end_date__c" DATE ENCODE lzo,
	"migrated_from__c" VARCHAR(30) ENCODE lzo,
	"next_bill_date__c" DATE ENCODE lzo,
	"notes__c" VARCHAR(500) ENCODE lzo,
	"oldest_due_date__c" DATE ENCODE lzo,
	"performance_email_count__c" INTEGER ENCODE lzo,
	"report_center_last_login_date_" DATE ENCODE lzo,
	"report_center_login_times__c" INTEGER ENCODE lzo,
	"suggested_budget_multiples__c" NUMERIC(18, 2) ENCODE lzo,
	"suggested_budget_multiplier__c" VARCHAR(1300) ENCODE lzo,
	"suggested_monthly_budget__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_0_10__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_11_30__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_121_150__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_151_over__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_31_60__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_61_90__c" NUMERIC(18, 2) ENCODE lzo,
	"aging_bucket_91_120__c" NUMERIC(18, 2) ENCODE lzo,
	"days_past_due__c" INTEGER ENCODE lzo,
	"opt_out_curated_posts__c" INTEGER ENCODE lzo,
	"validation_date__c" DATE ENCODE lzo,
	"assigned_rep__c" VARCHAR(18) ENCODE lzo,
	"actual_rep__c" VARCHAR(18) ENCODE lzo,
	"upsell__c" INTEGER ENCODE lzo,
	"sku__c" VARCHAR(80) ENCODE lzo,
	"primary_rep_employee_number__c" VARCHAR(40) ENCODE lzo,
	"primary_rep__c" VARCHAR(18) ENCODE lzo,
	"secondary_rep_employee_number__c" VARCHAR(40) ENCODE lzo,
	"secondary_rep__c" VARCHAR(18) ENCODE lzo,
	"social_future_start_date__c" DATE ENCODE lzo,
	"business_id__c" VARCHAR(255) ENCODE lzo,
	"opportunity__c" VARCHAR(18) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_product"
ADD CONSTRAINT "sfdc_product_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."sfdc_profile"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"name" VARCHAR(255) NOT NULL ENCODE lzo,
	"permissionsemailsingle" BOOLEAN NOT NULL ENCODE none,
	"permissionsemailmass" BOOLEAN NOT NULL ENCODE none,
	"permissionsedittask" BOOLEAN NOT NULL ENCODE none,
	"permissionseditevent" BOOLEAN NOT NULL ENCODE none,
	"permissionsexportreport" BOOLEAN NOT NULL ENCODE none,
	"permissionsimportpersonal" BOOLEAN NOT NULL ENCODE none,
	"permissionsdataexport" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageusers" BOOLEAN NOT NULL ENCODE none,
	"permissionseditpublictemplates" BOOLEAN NOT NULL ENCODE none,
	"permissionsmodifyalldata" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecases" BOOLEAN NOT NULL ENCODE none,
	"permissionsmassinlineedit" BOOLEAN NOT NULL ENCODE none,
	"permissionseditknowledge" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageknowledge" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagesolutions" BOOLEAN NOT NULL ENCODE none,
	"permissionscustomizeapplication" BOOLEAN NOT NULL ENCODE none,
	"permissionseditreadonlyfields" BOOLEAN NOT NULL ENCODE none,
	"permissionsrunreports" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewsetup" BOOLEAN NOT NULL ENCODE none,
	"permissionstransferanyentity" BOOLEAN NOT NULL ENCODE none,
	"permissionsnewreportbuilder" BOOLEAN NOT NULL ENCODE none,
	"permissionsactivatecontract" BOOLEAN NOT NULL ENCODE none,
	"permissionsimportleads" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageleads" BOOLEAN NOT NULL ENCODE none,
	"permissionstransferanylead" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewalldata" BOOLEAN NOT NULL ENCODE none,
	"permissionseditpublicdocuments" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewencrypteddata" BOOLEAN NOT NULL ENCODE none,
	"permissionseditbrandtemplates" BOOLEAN NOT NULL ENCODE none,
	"permissionsedithtmltemplates" BOOLEAN NOT NULL ENCODE none,
	"permissionschatterinternaluser" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagetranslation" BOOLEAN NOT NULL ENCODE none,
	"permissionsdeleteactivatedcontract" BOOLEAN NOT NULL ENCODE none,
	"permissionschatterinviteexternalusers" BOOLEAN NOT NULL ENCODE none,
	"permissionssendsitrequests" BOOLEAN NOT NULL ENCODE none,
	"permissionsapiuseronly" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageremoteaccess" BOOLEAN NOT NULL ENCODE none,
	"permissionscanusenewdashboardbuilder" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecategories" BOOLEAN NOT NULL ENCODE none,
	"permissionsconvertleads" BOOLEAN NOT NULL ENCODE none,
	"permissionspasswordneverexpires" BOOLEAN NOT NULL ENCODE none,
	"permissionsuseteamreassignwizards" BOOLEAN NOT NULL ENCODE none,
	"permissionsinstallmultiforce" BOOLEAN NOT NULL ENCODE none,
	"permissionspublishmultiforce" BOOLEAN NOT NULL ENCODE none,
	"permissionschatterowngroups" BOOLEAN NOT NULL ENCODE none,
	"permissionseditopplineitemunitprice" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatemultiforce" BOOLEAN NOT NULL ENCODE none,
	"permissionsbulkapiharddelete" BOOLEAN NOT NULL ENCODE none,
	"permissionsinboundmigrationtoolsuser" BOOLEAN NOT NULL ENCODE none,
	"permissionssolutionimport" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecallcenters" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagesynonyms" BOOLEAN NOT NULL ENCODE none,
	"permissionsoutboundmigrationtoolsuser" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewcontent" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageemailclientconfig" BOOLEAN NOT NULL ENCODE none,
	"permissionsenablenotifications" BOOLEAN NOT NULL ENCODE none,
	"permissionsisssoenabled" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagedataintegrations" BOOLEAN NOT NULL ENCODE none,
	"permissionsdistributefromperswksp" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewdatacategories" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagedatacategories" BOOLEAN NOT NULL ENCODE none,
	"permissionsauthorapex" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagemobile" BOOLEAN NOT NULL ENCODE none,
	"permissionsapienabled" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecustomreporttypes" BOOLEAN NOT NULL ENCODE none,
	"permissionseditcasecomments" BOOLEAN NOT NULL ENCODE none,
	"permissionstransferanycase" BOOLEAN NOT NULL ENCODE none,
	"permissionscontentadministrator" BOOLEAN NOT NULL ENCODE none,
	"permissionscreateworkspaces" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecontentpermissions" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecontentproperties" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecontenttypes" BOOLEAN NOT NULL ENCODE none,
	"permissionsschedulejob" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageexchangeconfig" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageanalyticsnapshots" BOOLEAN NOT NULL ENCODE none,
	"permissionsschedulereports" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagebusinesshourholidays" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagedynamicdashboards" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageinteraction" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewmyteamsdashboards" BOOLEAN NOT NULL ENCODE none,
	"permissionsmoderatechatter" BOOLEAN NOT NULL ENCODE none,
	"permissionsresetpasswords" BOOLEAN NOT NULL ENCODE none,
	"permissionsflowuflrequired" BOOLEAN NOT NULL ENCODE none,
	"permissionscaninsertfeedsystemfields" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageknowledgeimportexport" BOOLEAN NOT NULL ENCODE none,
	"permissionsemailtemplatemanagement" BOOLEAN NOT NULL ENCODE none,
	"permissionsemailadministration" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagechattermessages" BOOLEAN NOT NULL ENCODE none,
	"permissionschatterfilelink" BOOLEAN NOT NULL ENCODE none,
	"permissionsforcetwofactor" BOOLEAN NOT NULL ENCODE none,
	"permissionsvieweventlogfiles" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagenetworks" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageauthproviders" BOOLEAN NOT NULL ENCODE none,
	"permissionsrunflow" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatecustomizedashboards" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatedashboardfolders" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewpublicdashboards" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagedashbdsinpubfolders" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatecustomizereports" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatereportfolders" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewpublicreports" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagereportsinpubfolders" BOOLEAN NOT NULL ENCODE none,
	"permissionseditmydashboards" BOOLEAN NOT NULL ENCODE none,
	"permissionseditmyreports" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewallusers" BOOLEAN NOT NULL ENCODE none,
	"permissionsallowuniversalsearch" BOOLEAN NOT NULL ENCODE none,
	"permissionsconnectorgtoenvironmenthub" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatecustomizefilters" BOOLEAN NOT NULL ENCODE none,
	"permissionsgovernnetworks" BOOLEAN NOT NULL ENCODE none,
	"permissionssalesconsole" BOOLEAN NOT NULL ENCODE none,
	"permissionstwofactorapi" BOOLEAN NOT NULL ENCODE none,
	"permissionsdeletetopics" BOOLEAN NOT NULL ENCODE none,
	"permissionsedittopics" BOOLEAN NOT NULL ENCODE none,
	"permissionscreatetopics" BOOLEAN NOT NULL ENCODE none,
	"permissionsassigntopics" BOOLEAN NOT NULL ENCODE none,
	"permissionsidentityenabled" BOOLEAN NOT NULL ENCODE none,
	"permissionsidentityconnect" BOOLEAN NOT NULL ENCODE none,
	"permissionsallowviewknowledge" BOOLEAN NOT NULL ENCODE none,
	"permissionscreateworkbadgedefinition" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagesearchpromotionrules" BOOLEAN NOT NULL ENCODE none,
	"permissionscustommobileappsaccess" BOOLEAN NOT NULL ENCODE none,
	"permissionsviewhelplink" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageprofilespermissionsets" BOOLEAN NOT NULL ENCODE none,
	"permissionsassignpermissionsets" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageroles" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageipaddresses" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagesharing" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageinternalusers" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagepasswordpolicies" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageloginaccesspolicies" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagecustompermissions" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanageunlistedgroups" BOOLEAN NOT NULL ENCODE none,
	"permissionsmanagetwofactor" BOOLEAN NOT NULL ENCODE none,
	"permissionschatterforsharepoint" BOOLEAN NOT NULL ENCODE none,
	"permissionsdebugapex" BOOLEAN NOT NULL ENCODE none,
	"permissionslightningexperienceuser" BOOLEAN NOT NULL ENCODE none,
	"permissionsconfigcustomrecs" BOOLEAN NOT NULL ENCODE none,
	"permissionsshareinternalarticles" BOOLEAN NOT NULL ENCODE none,
	"permissionsmergetopics" BOOLEAN NOT NULL ENCODE none,
	"userlicenseid" VARCHAR(18) NOT NULL ENCODE lzo,
	"usertype" VARCHAR(40) ENCODE lzo,
	"createddate" TIMESTAMP NOT NULL ENCODE lzo,
	"createdbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"lastmodifieddate" TIMESTAMP NOT NULL ENCODE lzo,
	"lastmodifiedbyid" VARCHAR(18) NOT NULL ENCODE lzo,
	"systemmodstamp" TIMESTAMP NOT NULL ENCODE lzo,
	"isssoenabled" BOOLEAN NOT NULL ENCODE none,
	"description" VARCHAR(255) ENCODE lzo,
	"lastvieweddate" TIMESTAMP ENCODE lzo,
	"lastreferenceddate" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "stage"."sfdc_stage_companyschedule"
(
	"id_key" VARCHAR(50) NOT NULL ENCODE lzo,
	"region_key" NUMERIC(10, 0) NOT NULL ENCODE lzo,
	"book_key" NUMERIC(10, 0) NOT NULL ENCODE lzo,
	"pub_sequence" NUMERIC(10, 0) NOT NULL ENCODE lzo,
	"twp_database" VARCHAR(10) ENCODE lzo,
	"yb_database" VARCHAR(10) ENCODE lzo,
	"lawson_book_code" VARCHAR(10) ENCODE lzo,
	"office_name" VARCHAR(50) ENCODE lzo,
	"svp" VARCHAR(50) ENCODE lzo,
	"svp_direct_report" VARCHAR(50) ENCODE lzo,
	"national_sales_close" TIMESTAMP ENCODE lzo,
	"delivery_start" TIMESTAMP ENCODE lzo,
	"all_versions_distribution_qty" NUMERIC(10, 0) ENCODE lzo,
	"trim_size" VARCHAR(20) ENCODE lzo,
	"yellow_page_column_format" NUMERIC(10, 0) ENCODE lzo,
	"class_color" VARCHAR(30) ENCODE lzo,
	"new_book_indicator" VARCHAR(20) ENCODE lzo,
	"assignment_release" TIMESTAMP ENCODE lzo,
	"premium_renewal" TIMESTAMP ENCODE lzo,
	"credit_lockout" TIMESTAMP ENCODE lzo,
	"last_update_date" TIMESTAMP ENCODE lzo,
	"accounting_office_code" VARCHAR(50) ENCODE lzo,
	"month_book_hits_the_street" TIMESTAMP ENCODE lzo,
	"fy_billing_date" TIMESTAMP ENCODE lzo,
	"local_sales_start" TIMESTAMP ENCODE lzo,
	"pre_cmp_page_checks_completed" TIMESTAMP ENCODE lzo,
	"pre_cmp_pages_due_from_pindar" TIMESTAMP ENCODE lzo,
	"printer" VARCHAR(50) ENCODE lzo,
	"printing_plant" VARCHAR(50) ENCODE lzo,
	"binding_plant" VARCHAR(50) ENCODE lzo,
	"printer_ship_date" TIMESTAMP ENCODE lzo,
	"num_days_for_95_pcnt_distrib" NUMERIC(18, 0) ENCODE lzo,
	"forecasted_delivery_95_pcnt" TIMESTAMP ENCODE lzo,
	"director_vendor_manager" VARCHAR(50) ENCODE lzo,
	"sr_manager_vendor_company" VARCHAR(50) ENCODE lzo,
	"num_of_trucks" NUMERIC(18, 0) ENCODE lzo,
	"distribution_carrier" VARCHAR(50) ENCODE lzo,
	"version_a_large_book_distrib" NUMERIC(18, 0) ENCODE lzo,
	"version_b_mini_hardbacks" NUMERIC(18, 0) ENCODE lzo,
	"competitor_book_comes_out" VARCHAR(20) ENCODE lzo,
	"cover_tonnage" NUMERIC(18, 0) ENCODE lzo,
	"text_tonnage" NUMERIC(18, 0) ENCODE lzo,
	"prev_fy_revenue_recognition" TIMESTAMP ENCODE lzo,
	"white_pages_listing_format" VARCHAR(50) ENCODE lzo,
	"pre_cmp" TIMESTAMP ENCODE lzo,
	"publishing_center" VARCHAR(50) ENCODE lzo,
	"book_indicator" VARCHAR(20) ENCODE lzo,
	"company_schedule_book_name" VARCHAR(120) ENCODE lzo,
	"credit_lockout_date" TIMESTAMP ENCODE lzo,
	"last_proof_amend_cmp_date" TIMESTAMP ENCODE lzo,
	"last_proof_mailed_date" TIMESTAMP ENCODE lzo,
	"fil_cmp_class_pge_due_twp_out" TIMESTAMP ENCODE lzo,
	"pindar_badg_code" VARCHAR(20) ENCODE lzo,
	"listing_center" VARCHAR(50) ENCODE lzo,
	"white_page_order_listings_date" TIMESTAMP ENCODE lzo,
	"white_page_tapes_arrive_date" TIMESTAMP ENCODE lzo,
	"primary_cover_title" VARCHAR(255) ENCODE lzo,
	"telco_involvement" VARCHAR(120) ENCODE lzo,
	"wide_area_coverage_headings" VARCHAR(50) ENCODE lzo,
	"yellow_pages_special_notes" VARCHAR(255) ENCODE lzo,
	"white_page_masthead_pri_title" VARCHAR(255) ENCODE lzo,
	"white_page_town_designators" VARCHAR(20) ENCODE lzo,
	"white_page_zip_codes" VARCHAR(20) ENCODE lzo,
	"suppress_white_page_zip_codes" VARCHAR(20) ENCODE lzo,
	"white_page_area_codes" VARCHAR(20) ENCODE lzo,
	"suppress_white_page_area_code" VARCHAR(20) ENCODE lzo,
	"marketing_manager" VARCHAR(30) ENCODE lzo,
	"primary_book_name" VARCHAR(120) ENCODE lzo,
	"calendar_year" TIMESTAMP ENCODE lzo,
	"cs_book_code" VARCHAR(7) ENCODE lzo,
	"cs_book_year" VARCHAR(4) ENCODE lzo,
	"cs_fiscal_year" VARCHAR(4) ENCODE lzo,
	"cs_recognition_date" TIMESTAMP ENCODE lzo,
	"cs_start_canvass_date" TIMESTAMP ENCODE lzo,
	"cs_closing_date" TIMESTAMP ENCODE lzo,
	"cs_start_billing_date" TIMESTAMP ENCODE lzo,
	"cs_publication_date" TIMESTAMP ENCODE lzo,
	"cs_publishing_bill_date" TIMESTAMP ENCODE lzo,
	"cs_listings_correct_cmp_date" TIMESTAMP ENCODE lzo,
	"pre_pre_cmp_date" TIMESTAMP ENCODE lzo,
	"coupon_seniority_due_date" TIMESTAMP ENCODE lzo,
	"projected_class_page_count" NUMERIC(18, 0) ENCODE lzo,
	"frst_cls_flt_ships_to_prnt_dte" TIMESTAMP ENCODE lzo,
	"num_of_flights" NUMERIC(18, 0) ENCODE lzo,
	"projected_alpha_listing_count" NUMERIC(18, 0) ENCODE lzo,
	"pg_cnt_sec_inf_due_mfg_dpt_dte" TIMESTAMP ENCODE lzo,
	"bulk_download" TIMESTAMP ENCODE lzo,
	"sales_query_handoff_date" TIMESTAMP ENCODE lzo,
	"last_proof_amend" TIMESTAMP ENCODE lzo,
	"diad_market_buy_code" VARCHAR(5) ENCODE lzo,
	"active" NUMERIC(18, 0) ENCODE lzo,
	"bill_number_of_months" NUMERIC(10, 0) ENCODE lzo,
	"pub_number_of_months" NUMERIC(10, 0) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_stage_companyschedule"
ADD CONSTRAINT "sfdc_stage_companyschedule_pkey"
PRIMARY KEY ("id_key");



CREATE TABLE "stage"."sfdc_stage_diad_product_asset"
(
	"hibu_xref_id" NUMERIC(38, 0) ENCODE lzo,
	"product_name" VARCHAR(100) ENCODE lzo,
	"product_family" VARCHAR(100) ENCODE lzo,
	"product_start_date" VARCHAR(29) ENCODE lzo,
	"product_end_date" VARCHAR(29) ENCODE lzo,
	"product_sold_year" CHAR(4) ENCODE bytedict,
	"product_status" VARCHAR(20) ENCODE bytedict,
	"product_monthly_value" NUMERIC(16, 2) ENCODE lzo,
	"product_one_time_fees" NUMERIC(16, 2) ENCODE lzo,
	"product_total_sale_amount" NUMERIC(16, 2) ENCODE lzo,
	"contract_reference_link" VARCHAR(500) ENCODE lzo,
	"email_address" VARCHAR(200) ENCODE lzo,
	"url" VARCHAR(500) ENCODE lzo,
	"renewal_type" VARCHAR(20) ENCODE lzo,
	"rate_card" VARCHAR(20) ENCODE bytedict,
	"payment_term" VARCHAR(20) ENCODE bytedict,
	"payment_term_description" VARCHAR(200) ENCODE lzo,
	"prior_contract" VARCHAR(20) ENCODE lzo,
	"renewal_contract" VARCHAR(20) ENCODE lzo,
	"drop_date" VARCHAR(29) ENCODE lzo,
	"sale_date" VARCHAR(29) ENCODE lzo,
	"product_received_date" VARCHAR(29) ENCODE lzo,
	"sold_sales_rep_code" VARCHAR(20) ENCODE lzo,
	"assigned_sales_rep_code" VARCHAR(20) ENCODE lzo,
	"externalaccountkey" VARCHAR(71) ENCODE lzo,
	"contract_key" NUMERIC(38, 0) ENCODE mostly32,
	"customer_key" NUMERIC(38, 0) ENCODE mostly32,
	"heading_key" NUMERIC(38, 0) ENCODE delta32k,
	"sales_rep_key" NUMERIC(38, 0) ENCODE lzo,
	"region_key" NUMERIC(38, 0) ENCODE delta,
	"udac_key" NUMERIC(38, 0) ENCODE bytedict,
	"asset_code" VARCHAR(40) ENCODE text255,
	"asset_description" VARCHAR(200) ENCODE lzo,
	"asset_code_type" VARCHAR(20) ENCODE lzo,
	"listing_number" VARCHAR(4) ENCODE bytedict,
	"line_item_uuid" VARCHAR(40) ENCODE lzo,
	"heading_code" VARCHAR(20) ENCODE lzo,
	"art_id" VARCHAR(20) ENCODE lzo,
	"discount_code" VARCHAR(20) ENCODE bytedict,
	"monthly_cost" NUMERIC(16, 2) ENCODE lzo,
	"seniority_date" VARCHAR(29) ENCODE lzo,
	"product_number_of_months" NUMERIC(38, 0) ENCODE lzo,
	"book_key" NUMERIC(38, 0) ENCODE delta32k,
	"pub_sequence" NUMERIC(38, 0) ENCODE bytedict,
	"bundle_uuid" VARCHAR(40) ENCODE lzo,
	"bottom_line_discount" NUMERIC(16, 2) ENCODE lzo,
	"last_update_timestamp" VARCHAR(29) ENCODE bytedict,
	"last_update_user" VARCHAR(20) ENCODE lzo,
	"product_entry_date" VARCHAR(29) ENCODE lzo,
	"renewal_status" VARCHAR(20) ENCODE bytedict,
	"heading_description" VARCHAR(200) ENCODE lzo,
	"book_year" VARCHAR(5) ENCODE bytedict,
	"book_code" VARCHAR(7) ENCODE lzo,
	"external_asset_id" VARCHAR(100) NOT NULL ENCODE lzo,
	"external_product_id" VARCHAR(100) ENCODE lzo,
	"sales_rep_assign_name" VARCHAR(60) ENCODE lzo,
	"sales_rep_sold_name" VARCHAR(60) ENCODE lzo,
	"reservation_sys_id" VARCHAR(10) ENCODE lzo,
	"line_item" NUMERIC(38, 0) ENCODE bytedict,
	"renewal_start_date" VARCHAR(29) ENCODE lzo,
	"renewal_closing_date" VARCHAR(29) ENCODE lzo,
	"renewal_credit_lockout_date" VARCHAR(29) ENCODE lzo,
	"primary_market_code" VARCHAR(100) ENCODE lzo,
	"primary_market_desc" VARCHAR(100) ENCODE lzo,
	"renewal_state" VARCHAR(10) ENCODE bytedict,
	"cycle_year" CHAR(4) ENCODE bytedict
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_stage_diad_product_asset"
ADD CONSTRAINT "sfdc_stage_diad_product_asset_pkey"
PRIMARY KEY ("external_asset_id");



CREATE TABLE "stage"."sfdc_stage_oh_product_asset"
(
	"hibu_xref_id" NUMERIC(38, 0) ENCODE lzo,
	"product_name" VARCHAR(100) ENCODE lzo,
	"product_family" VARCHAR(100) ENCODE lzo,
	"product_start_date" VARCHAR(29) ENCODE lzo,
	"product_end_date" VARCHAR(29) ENCODE lzo,
	"product_sales_cycle_year" CHAR(4) ENCODE lzo,
	"product_status" VARCHAR(20) ENCODE bytedict,
	"product_monthly_value" NUMERIC(16, 2) ENCODE lzo,
	"product_one_time_fees" NUMERIC(16, 2) ENCODE bytedict,
	"product_total_sale_amount" NUMERIC(16, 2) ENCODE bytedict,
	"contract_reference_link" VARCHAR(500) ENCODE lzo,
	"email_address" VARCHAR(200) ENCODE lzo,
	"url" VARCHAR(500) ENCODE lzo,
	"renewal_type" VARCHAR(20) ENCODE lzo,
	"rate_card" VARCHAR(20) ENCODE lzo,
	"payment_term" VARCHAR(20) ENCODE lzo,
	"payment_term_description" VARCHAR(200) ENCODE lzo,
	"prior_contract" VARCHAR(20) ENCODE lzo,
	"renewal_contract" NUMERIC(10, 0) ENCODE lzo,
	"drop_date" VARCHAR(29) ENCODE lzo,
	"sale_date" VARCHAR(29) ENCODE lzo,
	"product_received_date" VARCHAR(29) ENCODE lzo,
	"sold_sales_rep_code" VARCHAR(20) ENCODE lzo,
	"assigned_sales_rep_code" VARCHAR(20) ENCODE lzo,
	"externalaccountkey" VARCHAR(71) ENCODE lzo,
	"asset_code" VARCHAR(40) ENCODE lzo,
	"asset_description" VARCHAR(200) ENCODE lzo,
	"asset_code_type" VARCHAR(20) ENCODE lzo,
	"listing_number" VARCHAR(4) ENCODE bytedict,
	"heading_code" VARCHAR(20) ENCODE lzo,
	"art_id" VARCHAR(20) ENCODE lzo,
	"discount_code" VARCHAR(20) ENCODE bytedict,
	"monthly_cost" NUMERIC(16, 2) ENCODE bytedict,
	"seniority_date" VARCHAR(29) ENCODE lzo,
	"product_number_of_months" NUMERIC(10, 0) ENCODE lzo,
	"xpk_mpe_standard_order" NUMERIC(38, 0) ENCODE mostly32,
	"xpk_mpe_line_items" NUMERIC(38, 0) ENCODE lzo,
	"xpk_mpe_childitems" NUMERIC(38, 0) ENCODE lzo,
	"product_entry_date" VARCHAR(29) ENCODE lzo,
	"bundle_uuid" VARCHAR(40) ENCODE lzo,
	"renewal_status" VARCHAR(20) ENCODE lzo,
	"heading_description" VARCHAR(200) ENCODE lzo,
	"last_update_timestamp" VARCHAR(29) ENCODE lzo,
	"last_update_user" VARCHAR(20) ENCODE lzo,
	"asset_sales_rep_code" CHAR(10) ENCODE lzo,
	"mpe_order_number" VARCHAR(1000) ENCODE lzo,
	"guid" VARCHAR(38) ENCODE lzo,
	"external_asset_id" VARCHAR(100) NOT NULL ENCODE lzo,
	"asset_id" BIGINT ENCODE lzo,
	"parent_asset_id" BIGINT ENCODE lzo,
	"external_product_id" VARCHAR(100) ENCODE lzo,
	"mpe_billing_type" CHAR(1) ENCODE lzo,
	"mpe_billingperiod" VARCHAR(1000) ENCODE lzo,
	"mpe_total_price" NUMERIC(18, 3) ENCODE bytedict,
	"sales_rep_assign_name" VARCHAR(60) ENCODE lzo,
	"sales_rep_sold_name" VARCHAR(60) ENCODE lzo,
	"region_name" VARCHAR(50) ENCODE lzo,
	"cons_comp_date" VARCHAR(29) ENCODE lzo,
	"mpe_discountamount" NUMERIC(18, 9) ENCODE bytedict,
	"mpe_discountduration" NUMERIC(38, 0) ENCODE lzo,
	"book_code" VARCHAR(7) ENCODE bytedict,
	"book_year" VARCHAR(5) ENCODE lzo,
	"clipper_code" VARCHAR(3) ENCODE bytedict,
	"trans_action" VARCHAR(10) ENCODE lzo,
	"trans_end_action" VARCHAR(10) ENCODE bytedict,
	"asset_monthly_cost" NUMERIC(16, 2) ENCODE bytedict,
	"monthly_cost_discount" NUMERIC(16, 2) ENCODE bytedict,
	"monthly_cost_dis" NUMERIC(16, 2) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_stage_oh_product_asset"
ADD CONSTRAINT "sfdc_stage_oh_product_asset_pkey"
PRIMARY KEY ("external_asset_id");



CREATE TABLE "stage"."sfdc_user"
(
	"id" VARCHAR(18) NOT NULL ENCODE lzo,
	"username" VARCHAR(80) ENCODE lzo,
	"lastname" VARCHAR(80) ENCODE lzo,
	"firstname" VARCHAR(40) ENCODE lzo,
	"name" VARCHAR(120) ENCODE lzo,
	"companyname" VARCHAR(80) ENCODE lzo,
	"division" VARCHAR(80) ENCODE lzo,
	"department" VARCHAR(80) ENCODE lzo,
	"title" VARCHAR(80) ENCODE lzo,
	"street" VARCHAR(255) ENCODE lzo,
	"city" VARCHAR(40) ENCODE lzo,
	"state" VARCHAR(80) ENCODE lzo,
	"postalcode" VARCHAR(20) ENCODE lzo,
	"country" VARCHAR(80) ENCODE lzo,
	"statecode" VARCHAR(3) ENCODE lzo,
	"countrycode" VARCHAR(16) ENCODE lzo,
	"email" VARCHAR(128) ENCODE lzo,
	"phone" VARCHAR(40) ENCODE lzo,
	"fax" VARCHAR(40) ENCODE lzo,
	"mobilephone" VARCHAR(40) ENCODE lzo,
	"alias" VARCHAR(10) ENCODE lzo,
	"isactive" NUMERIC(1, 0) ENCODE lzo,
	"timezonesidkey" VARCHAR(40) ENCODE lzo,
	"userroleid" VARCHAR(18) ENCODE lzo,
	"localesidkey" VARCHAR(40) ENCODE lzo,
	"profileid" VARCHAR(18) ENCODE lzo,
	"usertype" VARCHAR(40) ENCODE lzo,
	"languagelocalekey" VARCHAR(40) ENCODE lzo,
	"employeenumber" VARCHAR(20) ENCODE lzo,
	"delegatedapproverid" VARCHAR(18) ENCODE lzo,
	"managerid" VARCHAR(18) ENCODE lzo,
	"lastlogindate" VARCHAR(80) ENCODE lzo,
	"createddate" VARCHAR(80) ENCODE lzo,
	"createdbyid" NUMERIC(18, 0) ENCODE lzo,
	"lastmodifieddate" VARCHAR(80) ENCODE lzo,
	"lastmodifiedbyid" NUMERIC(18, 0) ENCODE lzo,
	"systemmodstamp" VARCHAR(80) ENCODE lzo,
	"contactid" VARCHAR(18) ENCODE lzo,
	"accountid" VARCHAR(18) ENCODE lzo,
	"callcenterid" VARCHAR(18) ENCODE lzo,
	"extension" VARCHAR(40) ENCODE lzo,
	"federationidentifier" VARCHAR(512) ENCODE lzo,
	"db_region__c" NUMERIC(4, 0) ENCODE lzo,
	"alignment_terralign__c" VARCHAR(80) ENCODE lzo,
	"channel__c" VARCHAR(80) ENCODE lzo,
	"geo__c" VARCHAR(80) ENCODE lzo,
	"market__c" VARCHAR(80) ENCODE lzo,
	"region__c" VARCHAR(80) ENCODE lzo,
	"sub_channel__c" VARCHAR(80) ENCODE lzo,
	"territory_terralign__c" VARCHAR(80) ENCODE lzo,
	"zone__c" VARCHAR(80) ENCODE lzo,
	"country_text__c" VARCHAR(50) ENCODE lzo,
	"employee_id__c" VARCHAR(40) ENCODE lzo,
	"state_text__c" VARCHAR(50) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."sfdc_user"
ADD CONSTRAINT "sfdc_user_pkey"
PRIMARY KEY ("id");



CREATE TABLE "stage"."stg_employee"
(
	"employee_id" VARCHAR(50) ENCODE lzo,
	"manager_id" VARCHAR(50) ENCODE lzo,
	"comp_position_code" VARCHAR(50) ENCODE lzo,
	"ultipro_job_code" VARCHAR(50) ENCODE lzo,
	"last_name" VARCHAR(50) ENCODE lzo,
	"first_name" VARCHAR(50) ENCODE lzo,
	"business_email" VARCHAR(100) ENCODE lzo,
	"division_code" VARCHAR(50) ENCODE lzo,
	"division_name" VARCHAR(100) ENCODE lzo,
	"company_code" VARCHAR(50) ENCODE lzo,
	"department_code" VARCHAR(50) ENCODE lzo,
	"department_name" VARCHAR(100) ENCODE lzo,
	"employee_type" VARCHAR(50) ENCODE lzo,
	"employee_status" VARCHAR(10) ENCODE lzo,
	"original_hire_date" VARCHAR(50) ENCODE lzo,
	"termination_date" VARCHAR(50) ENCODE lzo,
	"last_hire_date" VARCHAR(50) ENCODE lzo,
	"date_in_job" VARCHAR(50) ENCODE lzo,
	"employment_agreement_date" VARCHAR(50) ENCODE lzo,
	"comp_agreement_return_date" VARCHAR(50) ENCODE lzo,
	"integration_effective_date" VARCHAR(50) ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "stage"."stg_product_family_map"
(
	"product_code" VARCHAR(50) ENCODE lzo,
	"product_desc" VARCHAR(255) ENCODE lzo,
	"product_type" VARCHAR(255) ENCODE lzo,
	"product_category" VARCHAR(50) ENCODE lzo,
	"product_sub_category" VARCHAR(50) ENCODE lzo,
	"item_type" VARCHAR(50) ENCODE lzo,
	"product_family" VARCHAR(50) ENCODE lzo,
	"last_update_date" DATE ENCODE lzo,
	"insert_date" DATE ENCODE lzo,
	"product_sub_category_02" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "stage"."stg_product_hierarchy"
(
	"product_code" VARCHAR(50) ENCODE lzo,
	"product_desc" VARCHAR(255) ENCODE lzo,
	"product_type" VARCHAR(255) ENCODE lzo,
	"product_category" VARCHAR(50) ENCODE lzo,
	"product_sub_category" VARCHAR(50) ENCODE lzo,
	"item_type" VARCHAR(50) ENCODE lzo,
	"product_family" VARCHAR(50) ENCODE lzo,
	"product_sub_category_02" VARCHAR(255) ENCODE lzo,
	"anaplan_l4_product" VARCHAR(50) ENCODE lzo,
	"anaplan_l3_product" VARCHAR(50) ENCODE lzo,
	"anaplan_l2_product" VARCHAR(50) ENCODE lzo
)
DISTSTYLE EVEN;


CREATE TABLE "stage"."ultipro_businessrulejob"
(
	"jobcode" VARCHAR(255) NOT NULL ENCODE lzo,
	"jobdescription" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."ultipro_businessrulejob"
ADD CONSTRAINT "ultipro_businessrulejob_pkey"
PRIMARY KEY ("jobcode");



CREATE TABLE "stage"."ultipro_employee"
(
	"email" VARCHAR(255) ENCODE lzo,
	"employeeid" VARCHAR(255) NOT NULL ENCODE lzo,
	"otherpersonalfield06" TIMESTAMP ENCODE lzo,
	"otherpersonalfield12" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."ultipro_employee"
ADD CONSTRAINT "ultipro_employee_pkey"
PRIMARY KEY ("employeeid");



CREATE TABLE "stage"."ultipro_employment"
(
	"companycode" VARCHAR(255) NOT NULL ENCODE lzo,
	"dateinjob" TIMESTAMP ENCODE lzo,
	"employeeid" VARCHAR(255) NOT NULL ENCODE lzo,
	"employeenumber" VARCHAR(255) ENCODE lzo,
	"employeetype" VARCHAR(255) ENCODE lzo,
	"employmentstatus" VARCHAR(255) ENCODE lzo,
	"jobcode" VARCHAR(255) ENCODE lzo,
	"lasthire" TIMESTAMP ENCODE lzo,
	"orglevel1" VARCHAR(255) ENCODE lzo,
	"orglevel1description" VARCHAR(255) ENCODE lzo,
	"orglevel2" VARCHAR(255) ENCODE lzo,
	"orglevel2description" VARCHAR(255) ENCODE lzo,
	"originalhire" TIMESTAMP ENCODE lzo,
	"othercompanyfield02" VARCHAR(255) ENCODE lzo,
	"supervisoremployeenumber" VARCHAR(255) ENCODE lzo,
	"supervisorlastname" VARCHAR(255) ENCODE lzo,
	"terminationdate" TIMESTAMP ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."ultipro_employment"
ADD CONSTRAINT "ultipro_employment_pkey"
PRIMARY KEY ("companycode", "employeeid");



CREATE TABLE "stage"."ultipro_employmenthistory"
(
	"companycode" VARCHAR(255) ENCODE lzo,
	"datetimecreated" TIMESTAMP ENCODE lzo,
	"employeeid" VARCHAR(255) ENCODE lzo,
	"employeetype" VARCHAR(255) ENCODE lzo,
	"employmentstatus" VARCHAR(255) ENCODE lzo,
	"integrationeffectivedate" TIMESTAMP ENCODE lzo,
	"jobcode" VARCHAR(255) ENCODE lzo,
	"jobeffectivedate" TIMESTAMP ENCODE lzo,
	"supervisorid" VARCHAR(255) ENCODE lzo,
	"supervisorlastname" VARCHAR(255) ENCODE lzo,
	"systemid" VARCHAR(255) NOT NULL ENCODE lzo,
	"orglevel" VARCHAR(255) ENCODE lzo,
	"orgleve1description" VARCHAR(255) ENCODE lzo,
	"orglevel2" VARCHAR(255) ENCODE lzo,
	"orglevel2description" VARCHAR(255) ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."ultipro_employmenthistory"
ADD CONSTRAINT "ultipro_employmenthistory_pkey"
PRIMARY KEY ("systemid");



CREATE TABLE "stage"."ultipro_person"
(
	"employeeid" VARCHAR(255) ENCODE lzo,
	"firstname" VARCHAR(255) ENCODE lzo,
	"lastname" VARCHAR(255) ENCODE lzo,
	"personid" VARCHAR(255) NOT NULL ENCODE lzo
)
DISTSTYLE EVEN;

ALTER TABLE "stage"."ultipro_person"
ADD CONSTRAINT "ultipro_person_pkey"
PRIMARY KEY ("personid");



CREATE OR REPLACE VIEW "stage"."view_account_history_owner_all" AS 
( SELECT view_account_history_owner_first.business_id, view_account_history_owner_first.employee_id, view_account_history_owner_first.start_date
   FROM stage.view_account_history_owner_first
UNION 
 SELECT view_account_history_owner_change.business_id, view_account_history_owner_change.employee_id, view_account_history_owner_change.start_date
   FROM stage.view_account_history_owner_change)
UNION 
 SELECT view_account_history_owner_never.business_id, view_account_history_owner_never.employee_id, view_account_history_owner_never.start_date
   FROM stage.view_account_history_owner_never;


CREATE OR REPLACE VIEW "stage"."view_account_history_owner_change" AS 
 SELECT b.business_id, b.employee_id, b.start_date
   FROM stage.view_account_owner_by_day b
   JOIN ( SELECT b1.business_id, "max"(b1.start_datetime) AS max_start_datetime, b1.start_date
           FROM stage.view_account_owner_by_day b1
          GROUP BY b1.business_id, b1.start_date) b2 ON b.business_id = b2.business_id AND b.start_datetime = b2.max_start_datetime
  ORDER BY b.business_id, b.start_date;


CREATE OR REPLACE VIEW "stage"."view_account_history_owner_first" AS 
 SELECT c.business_id_hidden__c AS business_id, min(u2.employee_id__c::text)::character varying AS employee_id, c.createddate::date - 21 AS start_date
   FROM stage.view_account_history_owner_no_oldvalue_una999 b
   JOIN ( SELECT view_account_history_owner_no_oldvalue_una999.accountid, min(view_account_history_owner_no_oldvalue_una999.createddate) AS mincreateddate
           FROM stage.view_account_history_owner_no_oldvalue_una999
          GROUP BY view_account_history_owner_no_oldvalue_una999.accountid) a ON b.accountid::text = a.accountid::text AND b.createddate = a.mincreateddate
   JOIN stage.sfdc_master_customer c ON b.accountid::text = c.id::text
   JOIN stage.sfdc_user u2 ON b.oldvalue::text = u2.id::text
  WHERE NOT (EXISTS ( SELECT 1
   FROM stage.view_account_history_owner_change b
  WHERE c.business_id_hidden__c = b.business_id AND c.createddate::date = b.start_date))
  GROUP BY c.business_id_hidden__c, c.createddate::date - 21;


CREATE OR REPLACE VIEW "stage"."view_account_history_owner_never" AS 
 SELECT c.business_id_hidden__c AS business_id, u.employee_id__c AS employee_id, c.createddate::date - 21 AS start_date
   FROM stage.sfdc_master_customer c
   JOIN stage.sfdc_user u ON c.ownerid::text = u.id::text
  WHERE NOT (EXISTS ( SELECT 1
      FROM stage.view_account_history_owner_no_oldvalue_una999 b1
   JOIN stage.sfdc_master_customer c1 ON b1.accountid::text = c1.id::text
  WHERE c1.business_id_hidden__c = c.business_id_hidden__c)) AND u.employee_id__c::text <> 'UNA999'::character varying::text;


CREATE OR REPLACE VIEW "stage"."view_account_history_owner_no_newvalue_una999" AS 
 SELECT sfdc_account_history_owner.id, sfdc_account_history_owner.isdeleted, sfdc_account_history_owner.accountid, sfdc_account_history_owner.createdbyid, date_add('H'::character varying::text, - 5::bigint, sfdc_account_history_owner.createddate) AS createddate, sfdc_account_history_owner.field, sfdc_account_history_owner.oldvalue, sfdc_account_history_owner.newvalue
   FROM stage.sfdc_account_history_owner
  WHERE sfdc_account_history_owner.newvalue::text <> '005i0000003SASwAAO'::character varying::text AND date_add('H'::character varying::text, - 5::bigint, sfdc_account_history_owner.createddate) < 'now'::character varying::date;


CREATE OR REPLACE VIEW "stage"."view_account_history_owner_no_oldvalue_una999" AS 
 SELECT sfdc_account_history_owner.id, sfdc_account_history_owner.isdeleted, sfdc_account_history_owner.accountid, sfdc_account_history_owner.createdbyid, date_add('H'::character varying::text, - 5::bigint, sfdc_account_history_owner.createddate) AS createddate, sfdc_account_history_owner.field, sfdc_account_history_owner.oldvalue, sfdc_account_history_owner.newvalue
   FROM stage.sfdc_account_history_owner
  WHERE sfdc_account_history_owner.oldvalue::text <> '005i0000003SASwAAO'::character varying::text AND date_add('H'::character varying::text, - 5::bigint, sfdc_account_history_owner.createddate) < 'now'::character varying::date;


CREATE OR REPLACE VIEW "stage"."view_account_owner_by_day" AS 
 SELECT c.business_id_hidden__c AS business_id, "max"(u1.employee_id__c::text)::character varying AS employee_id, a.createddate AS start_datetime, a.createddate::date AS start_date
   FROM stage.view_account_history_owner_no_newvalue_una999 a
   JOIN stage.sfdc_master_customer c ON a.accountid::text = c.id::text
   JOIN stage.sfdc_user u1 ON a.newvalue::text = u1.id::text AND NOT u1.employee_id__c IS NULL
  GROUP BY c.business_id_hidden__c, a.createddate, a.createddate::date;


CREATE OR REPLACE VIEW "stage"."view_account_owner_update" AS 
 SELECT ao.business_id, ao.employee_id, ao.start_date, min(ao1.start_date) AS end_date
   FROM account_owner ao
   JOIN account_owner ao1 ON ao.business_id = ao1.business_id AND ao.start_date < ao1.start_date AND ao.end_date IS NULL
  WHERE (ao.business_id IN ( SELECT a.business_id
      FROM ( SELECT count(*) AS count, ao.business_id
              FROM account_owner ao
         JOIN customer c ON c.business_id = ao.business_id
        WHERE ao.end_date IS NULL
        GROUP BY ao.business_id
       HAVING count(*) > 1) a))
  GROUP BY ao.business_id, ao.employee_id, ao.start_date;


CREATE OR REPLACE VIEW "stage"."view_account_owner_upsert" AS 
 SELECT a1.business_id, a1.employee_id, a1.start_date, min(a2.start_date) AS end_date
   FROM stage.view_account_history_owner_all a1
   LEFT JOIN stage.view_account_history_owner_all a2 ON a1.business_id = a2.business_id AND a1.start_date < a2.start_date
  WHERE a1.business_id IS NOT NULL AND a1.employee_id IS NOT NULL AND a1.start_date IS NOT NULL
  GROUP BY a1.business_id, a1.employee_id, a1.start_date;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_detect_changes" AS 
 SELECT src.business_id, src.product, src.bill_date, src.selling_transaction_id, src.invoice_number, src.primary_rep, src.secondary_rep, src.tertiary_rep, src.source_system, src.full_billing_amount, src.prorated_billing_amount, src.one_time_fees, src.spread_fees, src.initial_load_date, src.update_date, src.first_bill, 
        CASE
            WHEN pub.billing_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.billing_transaction_id IS NULL AND (
            CASE
                WHEN src.business_id = pub.business_id OR src.business_id IS NULL AND pub.business_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.product::text = pub.product::text OR src.product IS NULL AND pub.product IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.bill_date = pub.bill_date OR src.bill_date IS NULL AND pub.bill_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_rep::text = pub.primary_rep::text OR src.primary_rep IS NULL AND pub.primary_rep IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.source_system::text = pub.source_system::text OR src.source_system IS NULL AND pub.source_system IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.full_billing_amount = pub.full_billing_amount OR src.full_billing_amount IS NULL AND pub.full_billing_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.prorated_billing_amount::character varying::text = pub.prorated_billing_amount::character varying::text OR src.prorated_billing_amount IS NULL AND pub.prorated_billing_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees::character varying::text = pub.one_time_fees::character varying::text OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees::character varying::text = pub.spread_fees::character varying::text OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.first_bill::character varying::text = pub.first_bill::character varying::text OR src.first_bill IS NULL AND pub.first_bill IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.billing_transaction_id
   FROM stage.billing_transaction_stage src
   LEFT JOIN billing_transaction pub ON src.business_id = pub.business_id AND src.product::text = pub.product::text AND src.bill_date = pub.bill_date AND src.invoice_number::text = pub.invoice_number::text AND src.source_system::text = pub.source_system::text
  WHERE src.source_system::text = 'SAP'::character varying::text
UNION 
 SELECT src.business_id, src.product, src.bill_date, src.selling_transaction_id, src.invoice_number, src.primary_rep, src.secondary_rep, src.tertiary_rep, src.source_system, src.full_billing_amount, src.prorated_billing_amount, src.one_time_fees, src.spread_fees, src.initial_load_date, src.update_date, src.first_bill, 
        CASE
            WHEN pub.billing_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.billing_transaction_id IS NULL AND (
            CASE
                WHEN src.business_id = pub.business_id OR src.business_id IS NULL AND pub.business_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.product::text = pub.product::text OR src.product IS NULL AND pub.product IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_rep::text = pub.primary_rep::text OR src.primary_rep IS NULL AND pub.primary_rep IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.source_system::text = pub.source_system::text OR src.source_system IS NULL AND pub.source_system IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.full_billing_amount = pub.full_billing_amount OR src.full_billing_amount IS NULL AND pub.full_billing_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.prorated_billing_amount::character varying::text = pub.prorated_billing_amount::character varying::text OR src.prorated_billing_amount IS NULL AND pub.prorated_billing_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees::character varying::text = pub.one_time_fees::character varying::text OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees::character varying::text = pub.spread_fees::character varying::text OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.first_bill::character varying::text = pub.first_bill::character varying::text OR src.first_bill IS NULL AND pub.first_bill IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.billing_transaction_id
   FROM stage.billing_transaction_stage src
   LEFT JOIN billing_transaction pub ON src.business_id = pub.business_id AND src.product::text = pub.product::text AND src.bill_date = pub.bill_date AND src.source_system::text = pub.source_system::text AND src.selling_transaction_id = pub.selling_transaction_id
  WHERE src.source_system::text = 'DIAD'::character varying::text;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_diad_print" AS 
 SELECT derived_table.hibu_xref_id AS business_id, derived_table.book_code__c AS product, derived_table.product_start_date, derived_table.name AS sfdc_book_name, cal.calendar_date AS bill_date, derived_table.monthly_spend__c AS full_billing_amount, 
        CASE
            WHEN cal.calendar_date = derived_table.product_start_date::timestamp without time zone THEN '1'::character varying
            ELSE '0'::character varying
        END AS first_bill
   FROM ( SELECT DISTINCT p.name, p.book_code__c, pa.hibu_xref_id, pa.book_key, pa.region_key, pa.pub_sequence, pa.product_start_date::date AS product_start_date, pa.product_end_date::date AS product_end_date, cs.bill_number_of_months, p.monthly_spend__c, p.total_sale__c
           FROM stage.sfdc_product p
      JOIN stage.sfdc_stage_diad_product_asset pa ON p.business_id_hidden__c::numeric::numeric(18,0) = pa.hibu_xref_id AND p.go_live_date__c = pa.product_start_date::date AND p.name::text = pa.product_name::text
   JOIN stage.sfdc_stage_companyschedule cs ON pa.region_key = cs.region_key AND pa.book_key = cs.book_key AND pa.pub_sequence = cs.pub_sequence
  WHERE p.product_family__c::text = 'Yellow Pages'::character varying::text AND p.go_live_date__c >= '2016-01-01'::date AND p.go_live_date__c < 'now'::character varying::date
  ORDER BY pa.product_start_date) derived_table
   JOIN stage.calendar_temp cal ON "date_part"('day'::character varying::text, derived_table.product_start_date) = "date_part"('day'::character varying::text, cal.calendar_date)
  WHERE cal.calendar_date >= derived_table.product_start_date AND cal.calendar_date <= derived_table.product_end_date AND cal.calendar_date < derived_table.product_end_date AND derived_table.monthly_spend__c <> 0::numeric(18,2)
  ORDER BY derived_table.hibu_xref_id, cal.calendar_date;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_diad_print_upsert" AS 
 SELECT DISTINCT b.business_id::bigint AS business_id, b.product AS anaplan_level_4, b.bill_date, st.selling_transaction_id, NULL::character varying::character varying(40) AS bill_number, ao.employee_id AS primary_rep, NULL::character varying::character varying(40) AS secondary_rep, NULL::character varying::character varying(40) AS tertiary_rep, 'DIAD'::character varying AS "system", b.full_billing_amount, 0 AS prorated_billing_amount, 0 AS one_time_fees, 0 AS spread_fees, 'now'::character varying::date AS initial_load_date, 'now'::character varying::date AS update_date, b.first_bill::integer AS first_bill
   FROM stage.view_billing_transaction_diad_print b
   JOIN selling_transaction st ON b.business_id = st.business_id::numeric::numeric(18,0) AND b.product::text = st.product::text AND b.product_start_date = st.go_live_date
   LEFT JOIN account_owner ao ON b.business_id = ao.business_id::numeric::numeric(18,0) AND b.bill_date >= ao.start_date AND b.bill_date < COALESCE(ao.end_date::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)
  WHERE b.bill_date >= '2017-01-01'::date AND b.bill_date <= 'now'::character varying::timestamp without time zone;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_sap_update_selling_id_date" AS 
 SELECT "max"(s.selling_transaction_id) AS selling_transaction_id, b.billing_transaction_id
   FROM billing_transaction b
   LEFT JOIN selling_transaction s ON b.business_id = s.business_id AND b.product::text = s.product::text AND b.bill_date > s.sold_date AND b.bill_date < COALESCE(COALESCE(s.cancel_date, s.end_date)::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)
  WHERE b.source_system::text = 'SAP'::character varying::text AND s.source_system::text = 'hGCP'::character varying::text
  GROUP BY b.billing_transaction_id;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_sap_update_selling_id_ordernum" AS 
 SELECT "max"(s.selling_transaction_id) AS selling_transaction_id, b.billing_transaction_id
   FROM billing_transaction b
   JOIN stage.view_billing_transactions_sap_raw r ON b.business_id = r.business_id AND b.invoice_number::text = r.invoice_number::text AND b.source_system::text = r."system"::text AND b.bill_date = r.bill_date AND b.product::text = r.product::text
   JOIN selling_transaction s ON s.order_number::text = r.doc_number::text AND s.business_id = r.business_id
  WHERE b.selling_transaction_id IS NULL AND b.source_system::text = 'SAP'::character varying::text
  GROUP BY b.billing_transaction_id;


CREATE OR REPLACE VIEW "stage"."view_billing_transaction_upsert" AS 
 SELECT dc.billing_transaction_id, dc.business_id, dc.product, dc.bill_date, dc.selling_transaction_id, dc.invoice_number, dc.primary_rep, dc.secondary_rep, dc.tertiary_rep, dc.source_system, dc.full_billing_amount, dc.prorated_billing_amount, dc.one_time_fees, dc.spread_fees, dc.initial_load_date, dc.update_date, dc.new_update, dc.today, dc.first_bill
   FROM stage.view_billing_transaction_detect_changes dc
  WHERE dc.new_update::text <> 'None'::character varying::text AND dc.bill_date >= '2017-01-01'::date AND dc.bill_date <= 'now'::text::date;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_diad_web" AS 
 SELECT a.business_id, a.product, a.bill_date, sum(a.amount) AS amount, min(a.last_update_date) AS last_update_date, 
        CASE
            WHEN a.sequence_number = 1::numeric::numeric(18,0) THEN 1
            ELSE 0
        END AS first_bill, a.selling_transaction_id
   FROM ( SELECT wcb4.bill_date, wcb4.amount, wcb4.last_update_date, wcb4.sequence_number, z.product, z.selling_transaction_id, mc.business_id_hidden__c AS business_id
           FROM stage.diad_web_contract_bill wcb4
      JOIN stage.diad_web_contract_lines cl1 ON wcb4.contract_key = cl1.contract_key::numeric::numeric(18,0) AND wcb4.region_key = cl1.region_key::numeric::numeric(18,0) AND lower(wcb4.uuid::text) = lower(cl1.uuid::text)
   JOIN product_hierarchy ph1 ON cl1.udac_code::text = ph1.product_code::text AND ph1.effective_end_date IS NULL
   JOIN stage.sfdc_master_customer mc ON mc.customer_key__c = cl1.customer_key AND mc.region_key__c = cl1.region_key
   JOIN ( SELECT wcb3.contract_key, wcb3.region_key, lower(wcb3.uuid::text) AS uuid, wcb3.sequence_number, wcb2.product, wcb2.selling_transaction_id
    FROM stage.diad_web_contract_bill wcb3
   JOIN stage.diad_web_contract_lines cl ON wcb3.contract_key = cl.contract_key::numeric::numeric(18,0) AND wcb3.region_key = cl.region_key::numeric::numeric(18,0) AND lower(wcb3.uuid::text) = lower(cl.uuid::text)
   JOIN product_hierarchy ph ON cl.udac_code::text = ph.product_code::text AND ph.effective_end_date IS NULL
   JOIN ( SELECT wcb1.contract_key, wcb1.region_key, wcb1.bill_date, st.selling_transaction_id, st.product, st.min_uuid
   FROM stage.diad_web_contract_bill wcb1
   JOIN selling_transaction st ON ((wcb1.contract_key::character varying::text || '-'::character varying::text) || wcb1.region_key::character varying::text) = st.order_number::text AND lower(wcb1.uuid::text) = lower(st.min_uuid::text)
  WHERE wcb1.sequence_number = 1::numeric::numeric(18,0)) wcb2 ON wcb2.contract_key = wcb3.contract_key AND wcb2.region_key = wcb3.region_key AND wcb2.bill_date = wcb3.bill_date AND wcb2.product::text = ph.anaplan_level_4::text AND wcb3.sequence_number = 1::numeric::numeric(18,0)) z ON z.contract_key = wcb4.contract_key AND z.region_key = wcb4.region_key AND lower(z.uuid) = lower(wcb4.uuid::text) AND z.product::text = ph1.anaplan_level_4::text) a
  WHERE a.bill_date >= '2017-01-01'::date AND a.bill_date <= 'now'::character varying::date
  GROUP BY a.business_id, a.product, a.bill_date, 
        CASE
            WHEN a.sequence_number = 1::numeric::numeric(18,0) THEN 1
            ELSE 0
        END, a.selling_transaction_id;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_diad_web_upsert" AS 
 SELECT DISTINCT rev.business_id, rev.product AS anaplan_level_4, rev.bill_date, rev.selling_transaction_id, NULL::character varying::character varying(40) AS bill_num, ao.employee_id AS primary_rep, NULL::character varying::character varying(40) AS secondary_rep, NULL::character varying::character varying(40) AS tertiary_rep, 'DIAD'::character varying AS "system", sum(rev.amount) AS full_billing_amount, 0 AS prorated_billing_amount, 0 AS one_time_fees, 0 AS spread_fees, 'now'::character varying::date AS initial_load_date, 'now'::character varying::date AS update_date, rev.first_bill
   FROM stage.view_billing_transactions_diad_web rev
   LEFT JOIN account_owner ao ON ao.business_id::numeric::numeric(18,0) = rev.business_id::numeric::numeric(18,0) AND rev.bill_date >= ao.start_date AND rev.bill_date < COALESCE(ao.end_date::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)
  GROUP BY rev.business_id, rev.product, rev.bill_date, rev.selling_transaction_id, ao.employee_id, rev.first_bill
 HAVING sum(rev.amount) <> 0::double precision;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_diad_webreach" AS 
 SELECT DISTINCT dpa.hibu_xref_id AS business_id, dpa.product_family, cw.udac_code, cw.actual_start_date::date AS actual_start_date, cw.budget_amount, 
        CASE
            WHEN cw.seq_nbr = 1::double precision THEN 1
            ELSE 0
        END AS first_bill, (cw.contract_key::character varying::text || '-'::character varying::text) || cw.region_key::character varying::text AS order_number
   FROM stage.contract_webreach_bill cw
   JOIN stage.sfdc_stage_diad_product_asset dpa ON cw.contract_key = dpa.contract_key AND cw.region_key = dpa.region_key
  WHERE cw.actual_start_date >= '2017-01-01'::date AND cw.actual_start_date <= 'now'::character varying::date::timestamp without time zone AND cw.budget_amount <> 0::double precision;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_diad_webreach_update" AS 
 SELECT w1.contract_key, w1.seq_nbr, w1.region_key, 0 AS budget_amount, 'now'::character varying::timestamp without time zone AS last_update_date
   FROM stage.contract_webreach_bill w1
  WHERE NOT (EXISTS ( SELECT 1
           FROM stage.contract_webreach_bill_full w2
          WHERE w1.contract_key = w2.contract_key AND w1.seq_nbr = w2.seq_nbr AND w1.region_key = w2.region_key));


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_diad_webreach_upsert" AS 
 SELECT DISTINCT derived_table1.business_id, derived_table1.anaplan_level_4, derived_table1.bill_date, derived_table1.selling_transaction_id, derived_table1.bill_num, derived_table1.primary_rep, derived_table1.secondary_rep, derived_table1.tertiary_rep, derived_table1."system", derived_table1.full_billing_amount, derived_table1.prorated_billing_amount, derived_table1.one_time_fees, derived_table1.spread_fees, derived_table1.initial_load_date, derived_table1.update_date, derived_table1.first_bill
   FROM ( SELECT DISTINCT rev.business_id::bigint AS business_id, ph.anaplan_level_4, rev.actual_start_date AS bill_date, s.selling_transaction_id, NULL::character varying::character varying(40) AS bill_num, ao.employee_id AS primary_rep, NULL::character varying::character varying(40) AS secondary_rep, NULL::character varying::character varying(40) AS tertiary_rep, 'DIAD'::character varying AS "system", rev.budget_amount AS full_billing_amount, 0 AS prorated_billing_amount, 0 AS one_time_fees, 0 AS spread_fees, 'now'::character varying::date AS initial_load_date, 'now'::character varying::date AS update_date, rev.first_bill
           FROM stage.view_billing_transactions_diad_webreach rev
      JOIN product_hierarchy ph ON rev.udac_code::text = ph.product_code::text AND ph.effective_end_date IS NULL
   JOIN selling_transaction s ON rev.business_id::character varying::text = s.business_id::character varying::text AND rev.order_number = s.order_number::text AND s.product::text = ph.anaplan_level_4::text AND s.source_system::text = 'DIAD'::character varying::text AND rev.actual_start_date >= s.sold_date AND rev.actual_start_date < COALESCE(COALESCE(s.cancel_date, s.end_date)::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)
   LEFT JOIN account_owner ao ON ao.business_id::numeric::numeric(18,0) = rev.business_id AND rev.actual_start_date >= ao.start_date AND rev.actual_start_date < COALESCE(ao.end_date::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)) derived_table1;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_sap_raw" AS 
 SELECT DISTINCT mc.business_id_hidden__c AS business_id, COALESCE(ph1.anaplan_level_4, ph.anaplan_level_4) AS product, sp.bill_date::date AS bill_date, NULL::bigint AS selling_transaction_id, sp.bill_num AS invoice_number, ao.employee_id AS primary_rep, NULL::character varying::character varying(40) AS secondary_rep, NULL::character varying::character varying(40) AS tertiary_rep, 'SAP'::character varying::character varying(3) AS "system", 
        CASE
            WHEN ph.prorated_sku = 0 AND ph.one_time_fee_sku = 0 AND ph.spreed_fee_sku = 0 THEN sp.netval_inv
            ELSE 0::numeric::numeric(18,0)
        END AS full_billing_amount, 
        CASE
            WHEN ph.prorated_sku = 1 THEN sp.netval_inv
            ELSE 0::numeric::numeric(18,0)
        END AS prorated_billing_amount, 
        CASE
            WHEN ph.one_time_fee_sku = 1 THEN sp.netval_inv
            ELSE 0::numeric::numeric(18,0)
        END AS one_time_fees, 
        CASE
            WHEN ph.spreed_fee_sku = 1 THEN sp.netval_inv
            ELSE 0::numeric::numeric(18,0)
        END AS spread_fees, COALESCE(fb.first_bill, 0) AS first_bill, sp.sold_to, sp.netval_inv, m.product_sku, sp.bill_item, sp.doc_number
   FROM stage.sap_billing sp
   JOIN product_hierarchy ph ON sp.material::text = ph.product_code::text AND ph.effective_end_date IS NULL
   JOIN stage.sfdc_master_customer mc ON sp.sold_to::numeric::numeric(18,0)::numeric(38,2) = mc.ghost_account_id__c::numeric::numeric(18,0)::numeric(38,2)
   LEFT JOIN ( SELECT s2.sold_to, s2.anaplan_level_4 AS product, s2.min_bill_date, s2.first_bill
   FROM ( SELECT sp.sold_to, ph.anaplan_level_3, ph.anaplan_level_4, min(sp.bill_date::text) AS min_bill_date, 1 AS first_bill
           FROM stage.sap_billing sp
      JOIN stage.view_mpe_order_sku_asset_map m ON sp.material::text = m.asset_sku::text AND sp.doc_number::text = m.mpe_order_number::text
   JOIN product_hierarchy ph ON ph.product_code::text = m.product_sku::text AND ph.effective_end_date IS NULL
  WHERE ph.prorated_sku = 0 AND ph.one_time_fee_sku = 0 AND ph.spreed_fee_sku = 0 AND sp.netval_inv <> 0::numeric::numeric(18,0)::numeric(17,2)
  GROUP BY sp.sold_to, ph.anaplan_level_3, ph.anaplan_level_4) s2
   JOIN ( SELECT s1.sold_to, s1.anaplan_level_3, min(s1.min_bill_date) AS min_bill_date, s1.first_bill
           FROM ( SELECT sp.sold_to, ph.anaplan_level_3, ph.anaplan_level_4, min(sp.bill_date::text) AS min_bill_date, 1 AS first_bill
                   FROM stage.sap_billing sp
              JOIN stage.view_mpe_order_sku_asset_map m ON sp.material::text = m.asset_sku::text AND sp.doc_number::text = m.mpe_order_number::text
         JOIN product_hierarchy ph ON ph.product_code::text = m.product_sku::text AND ph.effective_end_date IS NULL
        WHERE ph.prorated_sku = 0 AND ph.one_time_fee_sku = 0 AND ph.spreed_fee_sku = 0 AND sp.netval_inv <> 0::numeric::numeric(18,0)::numeric(17,2)
        GROUP BY sp.sold_to, ph.anaplan_level_3, ph.anaplan_level_4) s1
          GROUP BY s1.sold_to, s1.anaplan_level_3, s1.first_bill) s3 ON s2.sold_to::text = s3.sold_to::text AND s2.anaplan_level_3::text = s3.anaplan_level_3::text AND s2.min_bill_date = s3.min_bill_date) fb ON fb.sold_to::text = sp.sold_to::text AND fb.min_bill_date = sp.bill_date::text AND fb.product::text = ph.anaplan_level_4::text
   LEFT JOIN account_owner ao ON mc.business_id_hidden__c::numeric::numeric(18,0) = ao.business_id::numeric::numeric(18,0) AND sp.bill_date::date >= ao.start_date AND sp.bill_date::date < COALESCE(ao.end_date::timestamp without time zone, 'now'::character varying::date::timestamp without time zone)
   LEFT JOIN stage.mpe_order_sku_asset_map m ON m.asset_sku::text = sp.material::text AND m.mpe_order_number::text = sp.doc_number::text
   LEFT JOIN product_hierarchy ph1 ON m.product_sku::text = ph1.product_code::text AND ph1.effective_end_date IS NULL;


CREATE OR REPLACE VIEW "stage"."view_billing_transactions_sap_upsert" AS 
 SELECT DISTINCT view_billing_transactions_sap_raw.business_id, view_billing_transactions_sap_raw.product AS anaplan_level_4, view_billing_transactions_sap_raw.bill_date, NULL::bigint AS selling_transaction_id, view_billing_transactions_sap_raw.invoice_number AS bill_num, view_billing_transactions_sap_raw.primary_rep, NULL::character varying::character varying(40) AS secondary_rep, NULL::character varying::character varying(40) AS tertiary_rep, 'SAP'::character varying AS "system", sum(view_billing_transactions_sap_raw.full_billing_amount) AS full_billing_amount, sum(view_billing_transactions_sap_raw.prorated_billing_amount) AS prorated_billing_amount, sum(view_billing_transactions_sap_raw.one_time_fees) AS one_time_fees, sum(view_billing_transactions_sap_raw.spread_fees) AS spread_fees, 'now'::character varying::date AS initial_load_date, 'now'::character varying::date AS update_date, "max"(view_billing_transactions_sap_raw.first_bill) AS first_bill
   FROM stage.view_billing_transactions_sap_raw
  WHERE NOT view_billing_transactions_sap_raw.business_id IS NULL AND view_billing_transactions_sap_raw.bill_date >= '2017-01-01'::date
  GROUP BY view_billing_transactions_sap_raw.business_id, view_billing_transactions_sap_raw.product, view_billing_transactions_sap_raw.bill_date, view_billing_transactions_sap_raw.invoice_number, view_billing_transactions_sap_raw.primary_rep;


CREATE OR REPLACE VIEW "stage"."view_customer_new_upsert" AS 
 SELECT customer_product_incremental.parent_business_id, min(customer_product_incremental.min_go_live_date) AS new_customer_start_date, date_add('month'::character varying::text, (( SELECT customer_product_new_duration.number_of_months
           FROM customer_product_new_duration
          WHERE customer_product_new_duration.duration_type::text = 'customer'::character varying::text))::bigint, min(customer_product_incremental.min_go_live_date)::timestamp without time zone)::date AS new_customer_end_date, "max"(customer_product_incremental.max_bill_date) AS max_bill_date, date_add('month'::character varying::text, (( SELECT customer_product_new_duration.number_of_months
           FROM customer_product_new_duration
          WHERE customer_product_new_duration.duration_type::text = 'customer'::character varying::text))::bigint, COALESCE("max"(customer_product_incremental.max_bill_date), "max"(customer_product_incremental.min_go_live_date))::timestamp without time zone)::date AS customer_end_date
   FROM customer_product_incremental
  GROUP BY customer_product_incremental.parent_business_id;


CREATE OR REPLACE VIEW "public"."view_customer_owner" AS 
 SELECT a.business_id, a.employee_id, a.start_date, a.end_date, a.insert_date, a.update_date
   FROM account_owner a
   JOIN customer c ON a.business_id = c.business_id;


CREATE OR REPLACE VIEW "stage"."view_customer_owner_upsert" AS 
 SELECT a.business_id_hidden__c, a.employee_id__c, 
        CASE
            WHEN a.start_date IS NULL THEN a.startdate
            ELSE a.start_date
        END AS start_date, a.end_date
   FROM ( SELECT a.business_id_hidden__c, a.employee_id__c, a.startdate::date AS startdate, (lead(a.enddate::text)
          OVER( 
          PARTITION BY a.business_id_hidden__c
          ORDER BY a.startdate DESC))::date AS start_date, a.enddate::date AS end_date
           FROM ( SELECT a.business_id_hidden__c, c.employee_id__c, (''::character varying::text || a.createddate::text)::character varying AS startdate, 
                        CASE
                            WHEN bb.nv IS NULL AND (( SELECT count(DISTINCT x.createddate) AS count
                               FROM stage.sfdc_account_history_owner x
                              WHERE x.accountid::text = b.accountid::text)) < 2 THEN NULL::character varying::text
                            ELSE ''::character varying::text || min(b.createddate)::character varying::text
                        END::character varying AS enddate
                   FROM stage.sfdc_master_customer a
              JOIN stage.sfdc_account_history_owner b ON a.id::text = b.accountid::text
         JOIN stage.sfdc_user c ON b.oldvalue::text = c.id::text
    JOIN ( SELECT DISTINCT sfdc_account_history_owner.accountid, "first_value"(sfdc_account_history_owner.oldvalue::text)
                OVER( 
                PARTITION BY sfdc_account_history_owner.accountid
                ORDER BY sfdc_account_history_owner.createddate
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS ov, "first_value"(sfdc_account_history_owner.createddate)
                OVER( 
                PARTITION BY sfdc_account_history_owner.accountid
                ORDER BY sfdc_account_history_owner.createddate
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS cd, 
                      CASE
                          WHEN ("first_value"(sfdc_account_history_owner.newvalue::text)
                    OVER( 
                    PARTITION BY sfdc_account_history_owner.accountid
                    ORDER BY sfdc_account_history_owner.createddate
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) = '005i0000003SASwAAO'::character varying::text THEN NULL::character varying::text
                          ELSE "first_value"(sfdc_account_history_owner.newvalue::text)
                    OVER( 
                    PARTITION BY sfdc_account_history_owner.accountid
                    ORDER BY sfdc_account_history_owner.createddate
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                      END AS nv
                 FROM stage.sfdc_account_history_owner) bb ON b.oldvalue::text = bb.ov AND b.accountid::text = bb.accountid::text AND b.createddate = bb.cd
   WHERE c.employee_id__c::text <> 'UNA999'::character varying::text
   GROUP BY a.business_id_hidden__c, b.accountid, c.employee_id__c, a.createddate, b.createddate, bb.nv
        UNION ALL 
                 SELECT a.business_id_hidden__c, c.employee_id__c, (''::character varying::text || b.createddate::character varying::text)::character varying AS startdate, (''::character varying::text || (lead(b.createddate)
                  OVER( 
                  PARTITION BY b.accountid
                  ORDER BY b.createddate))::character varying::text)::character varying AS enddate
                   FROM stage.sfdc_master_customer a
              JOIN stage.sfdc_account_history_owner b ON a.id::text = b.accountid::text
         JOIN stage.sfdc_user c ON b.newvalue::text = c.id::text
        WHERE c.employee_id__c::text <> 'UNA999'::character varying::text
        GROUP BY a.business_id_hidden__c, b.accountid, c.employee_id__c, a.createddate, b.createddate) a
          GROUP BY a.business_id_hidden__c, a.employee_id__c, a.startdate, a.enddate) a
  WHERE NOT a.business_id_hidden__c IS NULL AND NOT a.employee_id__c IS NULL
UNION 
 SELECT qrymain.business_id_hidden__c, qrymain.employee_id__c, qrymain.start_date::date AS start_date, NULL::date AS end_date
   FROM ( SELECT DISTINCT mc.business_id_hidden__c, u.employee_id__c, mc.createddate AS start_date, (mc.business_id_hidden__c::character varying::text || '_'::character varying::text) || mc.createddate::text AS busid_startday, "max"(mc.createddate::text)
          OVER( 
          PARTITION BY (mc.business_id_hidden__c::character varying::text || '_'::character varying::text) || mc.createddate::date::character varying::text
          ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS maxstart_day, min(mc.createddate::date) AS end_date
           FROM stage.sfdc_master_customer mc
      JOIN stage.sfdc_user u ON mc.ownerid::text = u.id::text
     WHERE NOT (mc.id IN ( SELECT DISTINCT sfdc_account_history_owner.accountid
              FROM stage.sfdc_account_history_owner))
     GROUP BY mc.business_id_hidden__c, u.employee_id__c, mc.createddate, (mc.business_id_hidden__c::character varying::text || '_'::character varying::text) || mc.createddate::text) qrymain
  WHERE qrymain.start_date::text = qrymain.maxstart_day AND NOT qrymain.business_id_hidden__c IS NULL AND NOT qrymain.employee_id__c IS NULL;


CREATE OR REPLACE VIEW "stage"."view_customer_product_incremental_audit_seed" AS 
 SELECT derived_table1.parent_business_id, derived_table1.anaplan_level_3, 
        CASE
            WHEN derived_table1.min_submit_go_live_date > derived_table1.first_sale_date AND derived_table1.incremental_end_date < derived_table1.last_end_date THEN derived_table1.previous_sale_date
            WHEN derived_table1.min_submit_go_live_date > derived_table1.first_sale_date AND derived_table1.incremental_end_date = derived_table1.last_end_date AND derived_table1.previous_end_date >= derived_table1.min_submit_go_live_date AND derived_table1.previous_end_date <= derived_table1.incremental_end_date THEN derived_table1.first_sale_date
            ELSE derived_table1.min_submit_go_live_date
        END AS min_submit_go_live_date, derived_table1.max_end_date, derived_table1.min_bill_date, derived_table1.max_bill_date, derived_table1.new_product_end_date, derived_table1.incremental_end_date
   FROM ( SELECT customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3, customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.max_end_date, customer_product_incremental_audit_seed.min_bill_date, customer_product_incremental_audit_seed.max_bill_date, customer_product_incremental_audit_seed.new_product_end_date, customer_product_incremental_audit_seed.incremental_end_date, pg_catalog.lead(customer_product_incremental_audit_seed.min_submit_go_live_date, 1)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date DESC, customer_product_incremental_audit_seed.incremental_end_date DESC) AS previous_sale_date, pg_catalog.lead(customer_product_incremental_audit_seed.min_submit_go_live_date, 1)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date) AS next_sale_date, pg_catalog.lead(customer_product_incremental_audit_seed.incremental_end_date, 1)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date DESC, customer_product_incremental_audit_seed.incremental_end_date DESC) AS previous_end_date, pg_catalog.lead(customer_product_incremental_audit_seed.incremental_end_date, 1)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date) AS next_end_date, "first_value"(customer_product_incremental_audit_seed.min_submit_go_live_date)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date
          ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS first_sale_date, "last_value"(customer_product_incremental_audit_seed.incremental_end_date)
          OVER( 
          PARTITION BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.anaplan_level_3
          ORDER BY customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date
          ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS last_end_date
           FROM stage.customer_product_incremental_audit_seed
          GROUP BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date, customer_product_incremental_audit_seed.anaplan_level_3, customer_product_incremental_audit_seed.max_end_date, customer_product_incremental_audit_seed.min_bill_date, customer_product_incremental_audit_seed.max_bill_date, customer_product_incremental_audit_seed.new_product_end_date
          ORDER BY customer_product_incremental_audit_seed.parent_business_id, customer_product_incremental_audit_seed.min_submit_go_live_date, customer_product_incremental_audit_seed.incremental_end_date) derived_table1
  WHERE derived_table1.incremental_end_date < derived_table1.next_sale_date OR derived_table1.next_sale_date IS NULL;


CREATE OR REPLACE VIEW "stage"."view_customer_product_incremental_combine" AS 
 SELECT u.parent_business_id, u.anaplan_level_3, min(u.min_submit_go_live_date) AS min_submit_go_live_date, "max"(u.max_end_date) AS max_end_date, min(u.min_bill_date) AS min_bill_date, "max"(u.max_bill_date) AS max_bill_date, date_add('month'::character varying::text, min(u.incremental_number_of_months)::bigint, min(u.min_submit_go_live_date)::timestamp without time zone) AS new_product_end_date, date_add('month'::character varying::text, min(u.incremental_number_of_months)::bigint, "max"(u.max_bill_date)::timestamp without time zone) AS incremental_end_date
   FROM ( SELECT d.parent_business_id, d.anaplan_level_3, d.min_submit_go_live_date, d.max_end_date, d.min_bill_date, d.max_bill_date, d.incremental_number_of_months
           FROM stage.view_customer_product_incremental_diad d
UNION 
         SELECT h.parent_business_id, h.anaplan_level_3, h.min_submit_go_live_date, h.max_end_date, h.min_bill_date, h.max_bill_date, h.incremental_number_of_months
           FROM stage.view_customer_product_incremental_hgcp h) u
  GROUP BY u.parent_business_id, u.anaplan_level_3;


CREATE OR REPLACE VIEW "stage"."view_customer_product_incremental_detect_changes" AS 
 SELECT c.parent_business_id, c.anaplan_level_3, c.min_submit_go_live_date, c.max_end_date, c.min_bill_date, c.max_bill_date, c.new_product_end_date, c.incremental_end_date, 
        CASE
            WHEN a.parent_business_id IS NULL THEN 'New'::character varying
            WHEN NOT a.parent_business_id IS NULL AND (
            CASE
                WHEN c.min_submit_go_live_date = a.min_go_live_date OR c.min_submit_go_live_date IS NULL AND a.min_go_live_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN c.max_end_date = a.max_end_date OR c.max_end_date IS NULL AND a.max_end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN c.min_bill_date = a.min_bill_date OR c.min_bill_date IS NULL AND a.min_bill_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN c.max_bill_date = a.max_bill_date OR c.max_bill_date IS NULL AND a.max_bill_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN c.new_product_end_date = a.new_product_end_date::timestamp without time zone OR c.new_product_end_date IS NULL AND a.new_product_end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN c.incremental_end_date = a.incremental_end_date::timestamp without time zone OR c.incremental_end_date IS NULL AND a.incremental_end_date IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, a.insert_date
   FROM stage.view_customer_product_incremental_combine c
   LEFT JOIN customer_product_incremental a ON c.parent_business_id = a.parent_business_id AND c.anaplan_level_3::text = a.anaplan_level_3::text;


CREATE OR REPLACE VIEW "stage"."view_customer_product_incremental_diad" AS 
 SELECT a.parent_business_id, a.anaplan_level_3, min(a.min_submit_go_live_date) AS min_submit_go_live_date, "max"(a.max_end_date) AS max_end_date, min(a.min_bill_date) AS min_bill_date, "max"(a.max_bill_date) AS max_bill_date, min(a.incremental_number_of_months) AS incremental_number_of_months
   FROM ( SELECT COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c) AS parent_business_id, ph2.anaplan_level_3, p.name, 
                CASE
                    WHEN NOT min(pm.server_processed_date) IS NULL AND min(pm.server_processed_date) < min(p.sale_date__c)::timestamp without time zone THEN min(pm.server_processed_date)::timestamp without time zone
                    WHEN NOT min(pm.server_processed_date) IS NULL AND min(pm.server_processed_date) >= min(p.sale_date__c)::timestamp without time zone THEN min(p.sale_date__c)::timestamp without time zone
                    WHEN min(pm.server_processed_date) IS NULL AND NOT min(p.sale_date__c) IS NULL THEN min(p.sale_date__c)::timestamp without time zone
                    WHEN min(pm.server_processed_date) IS NULL AND min(p.sale_date__c) IS NULL AND NOT min(p.entry_date__c) IS NULL THEN min(p.entry_date__c)::timestamp without time zone
                    WHEN min(pm.server_processed_date) IS NULL AND min(p.sale_date__c) IS NULL AND min(p.entry_date__c) IS NULL AND NOT to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text) IS NULL THEN to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text)::timestamp without time zone
                    WHEN min(pm.server_processed_date) IS NULL AND min(p.sale_date__c) IS NULL AND min(p.entry_date__c) IS NULL AND to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text) IS NULL AND NOT min(wcb.bill_date) IS NULL THEN min(wcb.bill_date)
                    ELSE min(p.go_live_date__c)::timestamp without time zone
                END::date AS min_submit_go_live_date, "max"(dp.product_end_date::date) AS max_end_date, 
                CASE
                    WHEN NOT to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text) IS NULL THEN to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text)
                    WHEN to_date(min(wb.actual_start_date)::character varying::text, 'YYYY-MM-DD'::character varying::text) IS NULL AND NOT to_date(min(wcb.bill_date)::character varying::text, 'YYYY-MM-DD'::character varying::text) IS NULL THEN to_date(min(wcb.bill_date)::character varying::text, 'YYYY-MM-DD'::character varying::text)
                    ELSE min(p.go_live_date__c)
                END AS min_bill_date, "max"(dp.product_end_date::date) AS max_bill_date, "max"(d.number_of_months) AS incremental_number_of_months
           FROM stage.sfdc_master_customer mc
      LEFT JOIN stage.sfdc_master_customer mc2 ON mc.parentid::text = mc2.id::text
   JOIN stage.sfdc_product p ON p.business_id_hidden__c::text = mc.business_id_hidden__c::character varying::text AND COALESCE(p.sale_date__c, p.go_live_date__c) >= '2000-01-01'::date AND COALESCE(p.rate_card__c, 'R'::character varying)::text <> 'NAT'::character varying::text AND p.monthly_spend__c <> 0::numeric::numeric(18,0)::numeric(18,2)
   LEFT JOIN stage.sfdc_stage_diad_product_asset dp ON p.external_product_id__c::text = dp.external_product_id::text
   LEFT JOIN product_hierarchy ph2 ON dp.asset_code::text = ph2.product_code::text AND ph2.effective_end_date IS NULL
   LEFT JOIN stage.sfa_proposal_master pm ON dp.contract_reference_link::text = pm.proposal_guid::text
   LEFT JOIN stage.contract_webreach_bill wb ON dp.contract_key = wb.contract_key AND dp.region_key = wb.region_key AND wb.budget_amount <> 0::double precision
   LEFT JOIN stage.diad_web_contract_bill wcb ON dp.contract_key = wcb.contract_key AND dp.region_key = wcb.region_key AND dp.line_item_uuid::text = wcb.uuid::text AND wcb.amount <> 0::double precision
   LEFT JOIN customer_product_new_duration d ON d.anaplan_level_3::text = ph2.anaplan_level_3::text
   LEFT JOIN ( SELECT customer_product_incremental_audit.parent_business_id, customer_product_incremental_audit.anaplan_level_3, "max"(customer_product_incremental_audit.max_end_date) AS max_end_date
   FROM customer_product_incremental_audit
  GROUP BY customer_product_incremental_audit.parent_business_id, customer_product_incremental_audit.anaplan_level_3) ca ON ca.parent_business_id = COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c) AND ph2.anaplan_level_3::text = ca.anaplan_level_3::text
  WHERE NOT ph2.anaplan_level_3 IS NULL AND p.end_date__c > COALESCE(ca.max_end_date, '1980-01-01'::date)
  GROUP BY COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c), ph2.anaplan_level_3, p.name) a
  GROUP BY a.parent_business_id, a.anaplan_level_3;


CREATE OR REPLACE VIEW "stage"."view_customer_product_incremental_hgcp" AS 
 SELECT a.parent_business_id, a.anaplan_level_3, min(a.min_submit_go_live_date) AS min_submit_go_live_date, "max"(a.max_end_date) AS max_end_date, min(a.min_bill_date) AS min_bill_date, "max"(a.max_bill_date) AS max_bill_date, min(a.incremental_number_of_months) AS incremental_number_of_months
   FROM ( SELECT COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c) AS parent_business_id, ph1.anaplan_level_3, p.name, 
                CASE
                    WHEN NOT min(mso.submitted_on) IS NULL THEN min(mso.submitted_on)
                    WHEN min(mso.submitted_on) IS NULL AND NOT min(p.entry_date__c) IS NULL THEN min(p.entry_date__c)::timestamp without time zone
                    WHEN min(mso.submitted_on) IS NULL AND min(p.entry_date__c) IS NULL AND NOT min(p.go_live_date__c) IS NULL THEN min(p.go_live_date__c)::timestamp without time zone
                    WHEN min(mso.submitted_on) IS NULL AND min(p.entry_date__c) IS NULL AND min(p.go_live_date__c) IS NULL AND NOT min(to_date(bil.bill_date::text, 'YYYYMMDD'::character varying::text)) IS NULL THEN min(to_date(bil.bill_date::text, 'YYYYMMDD'::character varying::text))::timestamp without time zone
                    ELSE min(p.sale_date__c)::timestamp without time zone
                END::date AS min_submit_go_live_date, "max"(p.end_date__c) AS max_end_date, min(to_date(bil.bill_date::text, 'YYYYMMDD'::character varying::text)) AS min_bill_date, "max"(to_date(bil.bill_date::text, 'YYYYMMDD'::character varying::text)) AS max_bill_date, "max"(d.number_of_months) AS incremental_number_of_months
           FROM stage.sfdc_master_customer mc
      LEFT JOIN stage.sfdc_master_customer mc2 ON mc.parentid::text = mc2.id::text
   JOIN stage.sfdc_product p ON p.business_id_hidden__c::text = mc.business_id_hidden__c::character varying::text AND COALESCE(p.sale_date__c, p.go_live_date__c) >= '2000-01-01'::date AND COALESCE(p.rate_card__c, 'R'::character varying)::text <> 'NAT'::character varying::text AND p.monthly_spend__c <> 0::numeric::numeric(18,0)::numeric(18,2)
   LEFT JOIN stage.mpe_order_sku_asset_map os ON os.mpe_order_number::text = p.contract__c::text AND os.parent_asset_id::character varying::text = p.product_asset_id__c::text
   LEFT JOIN product_hierarchy ph1 ON os.product_sku::text = ph1.product_code::text AND ph1.effective_end_date IS NULL
   JOIN stage.mpe_submitted_order mso ON mso.order_number_alpha::text = p.contract__c::text
   LEFT JOIN ( SELECT b.sold_to, b.bill_date, b.material, ph.anaplan_level_3
   FROM stage.sap_billing b
   JOIN product_hierarchy ph ON b.material::text = ph.product_code::text AND ph.effective_end_date IS NULL
  WHERE b.netval_inv <> 0::numeric::numeric(18,0)::numeric(17,2)) bil ON bil.sold_to::numeric::numeric(18,0)::numeric(38,2) = mc.ghost_account_id__c::numeric::numeric(18,0)::numeric(38,2) AND bil.anaplan_level_3::text = ph1.anaplan_level_3::text
   LEFT JOIN customer_product_new_duration d ON d.anaplan_level_3::text = ph1.anaplan_level_3::text
   LEFT JOIN customer_product_incremental_audit ca ON ca.parent_business_id = COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c) AND ph1.anaplan_level_3::text = ca.anaplan_level_3::text
  WHERE NOT ph1.anaplan_level_3 IS NULL AND NOT p.business_id_hidden__c IS NULL AND COALESCE(p.end_date__c, '2100-01-01'::date) > COALESCE(ca.max_end_date, '1980-01-01'::date)
  GROUP BY COALESCE(mc2.business_id_hidden__c, mc.business_id_hidden__c), ph1.anaplan_level_3, p.name) a
  GROUP BY a.parent_business_id, a.anaplan_level_3;


CREATE OR REPLACE VIEW "stage"."view_customer_upsert" AS 
 SELECT mc1.business_id_hidden__c AS business_id, COALESCE(mc2.business_id_hidden__c, mc1.business_id_hidden__c) AS parent_business_id, COALESCE(mc2.name, mc1.name) AS parent_account_name, mc1.name AS company_name, 
        CASE
            WHEN COALESCE(ca.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN cn.new_customer_start_date
            WHEN COALESCE(cn.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN ca.new_customer_start_date
            WHEN cn.new_customer_start_date <= ca.new_customer_start_date THEN cn.new_customer_start_date
            ELSE ca.new_customer_start_date
        END AS original_acq_date, COALESCE(cn.max_bill_date, ca.max_bill_date) AS last_known_bill_date, mc1."type" AS sfc_type, 
        CASE
            WHEN cn.parent_business_id IS NULL AND ca.customer_end_date <= 'now'::character varying::date THEN 1
            ELSE 0
        END AS former_customer_flag, 
        CASE
            WHEN cn.parent_business_id IS NULL AND ca.customer_end_date <= 'now'::character varying::date THEN ca.customer_end_date
            ELSE NULL::date
        END AS former_customer_start_flag_date, m.name AS primary_market, h.name AS primary_classification, mc1.ig_rank__c AS igr, 
        CASE
            WHEN COALESCE(ca.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN cn.new_customer_start_date
            WHEN COALESCE(cn.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN ca.new_customer_start_date
            WHEN cn.new_customer_start_date >= ca.new_customer_start_date THEN cn.new_customer_start_date
            ELSE ca.new_customer_start_date
        END AS latest_acq_date, 
        CASE
            WHEN cn.new_customer_end_date > 'now'::character varying::date AND (cn.new_customer_start_date > ca.customer_end_date OR ca.customer_end_date IS NULL) THEN 'New'::character varying
            WHEN cn.parent_business_id IS NULL AND ca.customer_end_date <= 'now'::character varying::date THEN 'Prospect'::character varying
            ELSE 'Existing'::character varying
        END AS new_existing, mc1.p2d_flag, mc1.recent_sales_initiative, cn.new_customer_start_date, cn.new_customer_end_date
   FROM stage.sfdc_master_customer mc1
   LEFT JOIN stage.sfdc_master_customer mc2 ON mc1.parentid::text = mc2.id::text
   LEFT JOIN customer_new cn ON COALESCE(mc2.business_id_hidden__c, mc1.business_id_hidden__c) = cn.parent_business_id
   LEFT JOIN stage.sfdc_classification h ON mc1.primary_classification__c::text = h.id::text
   LEFT JOIN stage.sfdc_market m ON mc1.primary_market_lookup__c::text = m.id::text
   LEFT JOIN ( SELECT ca1.parent_business_id, ca1.new_customer_start_date, ca1.new_customer_end_date, ca1.max_bill_date, ca1.customer_end_date, ca1.insert_date, ca1.deleted_flag
   FROM customer_new_audit ca1
   JOIN ( SELECT ca3.parent_business_id, min(ca3.insert_date) AS min_insert_date
           FROM customer_new_audit ca3
          GROUP BY ca3.parent_business_id) ca2 ON ca1.parent_business_id = ca2.parent_business_id AND ca1.insert_date = ca2.min_insert_date) ca ON COALESCE(mc2.business_id_hidden__c, mc1.business_id_hidden__c) = ca.parent_business_id
  WHERE 
CASE
    WHEN COALESCE(ca.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN cn.new_customer_start_date
    WHEN COALESCE(cn.new_customer_start_date, '1980-01-01'::date) = '1980-01-01'::date THEN ca.new_customer_start_date
    WHEN cn.new_customer_start_date >= ca.new_customer_start_date THEN cn.new_customer_start_date
    ELSE ca.new_customer_start_date
END IS NOT NULL AND (EXISTS ( SELECT 1
   FROM stage.sfdc_product p
  WHERE p.business_id_hidden__c::text = mc1.business_id_hidden__c::character varying::text AND COALESCE(p.sale_date__c, p.go_live_date__c) >= '2000-01-01'::date AND COALESCE(p.rate_card__c, 'R'::character varying)::text <> 'NAT'::character varying::text));


CREATE OR REPLACE VIEW "stage"."view_employee_raw" AS 
 SELECT DISTINCT employment.employeenumber::integer AS employeenumber, employmenthistory.employeeid, employmenthistory.supervisorid AS managerid, manager.managernumber::integer AS managernumber, employmenthistory.jobcode AS ultipro_job_code, employmenthistory.integrationeffectivedate AS effective_start_date, person.lastname, person.firstname, employee.email AS email_address, employment.originalhire AS original_hire_date, employment.terminationdate AS termination_date, employment.lasthire AS last_hire_date, min(employmenthistory.integrationeffectivedate)
  OVER( 
  PARTITION BY (employmenthistory.employeeid::text || '_'::character varying::text) || employmenthistory.jobcode::text
  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_in_job, employee.otherpersonalfield12 AS employment_agreement_date, employee.otherpersonalfield06 AS comp_agreement_return_date, employmenthistory.orglevel AS division_code, employmenthistory.orgleve1description AS division_name, employmenthistory.companycode, employmenthistory.orglevel2 AS department_code, employmenthistory.orglevel2description AS department_name, employmenthistory.employeetype, employmenthistory.employmentstatus, employmenthistory.datetimecreated AS record_created_start_date, 
        CASE
            WHEN (employmenthistory.orglevel::text = 'FLSLS1'::character varying::text OR employmenthistory.orglevel::text = 'FLSLS2'::character varying::text OR employmenthistory.orglevel::text = 'FLSLS3'::character varying::text) AND employment.othercompanyfield02 IS NOT NULL THEN ('450'::character varying::text || employment.othercompanyfield02::text)::character varying
            WHEN employmenthistory.orglevel::text = 'INSLS'::character varying::text AND employment.othercompanyfield02 IS NOT NULL THEN ('465'::character varying::text || employment.othercompanyfield02::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0458'::character varying::text AND employment.othercompanyfield02 IS NOT NULL THEN ('458'::character varying::text || employment.othercompanyfield02::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0945'::character varying::text AND employment.othercompanyfield02 IS NOT NULL THEN ('945'::character varying::text || employment.othercompanyfield02::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0962'::character varying::text AND employment.othercompanyfield02 IS NOT NULL THEN ('962'::character varying::text || employment.othercompanyfield02::text)::character varying
            WHEN (employmenthistory.orglevel::text = 'FLSLS1'::character varying::text OR employmenthistory.orglevel::text = 'FLSLS2'::character varying::text OR employmenthistory.orglevel::text = 'FLSLS3'::character varying::text) AND employment.othercompanyfield02 IS NULL THEN ('450'::character varying::text || 'NULL'::character varying::text)::character varying
            WHEN employmenthistory.orglevel::text = 'INSLS'::character varying::text AND employment.othercompanyfield02 IS NULL THEN ('465'::character varying::text || 'NULL'::character varying::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0458'::character varying::text AND employment.othercompanyfield02 IS NULL THEN ('458'::character varying::text || 'NULL'::character varying::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0945'::character varying::text AND employment.othercompanyfield02 IS NULL THEN ('945'::character varying::text || 'NULL'::character varying::text)::character varying
            WHEN employmenthistory.orglevel::text = 'OTHSLS'::character varying::text AND employmenthistory.orglevel2::text = '0962'::character varying::text AND employment.othercompanyfield02 IS NULL THEN ('962'::character varying::text || 'NULL'::character varying::text)::character varying
            ELSE employment.othercompanyfield02
        END AS calc_comp_position_code, pg_catalog.lead(employmenthistory.datetimecreated, 0)
  OVER( 
  PARTITION BY (employmenthistory.employeeid::text || employmenthistory.supervisorid::text) || employmenthistory.jobcode::text
  ORDER BY employmenthistory.datetimecreated) AS mindate
   FROM ( SELECT a.companycode, a.datetimecreated, a.employeeid, a.employeetype, a.employmentstatus, a.integrationeffectivedate, a.jobcode, a.jobeffectivedate, a.supervisorid, a.supervisorlastname, a.systemid, a.orglevel, a.orgleve1description, a.orglevel2, a.orglevel2description
           FROM stage.ultipro_employmenthistory a
          WHERE (a.employeeid IN ( SELECT DISTINCT b.employeeid
                   FROM stage.ultipro_employment b
                  WHERE b.terminationdate > '2017-01-01 00:00:00'::timestamp without time zone OR b.terminationdate IS NULL))) employmenthistory
   LEFT JOIN stage.ultipro_employee employee ON employmenthistory.employeeid::text = employee.employeeid::text
   LEFT JOIN stage.ultipro_person person ON employmenthistory.employeeid::text = person.employeeid::text
   LEFT JOIN stage.ultipro_employment employment ON employment.employeeid::text = employmenthistory.employeeid::text AND employment.companycode::text = employmenthistory.companycode::text
   LEFT JOIN ( SELECT DISTINCT employment.employeeid, employment.employeenumber, employment1.employeenumber AS managernumber, manager.supervisorid
   FROM stage.ultipro_employmenthistory manager
   LEFT JOIN stage.ultipro_employment employment ON btrim(employment.employeeid::text) = btrim(manager.employeeid::text)
   LEFT JOIN stage.ultipro_employment employment1 ON btrim(manager.supervisorid::text) = btrim(employment1.employeeid::text)) manager ON employmenthistory.employeeid::text = manager.employeeid::text AND employmenthistory.supervisorid::text = manager.supervisorid::text
  WHERE employmenthistory.companycode::text = 'HIB'::character varying::text AND ("substring"(employmenthistory.jobcode::text, 1, 3) = 'SRF'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'SRI'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'SMF'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'SMI'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'SM1'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'SM2'::character varying::text OR "substring"(employmenthistory.jobcode::text, 1, 3) = 'EM0'::character varying::text) AND (employmenthistory.employmentstatus::text = 'A'::character varying::text OR employmenthistory.employmentstatus::text = 'T'::character varying::text OR employmenthistory.employmentstatus::text = 'L'::character varying::text OR employmenthistory.employmentstatus::text = 'S'::character varying::text)
  GROUP BY employment.employeenumber, employmenthistory.employeeid, employmenthistory.supervisorid, manager.managernumber, employmenthistory.jobcode, employmenthistory.integrationeffectivedate, person.lastname, person.firstname, employee.email, employment.originalhire, employment.terminationdate, employment.lasthire, employee.otherpersonalfield12, employee.otherpersonalfield06, employmenthistory.orglevel, employment.othercompanyfield02, employmenthistory.orgleve1description, employmenthistory.companycode, employment.companycode, employmenthistory.orglevel2, employmenthistory.orglevel2description, employmenthistory.employeetype, employmenthistory.employmentstatus, employmenthistory.datetimecreated;


CREATE OR REPLACE VIEW "stage"."view_employee_sfdc_profile" AS 
 SELECT DISTINCT u.employee_id__c AS employee_num, p.name AS sfdc_profile
   FROM stage.sfdc_user u
   JOIN ( SELECT u1.employee_id__c, "max"(u1.lastmodifieddate::text) AS max_lastmodifieddate
           FROM stage.sfdc_user u1
          GROUP BY u1.employee_id__c) u2 ON u2.employee_id__c::text = u.employee_id__c::text AND u2.max_lastmodifieddate = u.lastmodifieddate::text
   JOIN stage.sfdc_profile p ON u.profileid::text = p.id::text;


CREATE OR REPLACE VIEW "stage"."view_employee_upsert" AS 
 SELECT qrym.employee_num, qrym.manager_id, qrym.comp_position_code, qrym.ultipro_job_code, qrym.effective_start_date, qrym.record_created_start_date, qrym.record_created_end_date, qrym.last_name, qrym.first_name, qrym.email_address, qrym.original_hire_date, 
        CASE
            WHEN qrym.record_created_end_date IS NOT NULL THEN NULL::timestamp without time zone
            ELSE qrym.termination_date
        END AS termination_date, qrym.last_hire_date, qrym.date_in_job, qrym.employment_agreement_date, qrym.comp_agreement_return_date, qrym.division_code, qrym.division_name, qrym.company_code, qrym.department_code, qrym.department_name, qrym.employeetype, sfdc_profile.sfdc_profile
   FROM ( SELECT qrymain.employeenumber AS employee_num, qrymain.employeeid, qrymain.managernumber AS manager_id, qrymain.calc_comp_position_code AS comp_position_code, qrymain.ultipro_job_code, qrymain.effective_start_date, pg_catalog.lead(qrymain.record_created_start_date, 1)
          OVER( 
          PARTITION BY qrymain.employeeid
          ORDER BY qrymain.record_created_start_date) AS record_created_end_date, qrymain.termination_date, qrymain.record_created_start_date, qrymain.lastname AS last_name, qrymain.firstname AS first_name, qrymain.email_address, qrymain.original_hire_date, qrymain.last_hire_date, qrymain.date_in_job, qrymain.employment_agreement_date, qrymain.comp_agreement_return_date, qrymain.division_code, qrymain.division_name, qrymain.companycode AS company_code, qrymain.department_code, qrymain.department_name, qrymain.employeetype, qrymain.employmentstatus
           FROM ( SELECT 
                        CASE
                            WHEN cur_prev.employeeid::text = cur_prev.prev_employeeid AND cur_prev.managernumber::character varying::text = cur_prev.prev_managernumber AND cur_prev.ultipro_job_code::text = cur_prev.prev_ultipro_job_code AND cur_prev.calc_comp_position_code::text = cur_prev.prev_calc_comp_position_code THEN true
                            ELSE false
                        END AS filter, cur_prev.employeenumber, cur_prev.employeeid, cur_prev.managernumber, cur_prev.record_created_start_date, cur_prev.employmentstatus, cur_prev.calc_comp_position_code, cur_prev.ultipro_job_code, cur_prev.effective_start_date, cur_prev.termination_date, cur_prev.lastname, cur_prev.firstname, cur_prev.email_address, cur_prev.original_hire_date, cur_prev.last_hire_date, cur_prev.date_in_job, cur_prev.employment_agreement_date, cur_prev.comp_agreement_return_date, cur_prev.division_code, cur_prev.division_name, cur_prev.companycode, cur_prev.department_code, cur_prev.department_name, cur_prev.employeetype
                   FROM ( SELECT view_employee_raw.employeenumber, view_employee_raw.record_created_start_date, view_employee_raw.employeeid, view_employee_raw.managernumber, view_employee_raw.employmentstatus, view_employee_raw.calc_comp_position_code, view_employee_raw.ultipro_job_code, view_employee_raw.effective_start_date, view_employee_raw.lastname, view_employee_raw.firstname, view_employee_raw.email_address, view_employee_raw.original_hire_date, view_employee_raw.last_hire_date, view_employee_raw.date_in_job, view_employee_raw.employment_agreement_date, view_employee_raw.comp_agreement_return_date, view_employee_raw.division_code, view_employee_raw.division_name, view_employee_raw.companycode, view_employee_raw.department_code, view_employee_raw.department_name, view_employee_raw.employeetype, view_employee_raw.termination_date, pg_catalog.lead(view_employee_raw.employeeid::text, 1)
                          OVER( 
                          ORDER BY view_employee_raw.employeeid DESC, view_employee_raw.record_created_start_date DESC) AS prev_employeeid, pg_catalog.lead(view_employee_raw.managernumber::character varying::text, 1)
                          OVER( 
                          ORDER BY view_employee_raw.employeeid DESC, view_employee_raw.record_created_start_date DESC) AS prev_managernumber, pg_catalog.lead(view_employee_raw.ultipro_job_code::text, 1)
                          OVER( 
                          ORDER BY view_employee_raw.employeeid DESC, view_employee_raw.record_created_start_date DESC) AS prev_ultipro_job_code, pg_catalog.lead(view_employee_raw.calc_comp_position_code::text, 1)
                          OVER( 
                          ORDER BY view_employee_raw.employeeid DESC, view_employee_raw.record_created_start_date DESC) AS prev_calc_comp_position_code
                           FROM stage.view_employee_raw) cur_prev) qrymain
          WHERE qrymain.filter = false AND qrymain.employmentstatus::text <> 'T'::character varying::text) qrym
   LEFT JOIN stage.view_employee_sfdc_profile sfdc_profile ON qrym.employee_num::character varying::text = sfdc_profile.employee_num::text
  WHERE qrym.record_created_end_date > '2016-12-31 00:00:00'::timestamp without time zone OR qrym.record_created_end_date IS NULL
  ORDER BY qrym.employeeid, qrym.effective_start_date;


CREATE OR REPLACE VIEW "stage"."view_job_title_hierarchy" AS 
 SELECT derived_table1.jobcode, derived_table1.jobdescription, derived_table1."level", derived_table1.abbreviation, 
        CASE
            WHEN derived_table1."level"::text = 'REP'::character varying::text AND derived_table1.jobcode::text <> 'SMFDSM'::character varying::text THEN '1 to 5'::character varying
            WHEN derived_table1.jobcode::text = 'SMFDSM'::character varying::text THEN '1 to 7'::character varying
            WHEN derived_table1."level"::text = 'ASM'::character varying::text THEN '5'::character varying
            WHEN derived_table1.abbreviation::text = 'ARM'::character varying::text THEN '6'::character varying
            WHEN derived_table1.abbreviation::text = 'RM'::character varying::text THEN '6'::character varying
            WHEN derived_table1.abbreviation::text = 'SM'::character varying::text THEN '6'::character varying
            WHEN derived_table1.abbreviation::text = 'SD'::character varying::text THEN '7'::character varying
            WHEN derived_table1.abbreviation::text = 'RD'::character varying::text THEN '8'::character varying
            WHEN derived_table1.abbreviation::text = 'RVP'::character varying::text THEN '8'::character varying
            WHEN derived_table1.abbreviation::text = 'VP'::character varying::text THEN '9'::character varying
            WHEN derived_table1.abbreviation::text = 'CEO'::character varying::text THEN '10'::character varying
            ELSE NULL::character varying
        END AS hierarchy_level
   FROM ( SELECT jc.jobcode, jc.jobdescription, 
                CASE
                    WHEN "substring"(jc.jobcode::text, 1, 3) = 'EM0'::character varying::text THEN 'EXECUTIVE'::character varying
                    WHEN jc.jobcode::text ~~ '%CVP%'::character varying::text THEN 'EXECUTIVE'::character varying
                    WHEN jc.jobcode::text ~~ '%SVP%'::character varying::text THEN 'EXECUTIVE'::character varying
                    WHEN jc.jobcode::text = 'SMFDSM'::character varying::text THEN 'MANAGER'::character varying
                    WHEN "substring"(jc.jobcode::text, 2, 1) = 'M'::character varying::text THEN 'MANAGER'::character varying
                    WHEN jc.jobcode::text ~~ '%ARM%'::character varying::text THEN 'ARM'::character varying
                    WHEN jc.jobcode::text ~~ '%RMY%'::character varying::text THEN 'ARM'::character varying
                    WHEN jc.jobcode::text ~~ '%DASM%'::character varying::text THEN 'ASM'::character varying
                    WHEN "substring"(jc.jobcode::text, 2, 1) = 'R'::character varying::text THEN 'REP'::character varying
                    ELSE NULL::character varying
                END AS "level", 
                CASE
                    WHEN "substring"(jc.jobcode::text, 1, 3) = 'EM0'::character varying::text THEN 'CEO'::character varying
                    WHEN jc.jobcode::text = 'SMFDRCM'::character varying::text THEN 'RM'::character varying
                    WHEN jc.jobcode::text = 'SMFDSM'::character varying::text THEN 'GM-SPRTS'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICRM%'::character varying::text THEN 'TARM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKRM%'::character varying::text THEN 'TARM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICAC%'::character varying::text THEN 'TASC'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKAC%'::character varying::text THEN 'TASC'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICAE%'::character varying::text THEN 'TASE'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKAE%'::character varying::text THEN 'TASE'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICAL%'::character varying::text THEN 'TASL'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKAL%'::character varying::text THEN 'TASL'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICAM%'::character varying::text THEN 'TASM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKAM%'::character varying::text THEN 'TASM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICAR%'::character varying::text THEN 'TASR'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKAR%'::character varying::text THEN 'TASR'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICCC%'::character varying::text THEN 'TCSC'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKCC%'::character varying::text THEN 'TCSC'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICCE%'::character varying::text THEN 'TCSE'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKCE%'::character varying::text THEN 'TCSE'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICCM%'::character varying::text THEN 'TCSM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKCM%'::character varying::text THEN 'TCSM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRICCR%'::character varying::text THEN 'TCSR'::character varying
                    WHEN jc.jobcode::text ~~ '%SRIKCR%'::character varying::text THEN 'TCSR'::character varying
                    WHEN jc.jobcode::text ~~ '%DASM%'::character varying::text THEN 'DASM'::character varying
                    WHEN jc.jobcode::text ~~ '%SAM%'::character varying::text THEN 'SAM'::character varying
                    WHEN jc.jobcode::text ~~ '%TRM%'::character varying::text THEN 'TERM'::character varying
                    WHEN jc.jobcode::text ~~ '%AE1%'::character varying::text THEN 'AE'::character varying
                    WHEN jc.jobcode::text ~~ '%AEY%'::character varying::text THEN 'AE'::character varying
                    WHEN jc.jobcode::text ~~ '%AE2%'::character varying::text THEN 'AE2'::character varying
                    WHEN jc.jobcode::text ~~ '%A2Y%'::character varying::text THEN 'AE2'::character varying
                    WHEN jc.jobcode::text ~~ '%AM1%'::character varying::text THEN 'AM'::character varying
                    WHEN jc.jobcode::text ~~ '%AMY%'::character varying::text THEN 'AM'::character varying
                    WHEN jc.jobcode::text ~~ '%AR1%'::character varying::text THEN 'AR'::character varying
                    WHEN jc.jobcode::text ~~ '%ARY%'::character varying::text THEN 'AR'::character varying
                    WHEN jc.jobcode::text ~~ '%ARM%'::character varying::text THEN 'ARM'::character varying
                    WHEN jc.jobcode::text ~~ '%DRM%'::character varying::text THEN 'RM'::character varying
                    WHEN jc.jobcode::text ~~ '%CM1%'::character varying::text THEN 'CSM'::character varying
                    WHEN jc.jobcode::text ~~ '%DE1%'::character varying::text THEN 'DAE'::character varying
                    WHEN jc.jobcode::text ~~ '%DMD%'::character varying::text THEN 'DM'::character varying
                    WHEN jc.jobcode::text ~~ '%DDS%'::character varying::text THEN 'DS'::character varying
                    WHEN jc.jobcode::text ~~ '%RD1%'::character varying::text THEN 'RD'::character varying
                    WHEN jc.jobcode::text ~~ '%RM1%'::character varying::text THEN 'RM'::character varying
                    WHEN jc.jobcode::text ~~ '%SMFRMY%'::character varying::text THEN 'RM'::character varying
                    WHEN jc.jobcode::text ~~ '%SRFRMY%'::character varying::text THEN 'ARM'::character varying
                    WHEN jc.jobcode::text ~~ '%CRVP%'::character varying::text THEN 'RVP'::character varying
                    WHEN jc.jobcode::text ~~ '%DRVP%'::character varying::text THEN 'RVP'::character varying
                    WHEN jc.jobcode::text ~~ '%RVP%'::character varying::text THEN 'RVP'::character varying
                    WHEN jc.jobcode::text ~~ '%SE1%'::character varying::text THEN 'SAE'::character varying
                    WHEN jc.jobcode::text ~~ '%SEY%'::character varying::text THEN 'SAE'::character varying
                    WHEN jc.jobcode::text ~~ '%SE2%'::character varying::text THEN 'SAE2'::character varying
                    WHEN jc.jobcode::text ~~ '%DSDS%'::character varying::text THEN 'SDS'::character varying
                    WHEN jc.jobcode::text ~~ '%SDS%'::character varying::text THEN 'SAM-DS'::character varying
                    WHEN jc.jobcode::text ~~ '%SCAE%'::character varying::text THEN 'SCAE'::character varying
                    WHEN jc.jobcode::text ~~ '%CSD%'::character varying::text THEN 'SD'::character varying
                    WHEN jc.jobcode::text ~~ '%KSD%'::character varying::text THEN 'SD'::character varying
                    WHEN jc.jobcode::text ~~ '%DCP%'::character varying::text THEN 'SM'::character varying
                    WHEN jc.jobcode::text ~~ '%CSM%'::character varying::text THEN 'SM'::character varying
                    WHEN jc.jobcode::text ~~ '%KSM%'::character varying::text THEN 'SM'::character varying
                    WHEN jc.jobcode::text ~~ '%DSVS%'::character varying::text THEN 'SVS'::character varying
                    WHEN jc.jobcode::text ~~ '%CVP%'::character varying::text THEN 'VP'::character varying
                    WHEN jc.jobcode::text ~~ '%SVP%'::character varying::text THEN 'VP'::character varying
                    ELSE NULL::character varying
                END AS abbreviation
           FROM stage.ultipro_businessrulejob jc
          WHERE "substring"(jc.jobcode::text, 1, 3) = 'SRF'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'SRI'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'SMF'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'SMI'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'SM1'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'SM2'::character varying::text OR "substring"(jc.jobcode::text, 1, 3) = 'EM0'::character varying::text) derived_table1;


CREATE OR REPLACE VIEW "stage"."view_mpe_asset_max_transaction" AS 
 SELECT t2.asset_id, t2."action", t2.tran_date
   FROM stage.mpe_asset_transaction t2
   JOIN ( SELECT t1.asset_id, "max"(t1.tran_date) AS tran_date
           FROM stage.mpe_asset_transaction t1
          WHERE t1."action"::text = 'Add'::character varying::text OR t1."action"::text = 'Cancel'::character varying::text OR t1."action"::text = 'Active'::character varying::text OR t1."action"::text = 'Modify'::character varying::text OR t1."action"::text = 'ReInstate'::character varying::text
          GROUP BY t1.asset_id) t3 ON t2.asset_id = t3.asset_id AND t2.tran_date = t3.tran_date
  WHERE t2."action"::text = 'Add'::character varying::text OR t2."action"::text = 'Cancel'::character varying::text OR t2."action"::text = 'Active'::character varying::text OR t2."action"::text = 'Modify'::character varying::text OR t2."action"::text = 'ReInstate'::character varying::text;


CREATE OR REPLACE VIEW "stage"."view_mpe_order_sku_asset_map" AS 
 SELECT mpe_order_sku_asset_map.mpe_order_number, mpe_order_sku_asset_map.product_sku, mpe_order_sku_asset_map.parent_asset_id, mpe_order_sku_asset_map.asset_sku, mpe_order_sku_asset_map.asset_description, "max"(mpe_order_sku_asset_map.asset_id) AS asset_id, mpe_order_sku_asset_map.update_date
   FROM stage.mpe_order_sku_asset_map
  GROUP BY mpe_order_sku_asset_map.mpe_order_number, mpe_order_sku_asset_map.product_sku, mpe_order_sku_asset_map.parent_asset_id, mpe_order_sku_asset_map.asset_sku, mpe_order_sku_asset_map.asset_description, mpe_order_sku_asset_map.update_date;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad" AS 
 SELECT DISTINCT a.business_id_hidden__c, a.product, a.asset_id, a.asset_code, a.sold_date, a.order_number::character varying AS order_number, a.source_system, a.original_acq_date, a.latest_acq_date, min(a.submitted) AS submitted, min(a.consultation_complete_date) AS consultation_complete_date, min(a.go_live_date) AS go_live_date, "max"(a.cancel_date) AS cancel_date, "max"(a.end_date) AS end_date, a.new_existing, a.incremental_sale, min(a.primary_employee_id::text)::character varying AS primary_employee_id, a.secondary_employee_id, a.tertiary_employee_id, a.unit_sold, "max"(a.monthly_spend)::numeric(38,2) AS monthly_spend, a.discount_amount, a.discount_period, a.one_time_fees, a.one_time_fees_discount, a.spread_fees, a.spread_period, min(a.status::text)::character varying(255) AS status, a.sale_type::character varying(255) AS sale_type, "max"(a.drop_date) AS drop_date, a.bundle_id::character varying(40) AS bundle_id
   FROM ( SELECT p.business_id_hidden__c::bigint AS business_id_hidden__c, 
                CASE
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text THEN op.book_code
                    ELSE ph.anaplan_level_4
                END AS product, NULL::character varying::character varying(50) AS asset_id, NULL::character varying::character varying(50) AS asset_code, p.sale_date__c AS sold_date, (op.contract_key::character varying::text || '-'::character varying::text) || op.region_key::character varying::text AS order_number, 'DIAD'::character varying AS source_system, mc.original_acq_date, mc.latest_acq_date, min(COALESCE(pm.server_processed_date, p.sale_date__c)) AS submitted, p.consultation_complete_date__c AS consultation_complete_date, p.go_live_date__c AS go_live_date, "max"(
                CASE
                    WHEN p.status__c::text = 'Cancelled'::character varying::text THEN op.product_end_date::date
                    ELSE NULL::date
                END) AS cancel_date, "max"(
                CASE
                    WHEN p.status__c::text <> 'Cancelled'::character varying::text THEN op.product_end_date::date
                    ELSE NULL::date
                END) AS end_date, mc.new_existing, 
                CASE
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND NOT p.prior_contract__c IS NULL THEN 0
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND p.prior_contract__c IS NULL THEN 1
                    ELSE 0
                END AS incremental_sale, op.sold_sales_rep_code::character varying(40) AS primary_employee_id, NULL::character varying::character varying(40) AS secondary_employee_id, NULL::character varying::character varying(40) AS tertiary_employee_id, 1 AS unit_sold, 
                CASE
                    WHEN op.product_family::text = 'SEO'::character varying::text OR op.product_family::text = 'Online Display'::character varying::text OR op.product_family::text = 'Search'::character varying::text THEN dp3.monthly_cost
                    ELSE COALESCE(p.monthly_spend__c, 0::numeric::numeric(18,0))
                END AS monthly_spend, 0 AS discount_amount, 0 AS discount_period, 0::numeric::numeric(18,0)::numeric(18,2) AS one_time_fees, 0 AS one_time_fees_discount, 0 AS spread_fees, 0 AS spread_period, p.status__c AS status, 
                CASE
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND NOT p.prior_contract__c IS NULL THEN 'Renewal'::character varying
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND p.prior_contract__c IS NULL AND min(COALESCE(pm.server_processed_date, p.sale_date__c)) <= mc.latest_acq_date THEN 'New'::character varying
                    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND p.prior_contract__c IS NULL AND min(COALESCE(pm.server_processed_date, p.sale_date__c)) > mc.latest_acq_date THEN 'Insert'::character varying
                    ELSE NULL::character varying::character varying(255)
                END AS sale_type, p.dropped_date__c AS drop_date, "max"(op.bundle_uuid::text) AS bundle_id
           FROM stage.sfdc_product p
      JOIN stage.sfdc_stage_diad_product_asset op ON p.external_product_id__c::text = op.external_product_id::text AND NOT (op.book_code::text = 'NETP'::character varying::text OR op.book_code::text = 'NETMA'::character varying::text OR op.book_code::text = 'NETW'::character varying::text)
   LEFT JOIN ( SELECT dp1.external_product_id, dp1.product_start_date, dp1.monthly_cost
              FROM stage.sfdc_stage_diad_product_asset dp1
         JOIN ( SELECT dp.external_product_id, min(dp.product_start_date::text) AS min_product_start_date
                      FROM stage.sfdc_stage_diad_product_asset dp
                     WHERE dp.product_family::text = 'SEO'::character varying::text OR dp.product_family::text = 'Online Display'::character varying::text OR dp.product_family::text = 'Search'::character varying::text
                     GROUP BY dp.external_product_id) dp2 ON dp1.external_product_id::text = dp2.external_product_id::text AND dp1.product_start_date::text = dp2.min_product_start_date) dp3 ON p.external_product_id__c::text = dp3.external_product_id::text
   LEFT JOIN stage.sfa_proposal_master pm ON pm.proposal_guid::text = op.contract_reference_link::text
   LEFT JOIN customer mc ON p.business_id_hidden__c::text = mc.business_id::character varying::text
   JOIN product_hierarchy ph ON op.asset_code::text = ph.product_code::text AND ph.effective_end_date IS NULL
   JOIN stage.sfdc_user u1 ON p.actual_rep__c::text = u1.id::text
   JOIN stage.sfdc_user u2 ON p.assigned_rep__c::text = u2.id::text
   JOIN customer_product_incremental cpi ON cpi.parent_business_id = mc.business_id AND cpi.anaplan_level_3::text = ph.anaplan_level_3::text
  WHERE (op.product_family::text = 'Yellow Pages'::character varying::text OR op.product_family::text = 'Direct Mail'::character varying::text OR op.product_family::text = 'Print - Other'::character varying::text) AND p.sale_date__c >= '2015-01-01'::date OR NOT (op.product_family::text = 'Yellow Pages'::character varying::text OR op.product_family::text = 'Direct Mail'::character varying::text OR op.product_family::text = 'Print - Other'::character varying::text) AND p.sale_date__c >= '2000-01-01'::date
  GROUP BY p.business_id_hidden__c::bigint, 
CASE
    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text THEN op.book_code
    ELSE ph.anaplan_level_4
END, p.sale_date__c, (op.contract_key::character varying::text || '-'::character varying::text) || op.region_key::character varying::text, mc.original_acq_date, mc.latest_acq_date, p.consultation_complete_date__c, p.go_live_date__c, mc.new_existing, 
CASE
    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND NOT p.prior_contract__c IS NULL THEN 0
    WHEN ph.anaplan_level_3::text = 'Print Family'::character varying::text AND p.prior_contract__c IS NULL THEN 1
    ELSE 0
END, op.sold_sales_rep_code, 
CASE
    WHEN op.product_family::text = 'SEO'::character varying::text OR op.product_family::text = 'Online Display'::character varying::text OR op.product_family::text = 'Search'::character varying::text THEN dp3.monthly_cost
    ELSE COALESCE(p.monthly_spend__c, 0::numeric::numeric(18,0))
END, p.status__c, ph.anaplan_level_3, p.prior_contract__c, p.dropped_date__c) a
  GROUP BY a.business_id_hidden__c, a.product, a.asset_id, a.asset_code, a.sold_date, a.order_number, a.source_system, a.original_acq_date, a.latest_acq_date, a.new_existing, a.incremental_sale, a.secondary_employee_id, a.tertiary_employee_id, a.unit_sold, a.discount_amount, a.discount_period, a.one_time_fees, a.one_time_fees_discount, a.spread_fees, a.spread_period, a.sale_type, a.bundle_id
 HAVING sum(a.monthly_spend) > 0::numeric::numeric(18,0);


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_detect_changes" AS 
 SELECT DISTINCT src.business_id_hidden__c, src.product, src.asset_id, src.asset_code, src.sold_date, src.order_number, src.source_system, src.original_acq_date, src.latest_acq_date, src.submitted, src.consultation_complete_date, src.go_live_date, src.cancel_date, src.end_date, src.new_existing, src.incremental_sale, src.primary_employee_id, src.secondary_employee_id, src.tertiary_employee_id, src.unit_sold, src.monthly_spend, src.discount_amount, src.discount_period, src.one_time_fees, src.one_time_fees_discount, src.spread_fees, src.spread_period, src.status, src.sale_type, 
        CASE
            WHEN pub.selling_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.selling_transaction_id IS NULL AND (
            CASE
                WHEN src.consultation_complete_date = pub.consultation_complete_date OR src.consultation_complete_date IS NULL AND pub.consultation_complete_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.go_live_date = pub.go_live_date OR src.go_live_date IS NULL AND pub.go_live_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.cancel_date = pub.cancel_date OR src.cancel_date IS NULL AND pub.cancel_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.end_date::timestamp without time zone = pub.end_date::timestamp without time zone OR src.end_date IS NULL AND pub.end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_employee_id::text = pub.primary_employee_id::text OR src.primary_employee_id IS NULL AND pub.primary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.secondary_employee_id::text = pub.secondary_employee_id::text OR src.secondary_employee_id IS NULL AND pub.secondary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.tertiary_employee_id::text = pub.tertiary_employee_id::text OR src.tertiary_employee_id IS NULL AND pub.tertiary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.unit_sold = pub.unit_sold OR src.unit_sold IS NULL AND pub.unit_sold IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.monthly_spend = pub.product_monthly_revenue OR src.monthly_spend IS NULL AND pub.product_monthly_revenue IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_amount::numeric::numeric(18,0)::numeric(18,2) = pub.discount_amount OR src.discount_amount IS NULL AND pub.discount_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_period::numeric::numeric(18,0) = pub.discount_period::numeric::numeric(18,0) OR src.discount_period IS NULL AND pub.discount_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees = pub.one_time_fees OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees_discount::numeric::numeric(18,0)::numeric(18,2) = pub.one_time_fees_discount OR src.one_time_fees_discount IS NULL AND pub.one_time_fees_discount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees::numeric::numeric(18,0)::numeric(18,2) = pub.spread_fees OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_period = pub.spread_period OR src.spread_period IS NULL AND pub.spread_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.status::text = pub.status::text OR src.status IS NULL AND pub.status IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.drop_date = pub.drop_date OR src.drop_date IS NULL AND pub.drop_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.bundle_id::text = pub.bundle_id::text OR src.bundle_id IS NULL AND pub.bundle_id IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.selling_transaction_id, pub.initial_load_date, src.drop_date, src.bundle_id
   FROM stage.view_selling_transaction_diad src
   LEFT JOIN selling_transaction pub ON src.business_id_hidden__c = pub.business_id AND src.product::text = pub.product::text AND src.order_number::text = pub.order_number::text AND src.sold_date = pub.sold_date;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_iyp" AS 
 SELECT mc1.business_id AS business_id_hidden__c, ph.anaplan_level_4 AS product, NULL::character varying::character varying(50) AS asset_id, NULL::character varying::character varying(50) AS asset_code, min(cl.sale_date) AS sold_date, ((cl.contract_key::character varying::text || '-'::character varying::text) || cl.region_key::character varying::text)::character varying AS order_number, 'DIAD'::character varying AS source_system, mc1.original_acq_date, mc1.latest_acq_date, min(COALESCE(pm.server_processed_date, cl.sale_date)) AS submitted, NULL::date AS consultation_complete_date, cl.requested_start_date AS go_live_date, NULL::date AS cancel_date, date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone)::date AS end_date, mc1.new_existing, 0 AS incremental_sale, cl.salesrep_code::character varying(40) AS primary_employee_id, NULL::character varying::character varying(40) AS secondary_employee_id, NULL::character varying::character varying(40) AS tertiary_employee_id, 1 AS unit_sold, sum(cl.rate_override_amount) AS monthly_spend, 0 AS discount_amount, 0 AS discount_period, 0::numeric::numeric(18,0)::numeric(18,2) AS one_time_fees, 0 AS one_time_fees_discount, 0 AS spread_fees, 0 AS spread_period, 
        CASE
            WHEN cl.requested_start_date > 'now'::character varying::date THEN 'Pending'::character varying
            WHEN cl.requested_start_date < 'now'::character varying::date AND date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) >= 'now'::character varying::date THEN 'Active'::character varying
            WHEN date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) < 'now'::character varying::date THEN 'Expired'::character varying
            ELSE 'Other'::character varying
        END AS status, NULL::character varying::character varying(40) AS sale_type, min(lower(cl.uuid::text))::character varying(40) AS min_uuid, cl.drop_date, cl.net_coverage, COALESCE(cl.pkg_guid, cl.bundle_line_item_uuid) AS bundle_id
   FROM stage.diad_web_contract_lines cl
   JOIN stage.sfdc_master_customer mc ON cl.customer_key = mc.customer_key__c AND cl.region_key = mc.region_key__c
   JOIN customer mc1 ON mc.business_id_hidden__c = mc1.business_id
   JOIN product_hierarchy ph ON cl.udac_code::text = ph.product_code::text AND ph.effective_end_date IS NULL
   LEFT JOIN stage.contract_proposal_guid_map m ON cl.proposalid::text = m.proposalid::text AND cl.contract_key = m.contract_key AND cl.region_key = m.region_key AND cl.uuid::text = m.uuid::text
   LEFT JOIN stage.sfa_proposal_master pm ON pm.proposal_guid::text = m.proposal_guid::text
   JOIN customer_product_incremental cpi ON cpi.parent_business_id = mc1.business_id AND cpi.anaplan_level_3::text = ph.anaplan_level_3::text
  WHERE ph.anaplan_level_4::text = 'IYP'::character varying::text AND cl.rate_override_amount > 0::numeric::numeric(18,0)::numeric(9,2)
  GROUP BY mc1.business_id, ph.anaplan_level_4, (cl.contract_key::character varying::text || '-'::character varying::text) || cl.region_key::character varying::text, mc1.original_acq_date, mc1.latest_acq_date, mc1.new_existing, cl.requested_start_date, date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone)::date, cl.salesrep_code::character varying(40), 
CASE
    WHEN cl.requested_start_date > 'now'::character varying::date THEN 'Pending'::character varying
    WHEN cl.requested_start_date < 'now'::character varying::date AND date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) >= 'now'::character varying::date THEN 'Active'::character varying
    WHEN date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) < 'now'::character varying::date THEN 'Expired'::character varying
    ELSE 'Other'::character varying
END, cl.drop_date, cl.net_coverage, COALESCE(cl.pkg_guid, cl.bundle_line_item_uuid);


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_iyp_detect_changes" AS 
 SELECT DISTINCT src.business_id_hidden__c, src.product, src.asset_id, src.asset_code, src.sold_date, src.order_number, src.source_system, src.original_acq_date, src.latest_acq_date, src.submitted, src.consultation_complete_date, src.go_live_date, src.cancel_date, src.end_date, src.new_existing, src.incremental_sale, src.primary_employee_id, src.secondary_employee_id, src.tertiary_employee_id, src.unit_sold, src.monthly_spend, src.discount_amount, src.discount_period, src.one_time_fees, src.one_time_fees_discount, src.spread_fees, src.spread_period, src.status, src.sale_type, src.min_uuid, 
        CASE
            WHEN pub.selling_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.selling_transaction_id IS NULL AND (
            CASE
                WHEN src.consultation_complete_date::character varying::text = pub.consultation_complete_date::character varying::text OR src.consultation_complete_date IS NULL AND pub.consultation_complete_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.go_live_date = pub.go_live_date OR src.go_live_date IS NULL AND pub.go_live_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.cancel_date::character varying::text = pub.cancel_date::character varying::text OR src.cancel_date IS NULL AND pub.cancel_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.end_date::timestamp without time zone = pub.end_date::timestamp without time zone OR src.end_date IS NULL AND pub.end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_employee_id::text = pub.primary_employee_id::text OR src.primary_employee_id IS NULL AND pub.primary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.secondary_employee_id::text = pub.secondary_employee_id::text OR src.secondary_employee_id IS NULL AND pub.secondary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.tertiary_employee_id::text = pub.tertiary_employee_id::text OR src.tertiary_employee_id IS NULL AND pub.tertiary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.unit_sold = pub.unit_sold OR src.unit_sold IS NULL AND pub.unit_sold IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.monthly_spend = pub.product_monthly_revenue OR src.monthly_spend IS NULL AND pub.product_monthly_revenue IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_amount::numeric::numeric(18,0)::numeric(18,2) = pub.discount_amount OR src.discount_amount IS NULL AND pub.discount_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_period::numeric::numeric(18,0) = pub.discount_period::numeric::numeric(18,0) OR src.discount_period IS NULL AND pub.discount_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees = pub.one_time_fees OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees_discount::numeric::numeric(18,0)::numeric(18,2) = pub.one_time_fees_discount OR src.one_time_fees_discount IS NULL AND pub.one_time_fees_discount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees::numeric::numeric(18,0)::numeric(18,2) = pub.spread_fees OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_period = pub.spread_period OR src.spread_period IS NULL AND pub.spread_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.status::text = pub.status::text OR src.status IS NULL AND pub.status IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.drop_date = pub.drop_date OR src.drop_date IS NULL AND pub.drop_date IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.selling_transaction_id, pub.initial_load_date, src.drop_date, src.net_coverage AS iyp_net_coverage, src.bundle_id
   FROM stage.view_selling_transaction_diad_iyp src
   LEFT JOIN selling_transaction pub ON src.business_id_hidden__c = pub.business_id AND src.product::text = pub.product::text AND src.order_number::text = pub.order_number::text AND src.sold_date = pub.sold_date AND src.go_live_date = pub.go_live_date AND src.net_coverage::text = pub.iyp_net_coverage::text;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_mbid_map" AS 
 SELECT DISTINCT wb.contract_key, wb.region_key, ws.mbid
   FROM stage.contract_webreach_spent ws
   JOIN stage.contract_webreach_bill wb ON ws.webreach_key::double precision = wb.webreach_key AND ws.region_key::numeric::numeric(18,0) = wb.region_key;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_web" AS 
 SELECT mc1.business_id AS business_id_hidden__c, ph.anaplan_level_4 AS product, NULL::character varying::character varying(50) AS asset_id, NULL::character varying::character varying(50) AS asset_code, min(cl.sale_date) AS sold_date, ((cl.contract_key::character varying::text || '-'::character varying::text) || cl.region_key::character varying::text)::character varying AS order_number, 'DIAD'::character varying AS source_system, mc1.original_acq_date, mc1.latest_acq_date, min(COALESCE(pm.server_processed_date, cl.sale_date)) AS submitted, NULL::date AS consultation_complete_date, cl.requested_start_date AS go_live_date, NULL::date AS cancel_date, date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone)::date AS end_date, mc1.new_existing, 0 AS incremental_sale, cl.salesrep_code::character varying(40) AS primary_employee_id, NULL::character varying::character varying(40) AS secondary_employee_id, NULL::character varying::character varying(40) AS tertiary_employee_id, 1 AS unit_sold, sum(cl.rate_override_amount) AS monthly_spend, 0 AS discount_amount, 0 AS discount_period, 0::numeric::numeric(18,0)::numeric(18,2) AS one_time_fees, 0 AS one_time_fees_discount, 0 AS spread_fees, 0 AS spread_period, 
        CASE
            WHEN cl.requested_start_date > 'now'::character varying::date THEN 'Pending'::character varying
            WHEN cl.requested_start_date < 'now'::character varying::date AND date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) >= 'now'::character varying::date THEN 'Active'::character varying
            WHEN date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) < 'now'::character varying::date THEN 'Expired'::character varying
            ELSE 'Other'::character varying
        END AS status, NULL::character varying::character varying(40) AS sale_type, min(lower(cl.uuid::text))::character varying(40) AS min_uuid, cl.drop_date
   FROM stage.diad_web_contract_lines cl
   JOIN stage.sfdc_master_customer mc ON cl.customer_key = mc.customer_key__c AND cl.region_key = mc.region_key__c
   JOIN customer mc1 ON mc.business_id_hidden__c = mc1.business_id
   JOIN product_hierarchy ph ON cl.udac_code::text = ph.product_code::text AND ph.effective_end_date IS NULL
   LEFT JOIN stage.contract_proposal_guid_map m ON cl.proposalid::text = m.proposalid::text AND cl.contract_key = m.contract_key AND cl.region_key = m.region_key AND cl.uuid::text = m.uuid::text
   LEFT JOIN stage.sfa_proposal_master pm ON pm.proposal_guid::text = m.proposal_guid::text
   JOIN customer_product_incremental cpi ON cpi.parent_business_id = mc1.business_id AND cpi.anaplan_level_3::text = ph.anaplan_level_3::text
  WHERE ph.anaplan_level_4::text <> 'IYP'::character varying::text AND cl.rate_override_amount > 0::numeric::numeric(18,0)::numeric(9,2)
  GROUP BY mc1.business_id, ph.anaplan_level_4, (cl.contract_key::character varying::text || '-'::character varying::text) || cl.region_key::character varying::text, mc1.original_acq_date, mc1.latest_acq_date, mc1.new_existing, cl.requested_start_date, date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone)::date, cl.salesrep_code::character varying(40), 
CASE
    WHEN cl.requested_start_date > 'now'::character varying::date THEN 'Pending'::character varying
    WHEN cl.requested_start_date < 'now'::character varying::date AND date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) >= 'now'::character varying::date THEN 'Active'::character varying
    WHEN date_add('months'::character varying::text, cl.months_to_bill::bigint, cl.requested_start_date::timestamp without time zone) < 'now'::character varying::date THEN 'Expired'::character varying
    ELSE 'Other'::character varying
END, cl.drop_date;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_diad_web_detect_changes" AS 
 SELECT DISTINCT src.business_id_hidden__c, src.product, src.asset_id, src.asset_code, src.sold_date, src.order_number, src.source_system, src.original_acq_date, src.latest_acq_date, src.submitted, src.consultation_complete_date, src.go_live_date, src.cancel_date, src.end_date, src.new_existing, src.incremental_sale, src.primary_employee_id, src.secondary_employee_id, src.tertiary_employee_id, src.unit_sold, src.monthly_spend, src.discount_amount, src.discount_period, src.one_time_fees, src.one_time_fees_discount, src.spread_fees, src.spread_period, src.status, src.sale_type, src.min_uuid, 
        CASE
            WHEN pub.selling_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.selling_transaction_id IS NULL AND (
            CASE
                WHEN src.consultation_complete_date::character varying::text = pub.consultation_complete_date::character varying::text OR src.consultation_complete_date IS NULL AND pub.consultation_complete_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.go_live_date = pub.go_live_date OR src.go_live_date IS NULL AND pub.go_live_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.cancel_date::character varying::text = pub.cancel_date::character varying::text OR src.cancel_date IS NULL AND pub.cancel_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.end_date::timestamp without time zone = pub.end_date::timestamp without time zone OR src.end_date IS NULL AND pub.end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_employee_id::text = pub.primary_employee_id::text OR src.primary_employee_id IS NULL AND pub.primary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.secondary_employee_id::text = pub.secondary_employee_id::text OR src.secondary_employee_id IS NULL AND pub.secondary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.tertiary_employee_id::text = pub.tertiary_employee_id::text OR src.tertiary_employee_id IS NULL AND pub.tertiary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.unit_sold = pub.unit_sold OR src.unit_sold IS NULL AND pub.unit_sold IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.monthly_spend = pub.product_monthly_revenue OR src.monthly_spend IS NULL AND pub.product_monthly_revenue IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_amount::numeric::numeric(18,0)::numeric(18,2) = pub.discount_amount OR src.discount_amount IS NULL AND pub.discount_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_period::numeric::numeric(18,0) = pub.discount_period::numeric::numeric(18,0) OR src.discount_period IS NULL AND pub.discount_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees = pub.one_time_fees OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees_discount::numeric::numeric(18,0)::numeric(18,2) = pub.one_time_fees_discount OR src.one_time_fees_discount IS NULL AND pub.one_time_fees_discount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees::numeric::numeric(18,0)::numeric(18,2) = pub.spread_fees OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_period = pub.spread_period OR src.spread_period IS NULL AND pub.spread_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.status::text = pub.status::text OR src.status IS NULL AND pub.status IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.drop_date = pub.drop_date OR src.drop_date IS NULL AND pub.drop_date IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.selling_transaction_id, pub.initial_load_date, src.drop_date
   FROM stage.view_selling_transaction_diad_web src
   LEFT JOIN selling_transaction pub ON src.business_id_hidden__c = pub.business_id AND src.product::text = pub.product::text AND src.order_number::text = pub.order_number::text AND src.sold_date = pub.sold_date AND src.go_live_date = pub.go_live_date;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_hgcp" AS 
 SELECT view_selling_transaction_hgcp_not_search_display.business_id, view_selling_transaction_hgcp_not_search_display.product, view_selling_transaction_hgcp_not_search_display.asset_id, view_selling_transaction_hgcp_not_search_display.asset_code, view_selling_transaction_hgcp_not_search_display.sold_date, view_selling_transaction_hgcp_not_search_display.order_number, view_selling_transaction_hgcp_not_search_display.source_system, view_selling_transaction_hgcp_not_search_display.original_acq_date, view_selling_transaction_hgcp_not_search_display.latest_acq_date, view_selling_transaction_hgcp_not_search_display.submitted_on, view_selling_transaction_hgcp_not_search_display.consultation_complete_date, view_selling_transaction_hgcp_not_search_display.go_live_date, view_selling_transaction_hgcp_not_search_display.cancel_date, view_selling_transaction_hgcp_not_search_display.end_date, view_selling_transaction_hgcp_not_search_display.new_existing, view_selling_transaction_hgcp_not_search_display.incremental_sale, view_selling_transaction_hgcp_not_search_display.primary_employee_id, view_selling_transaction_hgcp_not_search_display.secondary_employee_id, view_selling_transaction_hgcp_not_search_display.tertiary_employee_id, view_selling_transaction_hgcp_not_search_display.unit_sold, view_selling_transaction_hgcp_not_search_display.monthly_spend, view_selling_transaction_hgcp_not_search_display.discount_amount, view_selling_transaction_hgcp_not_search_display.discount_period, view_selling_transaction_hgcp_not_search_display.one_time_fees, view_selling_transaction_hgcp_not_search_display.one_time_fees_discount, view_selling_transaction_hgcp_not_search_display.spread_fees, view_selling_transaction_hgcp_not_search_display.spread_period, view_selling_transaction_hgcp_not_search_display.status, view_selling_transaction_hgcp_not_search_display.sale_type, view_selling_transaction_hgcp_not_search_display.p2d, view_selling_transaction_hgcp_not_search_display.p2d_expected, view_selling_transaction_hgcp_not_search_display.p2d_serviced_dollars, view_selling_transaction_hgcp_not_search_display.p2d_serviced_number, view_selling_transaction_hgcp_not_search_display.p2d_total, view_selling_transaction_hgcp_not_search_display.recent_sales_initiative, view_selling_transaction_hgcp_not_search_display.upsell, view_selling_transaction_hgcp_not_search_display.drop_date
   FROM stage.view_selling_transaction_hgcp_not_search_display
UNION ALL 
 SELECT view_selling_transaction_hgcp_search_display.business_id, view_selling_transaction_hgcp_search_display.product, view_selling_transaction_hgcp_search_display.asset_id, view_selling_transaction_hgcp_search_display.asset_code, view_selling_transaction_hgcp_search_display.sold_date, view_selling_transaction_hgcp_search_display.order_number, view_selling_transaction_hgcp_search_display.source_system, view_selling_transaction_hgcp_search_display.original_acq_date, view_selling_transaction_hgcp_search_display.latest_acq_date, view_selling_transaction_hgcp_search_display.submitted_on, view_selling_transaction_hgcp_search_display.consultation_complete_date, view_selling_transaction_hgcp_search_display.go_live_date, view_selling_transaction_hgcp_search_display.cancel_date, view_selling_transaction_hgcp_search_display.end_date, view_selling_transaction_hgcp_search_display.new_existing, view_selling_transaction_hgcp_search_display.incremental_sale, view_selling_transaction_hgcp_search_display.primary_employee_id, view_selling_transaction_hgcp_search_display.secondary_employee_id, view_selling_transaction_hgcp_search_display.tertiary_employee_id, view_selling_transaction_hgcp_search_display.unit_sold, view_selling_transaction_hgcp_search_display.monthly_spend, view_selling_transaction_hgcp_search_display.discount_amount, view_selling_transaction_hgcp_search_display.discount_period, view_selling_transaction_hgcp_search_display.one_time_fee AS one_time_fees, view_selling_transaction_hgcp_search_display.one_time_fees_discount, view_selling_transaction_hgcp_search_display.spread_fees, view_selling_transaction_hgcp_search_display.spread_period, view_selling_transaction_hgcp_search_display.status, view_selling_transaction_hgcp_search_display.sale_type, view_selling_transaction_hgcp_search_display.p2d, view_selling_transaction_hgcp_search_display.p2d_expected, view_selling_transaction_hgcp_search_display.p2d_serviced_dollars, view_selling_transaction_hgcp_search_display.p2d_serviced_number, view_selling_transaction_hgcp_search_display.p2d_total, view_selling_transaction_hgcp_search_display.recent_sales_initiative, view_selling_transaction_hgcp_search_display.upsell, view_selling_transaction_hgcp_search_display.drop_date
   FROM stage.view_selling_transaction_hgcp_search_display;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_hgcp_detect_changes" AS 
 SELECT DISTINCT src.business_id AS business_id_hidden__c, src.product, src.asset_id, src.asset_code, src.sold_date, src.order_number, src.source_system, src.original_acq_date, src.latest_acq_date, src.submitted_on AS submitted, src.consultation_complete_date, src.go_live_date, src.cancel_date, src.end_date, src.new_existing, src.incremental_sale, src.primary_employee_id, src.secondary_employee_id, src.tertiary_employee_id, src.unit_sold, src.monthly_spend, src.discount_amount, src.discount_period, src.one_time_fees, src.one_time_fees_discount, src.spread_fees, src.spread_period, src.status, src.sale_type, src.p2d, src.p2d_expected, src.p2d_serviced_dollars, src.p2d_serviced_number, src.p2d_total, src.recent_sales_initiative, src.upsell, 
        CASE
            WHEN pub.selling_transaction_id IS NULL THEN 'New'::character varying
            WHEN NOT pub.selling_transaction_id IS NULL AND (
            CASE
                WHEN src.consultation_complete_date = pub.consultation_complete_date OR src.consultation_complete_date IS NULL AND pub.consultation_complete_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.go_live_date = pub.go_live_date OR src.go_live_date IS NULL AND pub.go_live_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.cancel_date = pub.cancel_date OR src.cancel_date IS NULL AND pub.cancel_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.end_date = pub.end_date OR src.end_date IS NULL AND pub.end_date IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.primary_employee_id::text = pub.primary_employee_id::text OR src.primary_employee_id IS NULL AND pub.primary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.secondary_employee_id::text = pub.secondary_employee_id::text OR src.secondary_employee_id IS NULL AND pub.secondary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.tertiary_employee_id::text = pub.tertiary_employee_id::text OR src.tertiary_employee_id IS NULL AND pub.tertiary_employee_id IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.unit_sold = pub.unit_sold OR src.unit_sold IS NULL AND pub.unit_sold IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.monthly_spend = pub.product_monthly_revenue OR src.monthly_spend IS NULL AND pub.product_monthly_revenue IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_amount = pub.discount_amount OR src.discount_amount IS NULL AND pub.discount_amount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.discount_period::numeric::numeric(18,0) = pub.discount_period::numeric::numeric(18,0) OR src.discount_period IS NULL AND pub.discount_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees = pub.one_time_fees OR src.one_time_fees IS NULL AND pub.one_time_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.one_time_fees_discount = pub.one_time_fees_discount OR src.one_time_fees_discount IS NULL AND pub.one_time_fees_discount IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_fees = pub.spread_fees OR src.spread_fees IS NULL AND pub.spread_fees IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.spread_period = pub.spread_period OR src.spread_period IS NULL AND pub.spread_period IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.status::text = pub.status::text OR src.status IS NULL AND pub.status IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.p2d::text = pub.p2d::text OR src.p2d IS NULL AND pub.p2d IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.p2d_expected = pub.p2d_expected OR src.p2d_expected IS NULL AND pub.p2d_expected IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.p2d_serviced_dollars = pub.p2d_serviced_dollars OR src.p2d_serviced_dollars IS NULL AND pub.p2d_serviced_dollars IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.p2d_serviced_number = pub.p2d_serviced_number OR src.p2d_serviced_number IS NULL AND pub.p2d_serviced_number IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.p2d_total = pub.p2d_total OR src.p2d_total IS NULL AND pub.p2d_total IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.upsell = pub.upsell OR src.upsell IS NULL AND pub.upsell IS NULL THEN 0
                ELSE 1
            END + 
            CASE
                WHEN src.drop_date = pub.drop_date OR src.drop_date IS NULL AND pub.drop_date IS NULL THEN 0
                ELSE 1
            END) > 0 THEN 'Update'::character varying
            ELSE 'None'::character varying
        END AS new_update, 'now'::character varying::timestamp without time zone AS today, pub.selling_transaction_id, pub.initial_load_date, src.drop_date
   FROM stage.view_selling_transaction_hgcp src
   LEFT JOIN selling_transaction pub ON src.business_id::character varying::text = pub.business_id::character varying::text AND src.product::text = pub.product::text AND src.asset_id::text = pub.asset_id::character varying::text AND src.order_number::text = pub.order_number::text;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_hgcp_not_search_display" AS 
 SELECT r.business_id::bigint AS business_id, r.anaplan_level_4 AS product, r.product_asset_id AS asset_id, r.product_asset_code AS asset_code, r.sold_date, r.order_number, r.source_system, r.original_acq_date, r.latest_acq_date, r.submitted_on, r.consultation_complete_date, r.go_live_date, 
        CASE
            WHEN r.status::text = 'Expired'::character varying::text AND (r."action"::text = 'Cancel'::character varying::text OR COALESCE(r.renewal_status, r.renewal_type)::text = 'Cancelled'::character varying::text) THEN r.end_date
            ELSE NULL::date
        END AS cancel_date, 
        CASE
            WHEN r.status::text = 'Expired'::character varying::text AND r."action"::text <> 'Cancel'::character varying::text AND COALESCE(r.renewal_status, r.renewal_type)::text <> 'Cancelled'::character varying::text THEN r.end_date
            ELSE NULL::date
        END AS end_date, r.new_existing, 0 AS incremental_sale, r.primary_employee_id, r.secondary_employee_id, r.tertiary_employee_id, r.unit_sold, sum(r.full_amount)::numeric(18,2) AS monthly_spend, sum(
        CASE
            WHEN r.billingperiod::text <> 'OTF'::character varying::text AND COALESCE(r.discountduration / 30::numeric::numeric(18,0), 0::numeric::numeric(18,0)) <> 0::numeric::numeric(18,0) THEN COALESCE(r.discountamount, 0::numeric::numeric(18,0))
            ELSE 0::numeric::numeric(18,0)
        END)::numeric(18,2) AS discount_amount, min(
        CASE
            WHEN r.billingperiod::text <> 'OTF'::character varying::text THEN COALESCE(r.discountduration / 30::numeric::numeric(18,0), 0::numeric::numeric(18,0))
            ELSE 0::numeric::numeric(18,0)
        END)::integer AS discount_period, sum(r.one_time_fees)::numeric(18,2) AS one_time_fees, sum(
        CASE
            WHEN r.billingperiod::text = 'OTF'::character varying::text THEN COALESCE(r.discountamount, 0::numeric::numeric(18,0))
            ELSE 0::numeric::numeric(18,0)
        END)::numeric(18,2) AS one_time_fees_discount, sum(r.spread_fees)::numeric(18,2) AS spread_fees, "max"(
        CASE
            WHEN r.spread_fees > 0::numeric::numeric(18,0)::numeric(18,2) THEN r.payment_term
            ELSE '0'::character varying
        END::text)::integer AS spread_period, COALESCE(r."action", r.status) AS status, NULL::character varying::character varying(255) AS sale_type, r.p2d, r.p2d_expected, r.p2d_serviced_dollars, r.p2d_serviced_number, r.p2d_total, r.recent_sales_initiative, r.upsell, r.drop_date
   FROM stage.view_selling_transaction_hgcp_raw r
  WHERE NOT (r.anaplan_level_3::text = 'SEM Family'::character varying::text OR r.anaplan_level_3::text = 'Display Family'::character varying::text OR r.anaplan_level_3::text = 'Social Family'::character varying::text)
  GROUP BY r.business_id, r.anaplan_level_4, r.product_asset_id, r.product_asset_code, r.sold_date, r.order_number, r.source_system, r.original_acq_date, r.latest_acq_date, r.submitted_on, r.consultation_complete_date, r.go_live_date, 
        CASE
            WHEN r.status::text = 'Expired'::character varying::text AND (r."action"::text = 'Cancel'::character varying::text OR COALESCE(r.renewal_status, r.renewal_type)::text = 'Cancelled'::character varying::text) THEN r.end_date
            ELSE NULL::date
        END, 
        CASE
            WHEN r.status::text = 'Expired'::character varying::text AND r."action"::text <> 'Cancel'::character varying::text AND COALESCE(r.renewal_status, r.renewal_type)::text <> 'Cancelled'::character varying::text THEN r.end_date
            ELSE NULL::date
        END, r.new_existing, r.primary_employee_id, r.secondary_employee_id, r.tertiary_employee_id, r.unit_sold, COALESCE(r."action", r.status), COALESCE(r.renewal_status, r.renewal_type), r.p2d, r.p2d_expected, r.p2d_serviced_dollars, r.p2d_serviced_number, r.p2d_total, r.recent_sales_initiative, r.upsell, r.drop_date;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_hgcp_raw" AS 
 SELECT DISTINCT p.business_id_hidden__c AS business_id, ph.anaplan_level_4, COALESCE(COALESCE(p.product_asset_id__c, op.parent_asset_id::character varying), pa.order_parent_asset_id__c)::character varying(50) AS product_asset_id, a.order_asset_id__c AS asset_id, pa.name__c AS product_asset_code, a.name__c AS asset_code, p.sale_date__c AS sold_date, p.contract__c AS order_number, 'hGCP'::character varying AS source_system, mc.original_acq_date, mc.latest_acq_date, so.submitted_on::date AS submitted_on, p.consultation_complete_date__c AS consultation_complete_date, p.go_live_date__c AS go_live_date, p.end_date__c AS end_date, p.status__c AS status, t."action", p.renewal_status__c AS renewal_status, p.renewal_type__c AS renewal_type, mc.new_existing, cpi.incremental_end_date, cpi.new_product_end_date, COALESCE(p.primary_rep_employee_number__c, u1.employee_id__c) AS primary_employee_id, p.secondary_rep_employee_number__c AS secondary_employee_id, NULL::character varying::character varying(40) AS tertiary_employee_id, 1 AS unit_sold, 
        CASE
            WHEN a.monthly__c = 0::numeric::numeric(18,0)::numeric(18,2) AND op.asset_monthly_cost <> 0::numeric::numeric(18,0)::numeric(16,2) THEN op.asset_monthly_cost
            ELSE a.monthly__c
        END AS monthly_amount, op.mpe_billingperiod AS billingperiod, COALESCE(op.mpe_discountduration, 0::numeric::numeric(18,0)) AS discountduration, COALESCE(op.mpe_discountamount, 0::numeric::numeric(18,0)) AS discountamount, p.product_family__c AS product_family, op.payment_term, ph.anaplan_level_3, 
        CASE
            WHEN o.p2d__c::text = 'Yes'::character varying::text THEN 1::character varying(255)
            ELSE 0::character varying(255)
        END AS p2d, o.p2d_expected__c AS p2d_expected, o.p2d_serviced_dollars__c AS p2d_serviced_dollars, o.p2d_serviced_number__c AS p2d_serviced_number, o.p2d_total__c AS p2d_total, mc.recent_sales_initiative, p.upsell__c AS upsell, 
        CASE
            WHEN pha.prorated_sku = 0 AND pha.one_time_fee_sku = 0 AND pha.spreed_fee_sku = 0 THEN a.monthly__c
            ELSE 0::numeric::numeric(18,0)
        END AS full_amount, 
        CASE
            WHEN pha.prorated_sku = 1 THEN a.monthly__c
            ELSE 0::numeric::numeric(18,0)
        END AS prorated_amount, 
        CASE
            WHEN pha.one_time_fee_sku = 1 THEN a.one_time_fee__c
            ELSE 0::numeric::numeric(18,0)
        END AS one_time_fees, 
        CASE
            WHEN pha.spreed_fee_sku = 1 THEN a.monthly__c
            ELSE 0::numeric::numeric(18,0)
        END AS spread_fees, p.dropped_date__c AS drop_date
   FROM stage.sfdc_product p
   JOIN stage.sfdc_asset a ON p.id::text = a.product_detail__c::text
   JOIN stage.sfdc_asset pa ON p.id::text = pa.product_detail__c::text AND pa.asset_id__c::text = pa.order_parent_asset_id__c::text
   LEFT JOIN stage.sfdc_stage_oh_product_asset op ON a.external_asset_id__c::text = op.external_asset_id::text
   LEFT JOIN stage.mpe_submitted_order so ON so.order_number_alpha::text = p.contract__c::text
   JOIN customer mc ON p.business_id_hidden__c::text = mc.business_id::character varying::text
   JOIN product_hierarchy ph ON pa.name__c::text = ph.product_code::text AND ph.effective_end_date IS NULL
   JOIN product_hierarchy pha ON a.name__c::text = pha.product_code::text AND ph.effective_end_date IS NULL
   JOIN stage.sfdc_user u1 ON p.actual_rep__c::text = u1.id::text
   JOIN stage.sfdc_user u2 ON p.assigned_rep__c::text = u2.id::text
   LEFT JOIN stage.view_mpe_asset_max_transaction t ON p.product_asset_id__c::text = t.asset_id::character varying::text
   JOIN customer_product_incremental cpi ON cpi.parent_business_id::character varying::text = mc.business_id::character varying::text AND cpi.anaplan_level_3::text = ph.anaplan_level_3::text
   LEFT JOIN stage.sfdc_opportunity o ON o.order_number__c::text = p.contract__c::text AND o.business_id__c::text = p.business_id_hidden__c::text AND o.stagename::text ~~ 'Closed Won%'::character varying::text AND NOT o.p2d__c IS NULL;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_hgcp_search_display" AS 
 SELECT rmin.business_id::bigint AS business_id, rmin.anaplan_level_4 AS product, rmin.product_asset_id AS asset_id, rmin.product_asset_code AS asset_code, rmin.sold_date, rmin.order_number, rmin.source_system, rmin.original_acq_date, rmin.latest_acq_date, rmin.submitted_on, rmin.consultation_complete_date, rmin.go_live_date, 
        CASE
            WHEN rmin.status::text = 'Expired'::character varying::text AND (rmin."action"::text = 'Cancel'::character varying::text OR COALESCE(rmin.renewal_status, rmin.renewal_type)::text = 'Cancelled'::character varying::text) THEN rmin.end_date
            ELSE NULL::date
        END AS cancel_date, 
        CASE
            WHEN rmin.status::text = 'Expired'::character varying::text AND rmin."action"::text <> 'Cancel'::character varying::text AND COALESCE(rmin.renewal_status, rmin.renewal_type)::text <> 'Cancelled'::character varying::text THEN rmin.end_date
            ELSE NULL::date
        END AS end_date, rmin.new_existing, 0 AS incremental_sale, rmin.primary_employee_id, rmin.secondary_employee_id, rmin.tertiary_employee_id, rmin.unit_sold, rmin.monthly_amount::numeric(18,2) AS monthly_spend, rmin.discountamount::numeric(18,2) AS discount_amount, (rmin.discountduration / 30::numeric::numeric(18,0))::integer AS discount_period, rmin.one_time_fee::numeric(18,2) AS one_time_fee, 
        CASE
            WHEN rmin.billingperiod::character varying::text = 'OTF'::character varying::text THEN COALESCE(rmin.discountamount, 0::numeric::numeric(18,0)::numeric(18,2))
            ELSE 0::numeric::numeric(18,0)::numeric(18,2)
        END::numeric(18,2) AS one_time_fees_discount, rmin.spread_fees, rmin.spread_period, rmin."action" AS status, NULL::character varying::character varying(255) AS sale_type, rmin.p2d, rmin.p2d_expected, rmin.p2d_serviced_dollars, rmin.p2d_serviced_number, rmin.p2d_total, rmin.recent_sales_initiative, rmin.upsell, rmin.drop_date
   FROM ( SELECT r.business_id, r.anaplan_level_4, r.product_asset_id, r.product_asset_code, r.sold_date, r.order_number, r.source_system, r.original_acq_date, r.latest_acq_date, r.submitted_on, r.consultation_complete_date, 
                CASE
                    WHEN r.submitted_on > r.go_live_date THEN r.submitted_on
                    ELSE r.go_live_date
                END AS go_live_date, r.end_date, r.status, r."action", r.renewal_status, r.renewal_type, r.new_existing, r.incremental_end_date, r.new_product_end_date, r.primary_employee_id, r.secondary_employee_id, r.tertiary_employee_id, r.unit_sold, sum(r.full_amount) AS monthly_amount, "max"(r.billingperiod::text) AS billingperiod, "max"(r.discountduration) AS discountduration, sum(r.one_time_fees) AS one_time_fee, sum(r.discountamount) AS discountamount, r.product_family, "max"(r.payment_term::text) AS payment_term, r.anaplan_level_3, r.p2d, r.p2d_expected, r.p2d_serviced_dollars, r.p2d_serviced_number, r.p2d_total, r.recent_sales_initiative, r.upsell, sum(COALESCE(r.spread_fees, 0::numeric::numeric(18,0))) AS spread_fees, "max"(
                CASE
                    WHEN r.spread_fees > 0::numeric::numeric(18,0)::numeric(18,2) THEN r.payment_term
                    ELSE '0'::character varying
                END::text)::integer AS spread_period, r.drop_date
           FROM stage.view_selling_transaction_hgcp_raw r
          WHERE r.anaplan_level_3::text = 'SEM Family'::character varying::text OR r.anaplan_level_3::text = 'Display Family'::character varying::text OR r.anaplan_level_3::text = 'Social Family'::character varying::text
          GROUP BY r.business_id, r.anaplan_level_4, r.product_asset_id, r.product_asset_code, r.sold_date, r.order_number, r.source_system, r.original_acq_date, r.latest_acq_date, r.submitted_on, r.consultation_complete_date, r.go_live_date, r.end_date, r.status, r."action", r.renewal_status, r.renewal_type, r.new_existing, r.incremental_end_date, r.new_product_end_date, r.primary_employee_id, r.secondary_employee_id, r.tertiary_employee_id, r.unit_sold, r.product_family, r.anaplan_level_3, r.p2d, r.p2d_expected, r.p2d_serviced_dollars, r.p2d_serviced_number, r.p2d_total, r.recent_sales_initiative, r.upsell, r.drop_date) rmin;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_incremental_min_selling_id" AS 
 SELECT DISTINCT min(st.selling_transaction_id) AS selling_transaction_id, st.business_id, ph.anaplan_level_3
   FROM selling_transaction st
   JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL
   JOIN ( SELECT DISTINCT st.business_id, ph.anaplan_level_3, min(st.submitted) AS submitted
      FROM selling_transaction st
   JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL
  WHERE ph.anaplan_level_3::text <> 'Print Family'::character varying::text
  GROUP BY st.business_id, ph.anaplan_level_3) a ON a.business_id = st.business_id AND a.submitted = st.submitted AND a.anaplan_level_3::text = ph.anaplan_level_3::text
  WHERE st.incremental_sale = 0
  GROUP BY ph.anaplan_level_3, st.business_id;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_incremental_update" AS 
 SELECT DISTINCT a.selling_transaction_id
   FROM ( SELECT st1.selling_transaction_id, st1.business_id, st1.product, st1.sold_date, st1.submitted, st1.go_live_date, st1.cancel_end_date, st1.incremental_sale, "max"(st2.cancel_end_date) AS prior_record_cancel_end_date, st1.incremental_end_date
           FROM ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, st.submitted, st.sold_date, st.go_live_date, cpi.incremental_end_date, st.incremental_sale, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL
         JOIN customer_product_incremental cpi ON cpi.parent_business_id = st.business_id AND ph.anaplan_level_3::text = cpi.anaplan_level_3::text
        WHERE ph.anaplan_level_3::text <> 'Print Family'::character varying::text) st1
      JOIN ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, st.submitted, st.sold_date, st.go_live_date, cpi.incremental_end_date, st.incremental_sale, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL
         JOIN customer_product_incremental cpi ON cpi.parent_business_id = st.business_id AND ph.anaplan_level_3::text = cpi.anaplan_level_3::text
        WHERE ph.anaplan_level_3::text <> 'Print Family'::character varying::text) st2 ON st1.business_id = st2.business_id AND st1.anaplan_level_3::text = st2.anaplan_level_3::text AND (st1.submitted > st2.submitted OR st1.sold_date > st2.sold_date OR st1.go_live_date > st2.go_live_date)
     GROUP BY st1.selling_transaction_id, st1.business_id, st1.product, st1.sold_date, st1.submitted, st1.go_live_date, st1.cancel_end_date, st1.incremental_sale, st1.incremental_end_date
    HAVING NOT "max"(st2.cancel_end_date) IS NULL) a
  WHERE a.go_live_date >= date_add('months'::character varying::text, 12::bigint, a.prior_record_cancel_end_date::timestamp without time zone) OR a.submitted >= date_add('months'::character varying::text, 12::bigint, a.prior_record_cancel_end_date::timestamp without time zone) OR a.sold_date >= date_add('months'::character varying::text, 12::bigint, a.prior_record_cancel_end_date::timestamp without time zone);


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_sale_type_insert" AS 
 SELECT a.selling_transaction_id
   FROM ( SELECT st1.selling_transaction_id, st1.business_id, st1.anaplan_level_3, st1.product, st1.submitted, st1.sold_date, st1.go_live_date, st1.sale_type, st1.cancel_end_date, "max"(st2.sold_date) AS prior_sold_date, "max"(st2.go_live_date) AS prior_go_live_date, "max"(st2.submitted) AS prior_submitted, "max"(st2.cancel_end_date) AS prior_cancel_end_date
           FROM ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, 
                        CASE
                            WHEN st.source_system::text = 'DIAD'::character varying::text AND st.sold_date < st.submitted THEN st.sold_date
                            ELSE st.submitted
                        END AS submitted, st.sold_date, st.go_live_date, st.sale_type, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL AND ph.anaplan_level_3::text <> 'Print Family'::character varying::text) st1
      JOIN ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, 
                        CASE
                            WHEN st.source_system::text = 'DIAD'::character varying::text AND st.sold_date < st.submitted THEN st.sold_date
                            ELSE st.submitted
                        END AS submitted, st.sold_date, st.go_live_date, st.sale_type, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL AND ph.anaplan_level_3::text <> 'Print Family'::character varying::text) st2 ON st1.business_id = st2.business_id AND st1.anaplan_level_3::text = st2.anaplan_level_3::text AND st1.submitted > st2.submitted
     WHERE NOT (st1.sale_type::text = 'New'::character varying::text OR st1.sale_type::text = 'New Insert'::character varying::text OR st1.sale_type::text = 'Spend Change'::character varying::text OR st1.sale_type::text = 'Insert'::character varying::text) OR st1.sale_type IS NULL
     GROUP BY st1.selling_transaction_id, st1.business_id, st1.anaplan_level_3, st1.product, st1.submitted, st1.sold_date, st1.go_live_date, st1.sale_type, st1.cancel_end_date) a;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_sale_type_new" AS 
 SELECT t.selling_transaction_id
   FROM selling_transaction t
  WHERE (t.sale_type::text <> 'New'::character varying::text OR t.sale_type IS NULL) AND (t.submitted = t.latest_acq_date OR t.sold_date = t.latest_acq_date OR t.submitted = t.original_acq_date OR t.sold_date = t.original_acq_date);


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_sale_type_new_insert" AS 
 SELECT a.selling_transaction_id
   FROM ( SELECT st1.selling_transaction_id, st1.business_id, st1.anaplan_level_3, st1.product, st1.submitted, st1.sold_date, st1.go_live_date, st1.sale_type, st1.cancel_end_date, "max"(st2.sold_date) AS prior_sold_date, "max"(st2.go_live_date) AS prior_go_live_date, "max"(st2.submitted) AS prior_submitted, "max"(st2.cancel_end_date) AS prior_cancel_end_date
           FROM ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, 
                        CASE
                            WHEN st.source_system::text = 'DIAD'::character varying::text AND st.sold_date < st.submitted THEN st.sold_date
                            ELSE st.submitted
                        END AS submitted, st.sold_date, st.go_live_date, st.sale_type, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL AND ph.anaplan_level_3::text <> 'Print Family'::character varying::text) st1
      LEFT JOIN ( SELECT DISTINCT st.selling_transaction_id, st.business_id, ph.anaplan_level_3, st.product, 
                        CASE
                            WHEN st.source_system::text = 'DIAD'::character varying::text AND st.sold_date < st.submitted THEN st.sold_date
                            ELSE st.submitted
                        END AS submitted, st.sold_date, st.go_live_date, st.sale_type, COALESCE(st.cancel_date, st.end_date) AS cancel_end_date
                   FROM selling_transaction st
              JOIN product_hierarchy ph ON st.product::text = ph.anaplan_level_4::text AND ph.effective_end_date IS NULL AND ph.anaplan_level_3::text <> 'Print Family'::character varying::text
             WHERE NOT st.go_live_date IS NULL AND st.go_live_date <> COALESCE(COALESCE(st.cancel_date, st.end_date), '2040-01-01'::date)) st2 ON st1.business_id = st2.business_id AND st1.anaplan_level_3::text = st2.anaplan_level_3::text AND st1.submitted > st2.submitted
     WHERE NOT (st1.sale_type::text = 'New'::character varying::text OR st1.sale_type::text = 'New Insert'::character varying::text OR st1.sale_type::text = 'Spend Change'::character varying::text) OR st1.sale_type IS NULL
     GROUP BY st1.selling_transaction_id, st1.business_id, st1.anaplan_level_3, st1.product, st1.submitted, st1.sold_date, st1.go_live_date, st1.sale_type, st1.cancel_end_date) a
  WHERE a.prior_submitted IS NULL OR a.submitted >= date_add('months'::character varying::text, 12::bigint, a.prior_cancel_end_date::timestamp without time zone);


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_sale_type_spend_change" AS 
 SELECT st4.selling_transaction_id, 'Spend Change' AS sale_type, st4.product_monthly_revenue - st3.product_monthly_revenue AS spend_change_amount, 'now'::character varying::timestamp with time zone AS update_datetime
   FROM selling_transaction st3
   JOIN ( SELECT DISTINCT st1.selling_transaction_id, st1.business_id, st1.asset_id, st1.submitted, st1.product_monthly_revenue, st1.sale_type, "max"(st2.submitted) AS prior_submitted
           FROM selling_transaction st1
      JOIN selling_transaction st2 ON st1.business_id = st2.business_id AND st1.asset_id = st2.asset_id AND st1.submitted > st2.submitted
     WHERE st1.sale_type::text <> 'Spend Change'::character varying::text AND st1.asset_id <> 0
     GROUP BY st1.selling_transaction_id, st1.business_id, st1.asset_id, st1.submitted, st1.product_monthly_revenue, st1.sale_type) st4 ON st3.business_id = st4.business_id AND st3.asset_id = st4.asset_id AND st3.submitted = st4.prior_submitted
  WHERE st3.product_monthly_revenue <> st4.product_monthly_revenue;


CREATE OR REPLACE VIEW "stage"."view_selling_transaction_upsert" AS 
(( SELECT view_selling_transaction_hgcp_detect_changes.business_id_hidden__c, view_selling_transaction_hgcp_detect_changes.product, view_selling_transaction_hgcp_detect_changes.asset_id, view_selling_transaction_hgcp_detect_changes.asset_code, view_selling_transaction_hgcp_detect_changes.sold_date, view_selling_transaction_hgcp_detect_changes.order_number, view_selling_transaction_hgcp_detect_changes.source_system, view_selling_transaction_hgcp_detect_changes.original_acq_date, view_selling_transaction_hgcp_detect_changes.latest_acq_date, view_selling_transaction_hgcp_detect_changes.submitted, view_selling_transaction_hgcp_detect_changes.consultation_complete_date, view_selling_transaction_hgcp_detect_changes.go_live_date, view_selling_transaction_hgcp_detect_changes.cancel_date, view_selling_transaction_hgcp_detect_changes.end_date, view_selling_transaction_hgcp_detect_changes.new_existing, view_selling_transaction_hgcp_detect_changes.incremental_sale, view_selling_transaction_hgcp_detect_changes.primary_employee_id, view_selling_transaction_hgcp_detect_changes.secondary_employee_id, view_selling_transaction_hgcp_detect_changes.tertiary_employee_id, view_selling_transaction_hgcp_detect_changes.unit_sold, view_selling_transaction_hgcp_detect_changes.monthly_spend, view_selling_transaction_hgcp_detect_changes.discount_amount, view_selling_transaction_hgcp_detect_changes.discount_period, view_selling_transaction_hgcp_detect_changes.one_time_fees, view_selling_transaction_hgcp_detect_changes.one_time_fees_discount, view_selling_transaction_hgcp_detect_changes.spread_fees, view_selling_transaction_hgcp_detect_changes.spread_period, view_selling_transaction_hgcp_detect_changes.status, view_selling_transaction_hgcp_detect_changes.sale_type, view_selling_transaction_hgcp_detect_changes.new_update, view_selling_transaction_hgcp_detect_changes.today, view_selling_transaction_hgcp_detect_changes.selling_transaction_id, view_selling_transaction_hgcp_detect_changes.initial_load_date, view_selling_transaction_hgcp_detect_changes.p2d, view_selling_transaction_hgcp_detect_changes.p2d_expected, view_selling_transaction_hgcp_detect_changes.p2d_serviced_dollars, view_selling_transaction_hgcp_detect_changes.p2d_serviced_number, view_selling_transaction_hgcp_detect_changes.p2d_total, view_selling_transaction_hgcp_detect_changes.recent_sales_initiative, view_selling_transaction_hgcp_detect_changes.upsell, NULL::character varying::character varying(40) AS min_uuid, view_selling_transaction_hgcp_detect_changes.drop_date, NULL::character varying::character varying(8) AS iyp_net_coverage, NULL::character varying::character varying(40) AS bundle_id
   FROM stage.view_selling_transaction_hgcp_detect_changes
  WHERE view_selling_transaction_hgcp_detect_changes.new_update::text <> 'None'::character varying::text
UNION 
 SELECT view_selling_transaction_diad_detect_changes.business_id_hidden__c, view_selling_transaction_diad_detect_changes.product, view_selling_transaction_diad_detect_changes.asset_id, view_selling_transaction_diad_detect_changes.asset_code, view_selling_transaction_diad_detect_changes.sold_date, view_selling_transaction_diad_detect_changes.order_number, view_selling_transaction_diad_detect_changes.source_system, view_selling_transaction_diad_detect_changes.original_acq_date, view_selling_transaction_diad_detect_changes.latest_acq_date, view_selling_transaction_diad_detect_changes.submitted, view_selling_transaction_diad_detect_changes.consultation_complete_date, view_selling_transaction_diad_detect_changes.go_live_date, view_selling_transaction_diad_detect_changes.cancel_date, view_selling_transaction_diad_detect_changes.end_date, view_selling_transaction_diad_detect_changes.new_existing, view_selling_transaction_diad_detect_changes.incremental_sale, view_selling_transaction_diad_detect_changes.primary_employee_id, view_selling_transaction_diad_detect_changes.secondary_employee_id, view_selling_transaction_diad_detect_changes.tertiary_employee_id, view_selling_transaction_diad_detect_changes.unit_sold, view_selling_transaction_diad_detect_changes.monthly_spend, view_selling_transaction_diad_detect_changes.discount_amount, view_selling_transaction_diad_detect_changes.discount_period, view_selling_transaction_diad_detect_changes.one_time_fees, view_selling_transaction_diad_detect_changes.one_time_fees_discount, view_selling_transaction_diad_detect_changes.spread_fees, view_selling_transaction_diad_detect_changes.spread_period, view_selling_transaction_diad_detect_changes.status, view_selling_transaction_diad_detect_changes.sale_type, view_selling_transaction_diad_detect_changes.new_update, view_selling_transaction_diad_detect_changes.today, view_selling_transaction_diad_detect_changes.selling_transaction_id, view_selling_transaction_diad_detect_changes.initial_load_date, NULL::"unknown" AS p2d, 0 AS p2d_expected, 0 AS p2d_serviced_dollars, 0 AS p2d_serviced_number, 0 AS p2d_total, NULL::"unknown" AS recent_sales_initiative, 0 AS upsell, NULL::character varying::character varying(40) AS min_uuid, view_selling_transaction_diad_detect_changes.drop_date, NULL::character varying::character varying(8) AS iyp_net_coverage, view_selling_transaction_diad_detect_changes.bundle_id
   FROM stage.view_selling_transaction_diad_detect_changes
  WHERE view_selling_transaction_diad_detect_changes.new_update::text <> 'None'::character varying::text)
UNION 
 SELECT view_selling_transaction_diad_web_detect_changes.business_id_hidden__c, view_selling_transaction_diad_web_detect_changes.product, view_selling_transaction_diad_web_detect_changes.asset_id, view_selling_transaction_diad_web_detect_changes.asset_code, view_selling_transaction_diad_web_detect_changes.sold_date, view_selling_transaction_diad_web_detect_changes.order_number, view_selling_transaction_diad_web_detect_changes.source_system, view_selling_transaction_diad_web_detect_changes.original_acq_date, view_selling_transaction_diad_web_detect_changes.latest_acq_date, view_selling_transaction_diad_web_detect_changes.submitted, view_selling_transaction_diad_web_detect_changes.consultation_complete_date, view_selling_transaction_diad_web_detect_changes.go_live_date, view_selling_transaction_diad_web_detect_changes.cancel_date, view_selling_transaction_diad_web_detect_changes.end_date, view_selling_transaction_diad_web_detect_changes.new_existing, view_selling_transaction_diad_web_detect_changes.incremental_sale, view_selling_transaction_diad_web_detect_changes.primary_employee_id, view_selling_transaction_diad_web_detect_changes.secondary_employee_id, view_selling_transaction_diad_web_detect_changes.tertiary_employee_id, view_selling_transaction_diad_web_detect_changes.unit_sold, view_selling_transaction_diad_web_detect_changes.monthly_spend, view_selling_transaction_diad_web_detect_changes.discount_amount, view_selling_transaction_diad_web_detect_changes.discount_period, view_selling_transaction_diad_web_detect_changes.one_time_fees, view_selling_transaction_diad_web_detect_changes.one_time_fees_discount, view_selling_transaction_diad_web_detect_changes.spread_fees, view_selling_transaction_diad_web_detect_changes.spread_period, view_selling_transaction_diad_web_detect_changes.status, view_selling_transaction_diad_web_detect_changes.sale_type, view_selling_transaction_diad_web_detect_changes.new_update, view_selling_transaction_diad_web_detect_changes.today, view_selling_transaction_diad_web_detect_changes.selling_transaction_id, view_selling_transaction_diad_web_detect_changes.initial_load_date, NULL::"unknown" AS p2d, 0 AS p2d_expected, 0 AS p2d_serviced_dollars, 0 AS p2d_serviced_number, 0 AS p2d_total, NULL::"unknown" AS recent_sales_initiative, 0 AS upsell, view_selling_transaction_diad_web_detect_changes.min_uuid, view_selling_transaction_diad_web_detect_changes.drop_date, NULL::character varying::character varying(8) AS iyp_net_coverage, NULL::character varying::character varying(40) AS bundle_id
   FROM stage.view_selling_transaction_diad_web_detect_changes
  WHERE view_selling_transaction_diad_web_detect_changes.new_update::text <> 'None'::character varying::text)
UNION 
 SELECT view_selling_transaction_diad_iyp_detect_changes.business_id_hidden__c, view_selling_transaction_diad_iyp_detect_changes.product, view_selling_transaction_diad_iyp_detect_changes.asset_id, view_selling_transaction_diad_iyp_detect_changes.asset_code, view_selling_transaction_diad_iyp_detect_changes.sold_date, view_selling_transaction_diad_iyp_detect_changes.order_number, view_selling_transaction_diad_iyp_detect_changes.source_system, view_selling_transaction_diad_iyp_detect_changes.original_acq_date, view_selling_transaction_diad_iyp_detect_changes.latest_acq_date, view_selling_transaction_diad_iyp_detect_changes.submitted, view_selling_transaction_diad_iyp_detect_changes.consultation_complete_date, view_selling_transaction_diad_iyp_detect_changes.go_live_date, view_selling_transaction_diad_iyp_detect_changes.cancel_date, view_selling_transaction_diad_iyp_detect_changes.end_date, view_selling_transaction_diad_iyp_detect_changes.new_existing, view_selling_transaction_diad_iyp_detect_changes.incremental_sale, view_selling_transaction_diad_iyp_detect_changes.primary_employee_id, view_selling_transaction_diad_iyp_detect_changes.secondary_employee_id, view_selling_transaction_diad_iyp_detect_changes.tertiary_employee_id, view_selling_transaction_diad_iyp_detect_changes.unit_sold, view_selling_transaction_diad_iyp_detect_changes.monthly_spend, view_selling_transaction_diad_iyp_detect_changes.discount_amount, view_selling_transaction_diad_iyp_detect_changes.discount_period, view_selling_transaction_diad_iyp_detect_changes.one_time_fees, view_selling_transaction_diad_iyp_detect_changes.one_time_fees_discount, view_selling_transaction_diad_iyp_detect_changes.spread_fees, view_selling_transaction_diad_iyp_detect_changes.spread_period, view_selling_transaction_diad_iyp_detect_changes.status, view_selling_transaction_diad_iyp_detect_changes.sale_type, view_selling_transaction_diad_iyp_detect_changes.new_update, view_selling_transaction_diad_iyp_detect_changes.today, view_selling_transaction_diad_iyp_detect_changes.selling_transaction_id, view_selling_transaction_diad_iyp_detect_changes.initial_load_date, NULL::"unknown" AS p2d, 0 AS p2d_expected, 0 AS p2d_serviced_dollars, 0 AS p2d_serviced_number, 0 AS p2d_total, NULL::"unknown" AS recent_sales_initiative, 0 AS upsell, view_selling_transaction_diad_iyp_detect_changes.min_uuid, view_selling_transaction_diad_iyp_detect_changes.drop_date, view_selling_transaction_diad_iyp_detect_changes.iyp_net_coverage, view_selling_transaction_diad_iyp_detect_changes.bundle_id
   FROM stage.view_selling_transaction_diad_iyp_detect_changes
  WHERE view_selling_transaction_diad_iyp_detect_changes.new_update::text <> 'None'::character varying::text;


CREATE OR REPLACE VIEW "stage"."view_sfdc_master_customer_dedup_zero_products" AS 
 SELECT mc.ghost_account_id__c, mc.business_id_hidden__c, mc.name, mc.id, count(p.id) AS count
   FROM stage.sfdc_master_customer mc
   LEFT JOIN stage.sfdc_product p ON p.business_id_hidden__c::text = mc.business_id_hidden__c::character varying::text
  WHERE (EXISTS ( SELECT 1
      FROM ( SELECT count(DISTINCT mc.business_id_hidden__c) AS cnt, mc.ghost_account_id__c
              FROM stage.sfdc_master_customer mc
             WHERE NOT mc.ghost_account_id__c IS NULL
             GROUP BY mc.ghost_account_id__c
            HAVING count(DISTINCT mc.business_id_hidden__c) > 1) a
     WHERE a.ghost_account_id__c = mc.ghost_account_id__c))
  GROUP BY mc.ghost_account_id__c, mc.business_id_hidden__c, mc.name, mc.id
 HAVING count(p.id) = 0
  ORDER BY mc.ghost_account_id__c;


